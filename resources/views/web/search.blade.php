@extends('web.layouts.base')
@section('content')
    <div class="site-content">
    <div class="mnmd-block mnmd-block--fullwidth mnmd-block--contiguous page-heading page-heading--has-background">
        <div class="container">
            <h2 class="page-heading__title">Từ khóa tìm kiếm: {{$keyword}}</h2>
            {{-- <div class="page-heading__subtitle">More description.</div> --}}
        </div>
    </div>
    <div class="mnmd-block mnmd-block--fullwidth">
        <div class="container">
            <div class="row">
                <div class="mnmd-main-col" role="main">
                <div class="mnmd-block">
                    <div class="posts-list list-unstyled list-space-lg">
                        @foreach ($listArticle as $index=>$article)
                            @if ($index == 0)
                            <li class="list-item">
                                <article class="post--overlay post--overlay-floorfade post--overlay-bottom post--overlay-sm post--overlay-padding-lg has-score-badge cat-4">
                                    <div class="background-img" style="background-image: url({{ MediaHelper::show($article->picture) }})"></div>
                                    <div class="post__text inverse-text">
                                        <div class="post__text-wrap">
                                        <div class="post__text-inner">
                                            <a href="{{'/'.$article->ct_slug}}" class="post__cat post__cat--bg cat-theme-bg">{{$article->ct_name}}</a>
                                            <h3 class="post__title typescale-4"><a href="#single-url">{{$article->title}}</a></h3>
                                            <div class="post__meta">
                                                @if ($article->show_author)
                                                    <span class="entry-author">Đăng bởi <a href="#" class="entry-author__name">{{$article->author}}</a></span> 
                                                @endif
                                                @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                                    <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time> 
                                                @endif   
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <a href="{{'/'.$article->ct_slug.'/'.$article->slug}}" class="link-overlay"></a>
                                </article>
                            </li>
                            @else
                            <div class="list-item">
                                <article class="post post--horizontal post--horizontal-sm cat-6">
                                    <div class="post__thumb"><a href="{{'/'.$article->ct_slug.'/'.$article->slug}}"><img src="{{ MediaHelper::show($article->picture) }}"></a></div>
                                    <div class="post__text">
                                        <a href="{{'/'.$article->ct_slug}}" class="post__cat cat-theme">{{$article->ct_name}}</a>
                                        <h3 class="post__title typescale-2"><a href="{{'/'.$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                        <div class="post__excerpt">{!! $article->sumary !!}</div>
                                        <div class="post__meta">
                                        @if ($article->show_author)
                                            <span class="entry-author">Đăng bởi <a href="#" class="entry-author__name">{{$article->author}}</a></span> 
                                        @endif
                                        @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                            <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time> 
                                        @endif   
                                        </div>
                                    </div>
                                </article>
                            </div>
                            @endif
                        @endforeach
                    </div>
                    <nav class="mnmd-pagination">
                            {{ $listArticle->render('vendor.pagination.custom') }}
                    </nav>
                </div>
                <!-- .mnmd-block -->
                </div>
                <!-- .mnmd-main-col -->
                @include('web.shared.sidebar')
                <div class="mnmd-sub-col mnmd-sub-col--right sidebar js-sticky-sidebar" role="complementary">
                    <!-- .widget subscriber -->
                    {{-- @include('web.shared.widget_subscribe') --}}
                    <!-- .widget social-->
                    {{-- @include('web.shared.widget_social') --}}
                </div>
                <!-- .mnmd-sub-col -->
            </div>
            <!-- .row -->
        </div>
        <!-- .container -->
    </div>
    <!-- .mnmd-block -->
    </div>
    <!-- .site-content -->    
@endsection 