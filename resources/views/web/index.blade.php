@extends('web.layouts.base')
@section('content')
   <div class="site-content" style="transform: none;">
      <div class="mnmd-block mnmd-block--fullwidth  mnmd-block--contiguous mnmd-mosaic mnmd-mosaic--has-shadow mnmd-mosaic--gutter-10 has-overlap-background">
         <div class="overlap-background gradient-5"></div>
         <div class="container container--wide">
            <div class="row row--space-between">
               @foreach ($latest_article as $key=>$article)
                  @if ($key == 0)
                     <div class="mosaic-item col-xs-12 col-md-6">
                        <article class="post--overlay post--overlay-bottom post--overlay-floorfade cat-1">
                           <div class="background-img" style="background-image: url({{MediaHelper::show($article->picture)}})"></div>
                           <div class="post__text">
                              <div class="post__text-wrap">
                                 <div class="post__text-inner inverse-text">
                                    <div class="media">
                                       @if ($article->article_type == 3)
                                          <div class="media-left media-middle post-type-icon"><i class="mdicon mdicon-play_circle_outline"></i></div>
                                       @endif
                                       <div class="media-body media-middle">
                                          <h3 class="post__title typescale-4">{{$article->title}}</h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @if($article->article_type == 2)
                              <div class="overlay-item gallery-icon"><i class="mdicon mdicon-filter"></i><span>{{$article->media_count}}</span></div>
                           @endif
                           <a href="{{$article->ct_slug.'/'.$article->slug}}" class="link-overlay"></a> 
                           <a href="{!! $article->ct_slug !!}" class="post__cat post__cat--bg cat-theme-bg overlay-item--top-left">{!! $article->ct_name !!}</a>
                        </article>
                     </div>
                  @endif
               @endforeach
               @foreach ($latest_article as $key=>$article)
                  @if ($key > 0)
                     <div class="mosaic-item mosaic-item--half col-xs-12 col-sm-6 col-md-3">
                        <article class="post--overlay post--overlay-bottom post--overlay-floorfade medium-post cat-2">
                           <div class="background-img" style="background-image: url({{MediaHelper::show($article->picture)}})"></div>
                           <div class="post__text inverse-text">
                              <div class="post__text-wrap">
                                 <div class="post__text-inner">
                                    <div class="media">
                                       @if ($article->article_type == 3)
                                          <div class="media-left media-middle post-type-icon"><i class="mdicon mdicon-play_circle_outline"></i></div>
                                       @endif
                                       <div class="media-body media-middle">
                                          <h3 class="post__title typescale-1"><a href="{{$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           @if($article->article_type == 2)
                              <div class="overlay-item gallery-icon"><i class="mdicon mdicon-filter"></i><span>{{$article->media_count}}</span></div>
                           @endif
                           <a href="{{$article->ct_slug.'/'.$article->slug}}" class="link-overlay"></a> 
                           <a href="{!! $article->ct_slug !!}" class="post__cat post__cat--bg cat-theme-bg overlay-item--top-left">{!! $article->ct_name !!}</a>
                        </article>
                     </div>
                  @endif
               @endforeach
            </div>
         </div>
      </div>
      {{-- .mnmd-block --}}
      {{-- <div class="mnmd-block mnmd-block--fullwidth">
         <div class="container">
            <div class="subscribe-form-wrap">
               <div class="subscribe-form subscribe-form--has-background subscribe-form--horizontal text-center">
                  <div class="subscribe-form__inner">
                     <div class="row row--space-between row--flex row--vertical-center">
                        <div class="col-xs-12 col-md-6">
                           <h3><b>Sign up for our weekly newsletter</b></h3>
                           <p>Get more travel inspiration, tips and exclusive offers sent straight to your inbox</p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                           <div class="subscribe-form__fields"><input type="email" name="EMAIL" placeholder="Your email address" required=""> <input type="submit" value="Subscribe" class="btn btn-primary"></div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> --}}
      <div class="mnmd-block mnmd-block--fullwidth">
         <div class="container">
            @include('web.shared.widget_ads_header')
         </div>
      </div>

      {{-- .mnmd-block --}}
      <div class="mnmd-layout-split mnmd-block mnmd-block--fullwidth" style="transform: none;">
         <div class="container" style="transform: none;">
            <div class="row" style="transform: none;">
               {{-- Main Column --}}
               <div class="mnmd-main-col">
                  @foreach ($listArticleByCate as $indexCate=>$listCateItem)
                     @if ($indexCate == 0 || $indexCate == 2 )
                        <div class="mnmd-block cat-1">
                           <div class="block-heading">
                              <h4 class="block-heading__title"><a href="{{route('web.category',['categorySlug'=>$listCateItem['category']['slug']])}}">{{$listCateItem['category']['name']}}</a></h4>
                           </div>
                           <div class="row row--space-between">
                              <div class="col-xs-12 col-sm-8">
                                 @foreach ($listCateItem['listArticle'] as $indexAr=>$article)
                                    @if ($indexAr == 0)
                                       <article class="post post--vertical cat-4 1-col">
                                          <div class="post__thumb"><a href="{{'/'.$article->ct_slug.'/'.$article->slug}}"><img src="{{ MediaHelper::show($article->picture) }}"> </a>
                                             <a href="{{$article->ct_slug}}" class="post__cat post__cat--bg cat-theme-bg overlay-item--top-left">{{$article->ct_name}}</a>
                                          </div>
                                          <div class="post__text">
                                             <h3 class="post__title typescale-3"><a href="{{$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                             <div class="post__meta">
                                                @if (isset($configs) && !empty($configs['SHOW_AUTHOR_HOMEPAGE']))
                                                   @if(!empty($article->author))
                                                      <span class="entry-author">Đăng bởi <a href="#" class="entry-author__name">{{$article->author}}</a></span>
                                                   @endif
                                                @endif
                                                @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                                   <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time> 
                                                @endif 
                                                @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                   <a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}"><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}} bình luận</a>
                                                @endif
                                             </div>
                                          </div>
                                       </article>
                                     
                                    @endif
                                 @endforeach
                              </div>
                              <div class="col-xs-12 col-sm-4">
                                 <ul class="list-space-xs list-seperated list-square-bullet-exclude-first list-unstyled">
                                    @foreach ($listCateItem['listArticle'] as $indexAr=>$article)
                                       @if ($indexAr == 1)
                                          <li>
                                             <article class="post post--vertical cat-4">
                                                <div class="post__thumb"><a href="{{'/'.$article->ct_slug.'/'.$article->slug}}"><img src="{{ MediaHelper::show($article->picture) }}"></a></div>
                                                <div class="post__text">
                                                   <h3 class="post__title typescale-1"><a href="{{'/'.$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                                   <div class="post__meta">
                                                      @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                                         <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time>
                                                      @endif
                                                   </div>
                                                </div>
                                             </article>
                                          </li>
                                       @elseif($indexAr > 1)
                                          <li>
                                             <article class="post cat-4">
                                                <h3 class="post__title typescale-0"><a href="{{'/'.$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                             </article>
                                          </li>
                                       @endif
                                    @endforeach
                                 </ul>
                              </div>
                           </div>
                        </div>
                     @elseif($indexCate == 1)
                        {{-- .mnmd-block --}}
                        <div class="mnmd-block mnmd-carousel">
                           <div class="block-heading">
                           <h4 class="block-heading__title"><a href="{{route('web.category',['categorySlug'=>$listCateItem['category']['slug']])}}">{{$listCateItem['category']['name']}}</a></h4>
                           </div>
                           <div class="owl-carousel js-carousel-3i4m-small owl-loaded owl-drag">
                              @foreach ($listCateItem['listArticle'] as $article)
                                 <div class="slide-content item">
                                    <article class="post--overlay post--overlay-sm has-score-badge-bottom cat-4">
                                       <div class="background-img background-img--darkened" style="background-image: url({{ MediaHelper::show($article->picture) }})"></div>
                                       <div class="post__text inverse-text text-center">
                                          <div class="post__text-wrap">
                                          <div class="post__text-inner">
                                             <h3 class="post__title typescale-2">{{$article->title}}</h3>
                                             <div class="post__meta">
                                                @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                                   <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time>
                                                @endif
                                             </div>
                                          </div>
                                          </div>
                                       </div>
                                       {{-- <div class="post-score-hexagon">
                                          <svg class="hexagon-svg" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="-5 -5 184 210">
                                          <g>
                                             <path fill="#FC3C2D" stroke="#fff" stroke-width="10px" d="M81.40638795573723 2.9999999999999996Q86.60254037844386 0 91.7986928011505 2.9999999999999996L168.0089283341811 47Q173.20508075688772 50 173.20508075688772 56L173.20508075688772 144Q173.20508075688772 150 168.0089283341811 153L91.7986928011505 197Q86.60254037844386 200 81.40638795573723 197L5.196152422706632 153Q0 150 0 144L0 56Q0 50 5.196152422706632 47Z"></path>
                                          </g>
                                          </svg>
                                          <span class="post-score-value">9</span>
                                       </div> --}}
                                       @if($article->article_type == 2)
                                          <div class="overlay-item gallery-icon"><i class="mdicon mdicon-filter"></i><span>{{$article->media_count}}</span></div>
                                       @endif
                                       <a href="{{$article->ct_slug.'/'.$article->slug}}" class="link-overlay"></a>
                                    </article>
                                 </div>
                              @endforeach        
                           </div>
                        </div>
                     @endif
                  @endforeach
                  <div class="mnmd-block">
                     <div class="row row--space-between">   
                        @foreach ($listArticleByCate as $indexCate=>$listCateItem)
                           @if($indexCate == 3 || $indexCate == 4)
                              <div class="col-xs-12 col-sm-6">
                                    <div class="block-heading block-heading--line">
                                       <h4 class="block-heading__title"><a href="{{route('web.category',['categorySlug'=>$listCateItem['category']['slug']])}}">{{$listCateItem['category']['name']}}</a></h4>
                                    </div>
                                    @foreach ($listCateItem['listArticle'] as $indexAr=>$article)
                                       @if ($indexAr == 0)
                                          <article class="post--overlay post--overlay-bottom post--overlay-floorfade post--overlay-xs cat-2">
                                             <div class="background-img" style="background-image: url({{ MediaHelper::show($article->picture) }})"></div>
                                             <div class="post__text inverse-text">
                                                <div class="post__text-wrap">
                                                   <div class="post__text-inner">
                                                      <h3 class="post__title typescale-2">{{$article->title}}</h3>
                                                      <div class="post__meta">
                                                         @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                                            <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time>
                                                         @endif
                                                         @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                            <a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}"><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}}</a></div>
                                                         @endif
                                                   </div>
                                                </div>
                                             </div>
                                             <a href="{{$article->ct_slug}}" class="post__cat post__cat--bg cat-theme-bg overlay-item--top-left">{{$article->ct_name}}</a> 
                                             @if($article->article_type == 2)
                                                <div class="overlay-item gallery-icon"><i class="mdicon mdicon-filter"></i><span>{{$article->media_count}}</span></div>
                                             @endif
                                             <a href="{{$article->ct_slug.'/'.$article->slug}}" class="link-overlay"></a>
                                          </article>
                                       @endif
                                    @endforeach
                                       <div class="spacer-xs"></div>
                                    <ul class="list-space-xs list-square-bullet-exclude-first list-seperated list-unstyled">  
                                       @foreach ($listCateItem['listArticle'] as $indexAr=>$article)
                                       
                                          @if ($indexAr == 1)
                                             <li>
                                                <article class="post--horizontal post--horizontal-xs cat-2">
                                                   <div class="post__thumb"><a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}"><img src="{{ MediaHelper::show($article->picture) }}"></a></div>
                                                   <div class="post__text">
                                                      <h3 class="post__title typescale-1"><a href="{{$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                                      <div class="post__meta">
                                                         @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                                            <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time>
                                                         @endif
                                                      </div>
                                                   </div>
                                                </article>
                                             </li>
                                          @elseif($indexAr > 1)
                                             <li>
                                                <article class="cat-2">
                                                   <h3 class="post__title typescale-0"><a href="{{$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                                </article>
                                             </li>
                                          @endif
                                       @endforeach
                                    </ul>
                              </div>
                           @endif
                        @endforeach
                     </div>
                  </div>
                  {{-- Video --}}
                  @if ($listArticleVideo && !$listArticleVideo->isEmpty())
                  <div class="mnmd-block">
                     <div class="block-heading block-heading--line-under">
                        <h4 class="block-heading__title">Videos</h4>
                        <a href="{{route('web.video')}}" class="block-heading__secondary">Tất cả Videos<i class="mdicon mdicon-arrow_forward mdicon--last"></i></a>
                     </div>
                     <div class="row row--space-between">
                        <div class="col-xs-12">
                           @foreach ($listArticleVideo as $arIndex=>$article)
                              @if ($arIndex == 0)
                                 <article class="post--overlay post--overlay-bottom post--overlay-floorfade post--overlay-sm cat-2">
                                    <div class="background-img" style="background-image: url({{MediaHelper::show($article->picture)}})"></div>
                                    <div class="post__text inverse-text">
                                       <div class="post__text-wrap">
                                          <div class="post__text-inner">
                                             <div class="media">
                                                <div class="media-left media-middle post-type-icon post-type-icon--md"><i class="mdicon mdicon-play_circle_outline"></i></div>
                                                <div class="media-body media-middle">
                                                   <h3 class="post__title typescale-3">{{$article->title}}</h3>
                                                   <div class="post__meta">
                                                      @if (isset($configs) && !empty($configs['SHOW_VIEW_COUNT']))
                                                         <span><i class="mdicon mdicon-visibility"></i>{{$article->view_count}}</span> 
                                                      @endif
                                                      @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                         <span><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}}</span>
                                                      @endif
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    @if($article->article_type == 2)
                                       <div class="overlay-item gallery-icon"><i class="mdicon mdicon-filter"></i><span>{{$article->media_count}}</span></div>
                                    @endif
                                    <a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}" class="link-overlay"></a> <a href="{{route('web.category',['categorySlug'=>$article->ct_slug])}}" class="post__cat post__cat--bg cat-theme-bg overlay-item--top-left">{{$article->ct_name}}</a>
                                 </article>
                              @endif
                           @endforeach
                        </div>
                        <div class="col-xs-12">
                           <div class="row row--space-between">
                              @foreach ($listArticleVideo as $arIndex=>$article)
                                 @if ($arIndex > 0)
                                    <div class="col-xs-12 col-sm-4">
                                       <article class="post--vertical cat-2">
                                          <div class="post__thumb">
                                             <a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}">
                                                <img src="{{MediaHelper::show($article->picture)}}">
                                                <div class="overlay-item--center-xy post-type-icon"><i class="mdicon mdicon-play_circle_outline"></i></div>
                                             </a>
                                          </div>
                                          <div class="post__text">
                                             <h3 class="post__title typescale-1"><a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}">{{$article->title}}</a></h3>
                                             <div class="post__meta">
                                                @if (isset($configs) && !empty($configs['SHOW_VIEW_COUNT']))
                                                   <span><i class="mdicon mdicon-visibility"></i>{{$article->view_count}}</span> 
                                                @endif
                                                @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                   <span><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}}</span>
                                                @endif
                                             </div>
                                          </div>
                                       </article>
                                    </div>
                                 @endif
                              @endforeach
                           </div>
                        </div>
                     </div>
                  </div>
                  @endif
                  
                  {{-- Block 3 column --}}
                  <div class="mnmd-block">
                     <div class="row row--space-between">
                        @foreach ($listArticleByCate as $indexCate=>$listCateItem)
                           @if($indexCate == 5 || $indexCate == 6 || $indexCate == 7)
                           <div class="col-xs-12 col-md-4">
                                 <div class="block-heading">
                                    <h4 class="block-heading__title"><a href="{{route('web.category',['categorySlug'=>$listCateItem['category']['slug']])}}">{{$listCateItem['category']['name']}}</a></h4>
                                 </div>
                                 @foreach ($listCateItem['listArticle'] as $indexAr =>$article)
                                    @if ($indexAr == 0)
                                       <article class="post post--vertical cat-6">
                                          <div class="post__thumb"><a href="{{$article->ct_slug.'/'.$article->slug}}"><img src="{{ MediaHelper::show($article->picture) }}"></a></div>
                                          <div class="post__text">
                                             <h3 class="post__title typescale-1"><a href="{{$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                             <div class="post__meta">
                                                @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                                   <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time>
                                                @endif
                                             </div>
                                          </div>
                                       </article>
                                    @endif
                                 @endforeach
                                 <div class="spacer-sm"></div>
                                 <ul class="list-space-xs list-unstyled list-seperated list-square-bullet">
                                       @foreach ($listCateItem['listArticle'] as $indexAr =>$article)
                                          @if ($indexAr == 1)
                                             <li>
                                                <article class="post--list cat-4">
                                                   <div class="post__text">
                                                      <h3 class="post__title typescale-0"><a href="{{$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                                   </div>
                                                </article>
                                             </li>
                                          @elseif($indexAr > 1)
                                             <li>
                                                <article class="post--list cat-4">
                                                   <div class="post__text">
                                                      <h3 class="post__title typescale-0"><a href="{{$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                                   </div>
                                                </article>
                                             </li>
                                          @endif
                                       @endforeach
                                 </ul>
                              </div>
                           @endif
                        @endforeach
                     </div>
                  </div>
                  {{-- Galleries --}}
                  @if ($listArticleGallery && !$listArticleGallery->isEmpty())
                  <div class="mnmd-block">
                        <div class="block-heading">
                           <h4 class="block-heading__title">Thư viện ảnh</h4>
                           <a href="{{route('web.gallery')}}" class="block-heading__secondary">Xem tất cả<i class="mdicon mdicon-arrow_forward mdicon--last"></i></a>
                        </div>
                        <div class="row row--space-between">
                           @foreach ($listArticleGallery as $arIndex=>$article)
                               @if ($arIndex == 0)
                                 <div class="col-xs-12">
                                    <article class="post--overlay post--overlay-bottom post--overlay-floorfade post--overlay-sm cat-4">
                                       <div class="background-img" style="background-image: url({{MediaHelper::show($article->picture)}})"></div>
                                       <div class="post__text inverse-text">
                                          <div class="post__text-wrap">
                                             <div class="post__text-inner">
                                                <h3 class="post__title typescale-3">{{$article->title}}</h3>
                                                <div class="post__meta">
                                                   @if (isset($configs) && !empty($configs['SHOW_VIEW_COUNT']))
                                                      <span><i class="mdicon mdicon-visibility"></i>{{$article->view_count}}</span> 
                                                   @endif
                                                   @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                      <span><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}}</span>
                                                   @endif
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="overlay-item gallery-icon"><i class="mdicon mdicon-filter"></i><span>{{$article->media_count}}</span></div>
                                       <a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}" class="link-overlay"></a> 
                                       <a href="{{route('web.category',['categorySlug'=>$article->ct_slug])}}" class="post__cat post__cat--bg cat-theme-bg overlay-item--top-left">{{$article->ct_name}}</a>
                                    </article>
                                 </div>
                               @endif
                           @endforeach
                           
                           <div class="col-xs-12">
                              <div class="row row--space-between">
                                 @foreach ($listArticleGallery as $arIndex=>$article)
                                     @if ($arIndex > 0)
                                       <div class="col-xs-12 col-sm-3">
                                          <article class="post--vertical cat-2">
                                             <div class="post__thumb">
                                                <a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}">
                                                   <img src="{{MediaHelper::show($article->picture)}}">
                                                   <div class="overlay-item overlay-item--sm-p overlay-item--left-bottom gallery-icon">
                                                      <i class="mdicon mdicon-filter"></i>
                                                      <span>{{$article->media_count}}</span>
                                                   </div>
                                                </a>
                                             </div>
                                             <div class="post__text">
                                                <h3 class="post__title typescale-0"><a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}">{{$article->title}}</a></h3>
                                                <div class="post__meta">
                                                   @if (isset($configs) && !empty($configs['SHOW_VIEW_COUNT']))
                                                      <span><i class="mdicon mdicon-visibility"></i>{{$article->view_count}}</span> 
                                                   @endif
                                                   @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                      <span><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}}</span>
                                                   @endif
                                             </div>
                                          </article>
                                       </div>
                                     @endif
                                 @endforeach
                              </div>
                           </div>
                        </div>
                     </div> 
                     {{-- End Galleries --}}
                  @endif
               </div>
               {{-- end main col --}}
               @include('web.shared.sidebar')
               {{-- .mnmd-sub-col --}}
            </div>
         </div>
      </div>
      {{-- .mnmd-block --}}
   </div>
@endsection
