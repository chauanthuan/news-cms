<html>
<head>
<title>Xác nhận đăng ký tài khoản tại {{$website}}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width" />
</head>
<body style="margin:0; padding:0;font-family:Tahoma, Arial, sans-serif;">
	<table width="600px" cellpadding="0" cellspacing="0" border="0" bgcolor="#fff" style=" margin: 0 auto;">
		<tr>
			<td>
				<p style="text-align:center;padding:15px 0"><img src="http://hueinfo.com/images/logo.png" style="height:35px;"></p>
			</td>
		</tr>
		<tr>
			<td style="height:50px;"></td>
		</tr>
		<tr>
			<td>
				<h3 style="font-size: 16px;color: #4A4A4A;">Xin chào {{$name}}</h3>
                <p style="font-size: 14px;color: #333333;">Cám ơn bạn đã đăng ký thành viên tại <a href="{{$weblink}}">{{$website}}</a>.</p>
				<p style="font-size: 14px;color: #333333;">Để hoàn tất quá trình đăng kí, vui lòng nhấn vào <a href='{{ url($weblink."/auth/register/confirm/{$token}") }}'>liên kết này</a> để xác nhận tài khoản của bạn!</p>
				<p>Nếu bạn đang gặp sự cố khi nhấp vào liên kết. Hãy sao chép và dán URL dưới đây vào trình duyệt web của bạn: {{ url($weblink."/auth/register/confirm/{$token}") }}</p>
			</td>
		</tr>
		<tr>
			<td style="height:30px;"></td>
		</tr>
		<tr>
			<td>
				<p>Trân trọng</p>
			</td>
		</tr>
		
		<tr>
			<td style="font-size: 10px;color: #BDBDBD;text-align:center;padding:20px 0 10px;background:#FAFAFA">
				<p style="line-height: 15px;">Copyright © 2019 {{$website}}. All rights reserved. <br>
			</td>
		</tr>
	</table>
</body>
</html>
