<!DOCTYPE html>
<html dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    {{-- Tell the browser to be responsive to screen width --}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    {{-- Favicon icon --}}
    <link rel="icon" href="{{asset('web/img/icon.png')}}" sizes="32x32">
    <link rel="icon" href="{{asset('web/img/icon.png')}}" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="{{asset('web/img/icon.png')}}">
    <title>{{ isset($configs) && !empty($configs['SITE_TITLE']) ? $configs['SITE_TITLE'] : env('SITE_TITLE','Viet24.vn - Tin nhanh, thông tin kinh tế, văn hóa, xã hội... cập nhật liên tục 24h') }}</title>
    {{-- Custom CSS --}}
    <link href="{{asset('admin/dist/css/style.min.css')}}" rel="stylesheet">
    {{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
    {{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    #loginform .logo img{
        height: 60px;;
    }
    #loginform .logo .font-medium{
        margin: 20px 0;
    }
    *{
        font-family: SFProtext, Arial, Helvetica, sans-serif;
    }
    </style>
</head>

<body>
    <div class="main-wrapper">
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <div class="auth-wrapper d-flex no-block justify-content-center align-items-center" style="background:url({{asset('web/img/auth-bg.jpg')}}) no-repeat center center;">
            <div class="auth-box">
                @yield('content')
            </div>
        </div>
       
    </div>
    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <script>
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    </script>
</body>

</html>