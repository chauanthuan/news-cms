@extends('web.auth.base')
@section('content')
    <div id="loginform">
        <div class="logo">
                <span class="db"><a href="{{route('web.index')}}"><img src="{{asset('web/img/logo.png')}}" alt="logo"/></a></span>
            <h3 class="font-medium m-b-20">Đăng ký</h3>
        </div>
        <!-- Form -->
        <div class="row">
            <div class="col-12">
                @if(isset($errors) && !is_array($errors) && $errors->all())
                    <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <ul style="padding: 0px;list-style: none;">
                            @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('status'))
                    <div class="col-md-12">
                        <div class="alert bg-success alert-dismissible mb-2">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        {{ session('status') }}
                        </div>
                    </div>
                @endif
                <form class="form-horizontal m-t-20" id="loginform" method="POST" action="{{route('web.auth.register')}}">
                    {{ csrf_field() }}
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="ti-id-badge"></i></span>
                        </div>
                        <input type="text" name="name" value="{{Input::old('name')}}" class="form-control form-control-lg" placeholder="Họ và tên" aria-label="Name" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="ti-email"></i></span>
                        </div>
                        <input type="text" name="email" value="{{Input::old('email')}}" class="form-control form-control-lg" placeholder="Địa chỉ email" aria-label="email" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="ti-user"></i></span>
                        </div>
                        <input type="text" name="username" value="{{Input::old('username')}}" class="form-control form-control-lg" placeholder="Tên đăng nhập" aria-label="Username" aria-describedby="basic-addon1">
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon2"><i class="ti-pencil"></i></span>
                        </div>
                        <input type="password" name="password" value="{{Input::old('password')}}" class="form-control form-control-lg" placeholder="Mật khẩu" aria-label="Password" aria-describedby="basic-addon1">
                    </div>
                    
                    <div class="form-group text-center">
                        <div class="col-xs-12 p-b-20">
                            <button class="btn btn-block btn-lg btn-info" type="submit">Đăng ký</button>
                        </div>
                    </div>
                    {{-- <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social">
                                <a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="" data-original-title="Login with Facebook"> <i aria-hidden="true" class="fab  fa-facebook"></i> </a>
                                <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="" data-original-title="Login with Google"> <i aria-hidden="true" class="fab  fa-google-plus"></i> </a>
                            </div>
                        </div>
                    </div> --}}
                    <div class="form-group m-b-0 m-t-10">
                        <div class="col-sm-12 text-center">
                            Bạn đã có tài khoản? <a href="{{route('web.auth.login')}}" class="text-info m-l-5"><b>Đăng nhập</b></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
               