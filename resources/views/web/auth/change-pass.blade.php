@extends('web.layouts.base')
@section('content')
    <div class="site-content">
        <div class="container">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <div class="row">
                <!-- Column -->
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <center class="m-t-30"> <img src="{{asset('web/img/users/avatar.png')}}" class="rounded-circle" width="150" />
                                <h4 class="card-title m-t-10">{{isset($profile) && !empty($profile) && $profile->gender == 1 ? 'Mr':'Ms'}} {{isset(\Auth::user()->name) && !empty(\Auth::user()->name) ? \Auth::user()->name : '@User'}}</h4>
                               
                                @if (isset($profile->company))
                                    <h6 class="card-subtitle">{{$profile->company}}</h6>
                                @endif
                                <p>Thành viên: {{Helpers::convertDate(\Auth::user()->created_at,'d/m/Y')}}</p>
                            </center>
                        </div>
                        <div><hr></div>
                        <div class="card-body"> 
                            <small class="text-muted">Địa chỉ email:</small>
                            <h6>{{\Auth::user()->email}}</h6> 
                            @if (isset($profile->phone))
                                <small class="text-muted p-t-30 db">Số điện thoại</small>
                                <h6>{{$profile->phone}}</h6> 
                            @endif
                            
                            @if (isset($profile->address))
                                <small class="text-muted p-t-30 db">Địa chỉ</small>
                                <h6>{{$profile->address}}</h6>
                            @endif
                            
                            @if (isset($profile->facebook_url) || isset($profile->twitter_url) || isset($profile->linkin_url))
                                <small class="text-muted p-t-30 db">Mạng xã hội</small>
                                <br/><br/>
                                @if ($profile->facebook_url)
                                    <a target="_blank" href="{{$profile->facebook_url}}" class="btn-circle btn-secondary"><i class="mdicon mdicon-facebook"></i></a>
                                @endif
                                @if ($profile->twitter_url)
                                    <a target="_blank" href="{{$profile->twitter_url}}" class="btn-circle btn-secondary"><i class="mdicon mdicon-twitter"></i></a>
                                @endif 
                                @if ($profile->linkin_url)
                                    <a target="_blank" href="{{$profile->linkin_url}}" class="btn-circle btn-secondary"><i class="mdicon mdicon-linkedin"></i></a>
                                @endif
                            @endif
                            
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <h3>Thay đổi mật khẩu</h3>
                            @if (session('status'))
                                <div class="col-md-12">
                                    <div class="alert bg-success alert-dismissible mb-2">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    {{ session('status') }}
                                    </div>
                                </div>
                            @endif
                            @if(isset($errors) && !is_array($errors) && $errors->all())
                                <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <ul style="padding: 0px;list-style: none;">
                                        @foreach($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            
                            <form class="form-horizontal form-material" method="POST" action="{{route('web.auth.changepass')}}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="password" class="col-md-12 col-form-label">Mật khẩu</label>
                                        <div class="col-md-12">
                                        <input type="password" placeholder="Mật khẩu mới" class="form-control form-control-line" name="password" id="password" value="{{Input::old('password')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                            <label for="confirm-password" class="col-md-12 col-form-label">Xác nhận mật khẩu</label>
                                            <div class="col-md-12">
                                                <input type="password" placeholder="Xác nhận Mật khẩu mới" class="form-control form-control-line" name="password_confirmation" id="confirm-password" value="{{Input::old('password_confirmation')}}">
                                            </div>
                                        </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-success">Đổi mật khẩu</button>
                                        </div>
                                    </div>
                                </form>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div>
    </div>
@endsection