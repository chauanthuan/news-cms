@extends('web.auth.base')
@section('content')
    @if ($validateUser)
    {{-- Nêu user pass qua bước confirm được gửi từ email thì show cái này, ko thì show form dưới --}}
        <div id="loginform">
            <div class="logo">
                <span class="db"><a href="{{route('web.index')}}"><img src="{{asset('web/img/logo.png')}}" alt="logo"/></a></span>
                <h3 class="font-medium m-b-20">Lấy lại mật khẩu</h3>
                <span>Điền mật khẩu mới của bạn!</span>
            </div>
            <div class="row">
                <div class="col-12">
                    @if(isset($errors) && !is_array($errors) && $errors->all())
                        <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul style="padding: 0px;list-style: none;">
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="col-md-12">
                            <div class="alert bg-success alert-dismissible mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('status') }}
                            </div>
                        </div>
                    @endif
                    <form class="form-horizontal m-t-20" id="loginform" method="POST" action="{{route('web.auth.resetpassconfirm',['token'=>$token])}}">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control form-control-lg" type="password" required="" placeholder="Mật khẩu mới" name="password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control form-control-lg" type="password" required="" placeholder="Xác nhận lại mật khẩu" name="password_confirmation">
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-20">
                                <button class="btn btn-block btn-lg btn-info" type="submit">Tạo mật khẩu mới</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @else
        <div id="loginform">
            <div class="logo">
                <span class="db"><a href="{{route('web.index')}}"><img src="{{asset('web/img/logo.png')}}" alt="logo" height="60px"/></a></span>
                <h3 class="font-medium m-b-20">Lấy lại mật khẩu</h3>
                <span>Vui lòng điền địa chỉ email của bạn, và chúng tôi sẽ gửi thông tin đến địa chỉ email này!</span>
            </div>
            <div class="row">
                <div class="col-12">
                    @if(isset($errors) && !is_array($errors) && $errors->all())
                        <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            <ul style="padding: 0px;list-style: none;">
                                @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if (session('status'))
                        <div class="col-md-12">
                            <div class="alert bg-success alert-dismissible mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('status') }}
                            </div>
                        </div>
                    @endif
                    <form class="form-horizontal m-t-20" id="loginform" method="POST" action="{{route('web.auth.resetpass')}}">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-12">
                                <input class="form-control form-control-lg" type="text" required="" placeholder="Tên của bạn" name="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12  p-b-20">
                                <input class="form-control form-control-lg" type="text" required="" placeholder="Địa chỉ email" name="email">
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <div class="col-xs-12 p-b-0">
                                <button class="btn btn-block btn-lg btn-info" type="submit">Lấy mật khẩu</button>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <p class="login-lost-password">
                                <a href="{{route('web.auth.login')}}" class="link link--darken">Đăng nhập</a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endif
@endsection