@extends('web.layouts.base')
@section('content')
    <div class="site-content">
        <div class="container">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            <!-- Row -->
            <div class="row">
                <!-- Column -->
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-body">
                            <center class="m-t-30"> <img src="{{asset('web/img/users/avatar.png')}}" class="rounded-circle" width="150" />
                                <h4 class="card-title m-t-10">{{isset($profile) && !empty($profile) && $profile->gender == 1 ? 'Mr':'Ms'}} {{isset(\Auth::user()->name) && !empty(\Auth::user()->name) ? \Auth::user()->name : '@User'}}</h4>
                               
                                @if (isset($profile->company))
                                    <h6 class="card-subtitle">{{$profile->company}}</h6>
                                @endif
                                <p>Thành viên: {{Helpers::convertDate(\Auth::user()->created_at,'d/m/Y')}}</p>
                            </center>
                        </div>
                        <div><hr></div>
                        <div class="card-body"> 
                            <small class="text-muted">Địa chỉ email:</small>
                            <h6>{{\Auth::user()->email}}</h6> 
                            @if (isset($profile->phone))
                                <small class="text-muted p-t-30 db">Số điện thoại</small>
                                <h6>{{$profile->phone}}</h6> 
                            @endif
                            
                            @if (isset($profile->address))
                                <small class="text-muted p-t-30 db">Địa chỉ</small>
                                <h6>{{$profile->address}}</h6>
                            @endif
                            
                            @if (isset($profile->facebook_url) || isset($profile->twitter_url) || isset($profile->linkin_url))
                                <small class="text-muted p-t-30 db">Mạng xã hội</small>
                                <br/><br/>
                                @if ($profile->facebook_url)
                                    <a target="_blank" href="{{$profile->facebook_url}}" class="btn-circle btn-secondary"><i class="mdicon mdicon-facebook"></i></a>
                                @endif
                                @if ($profile->twitter_url)
                                    <a target="_blank" href="{{$profile->twitter_url}}" class="btn-circle btn-secondary"><i class="mdicon mdicon-twitter"></i></a>
                                @endif 
                                @if ($profile->linkin_url)
                                    <a target="_blank" href="{{$profile->linkin_url}}" class="btn-circle btn-secondary"><i class="mdicon mdicon-linkedin"></i></a>
                                @endif
                            @endif
                            
                        </div>
                    </div>
                </div>
                <!-- Column -->
                <!-- Column -->
                <div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card">
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#timeline" aria-controls="timeline" role="tab" data-toggle="tab">Dòng thời gian</a>
                            </li>
                            <li role="presentation">
                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Thông tin cá nhân</a>
                            </li>
                        </ul>
                        
                        <!-- Tabs -->
                        <div class="tab-content">
                            @if (session('status'))
                                <div class="col-md-12">
                                    <div class="alert bg-success alert-dismissible mb-2">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    {{ session('status') }}
                                    </div>
                                </div>
                            @endif
                            <div role="tabpanel" class="tab-pane fade in active" id="timeline">
                                <div class="card-body">
                                    <div class="profiletimeline m-t-0">
                                        <p>Chưa có hoạt động nào gần đây</p>
                                        {{-- <div class="sl-item">
                                            <div class="sl-left"> <img src="{{asset('web/img/users/5.jpg')}}" alt="user" class="rounded-circle" /> </div>
                                            <div class="sl-right">
                                                <div><a href="javascript:void(0)" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                    <p>assign a new task <a href="javascript:void(0)"> Design weblayout</a></p>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-6 m-b-20"><img src="{{asset('web/img/users/5.jpg')}}" class="img-fluid rounded" /></div>
                                                        <div class="col-lg-3 col-md-6 m-b-20"><img src="{{asset('web/img/users/5.jpg')}}" class="img-fluid rounded" /></div>
                                                        <div class="col-lg-3 col-md-6 m-b-20"><img src="{{asset('web/img/users/5.jpg')}}" class="img-fluid rounded" /></div>
                                                        <div class="col-lg-3 col-md-6 m-b-20"><img src="{{asset('web/img/users/5.jpg')}}" class="img-fluid rounded" /></div>
                                                    </div>
                                                    <div class="like-comm"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="sl-item">
                                            <div class="sl-left"> <img src="{{asset('web/img/users/5.jpg')}}" alt="user" class="rounded-circle" /> </div>
                                            <div class="sl-right">
                                                <div> <a href="javascript:void(0)" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                    <div class="m-t-20 row">
                                                        <div class="col-md-3 col-xs-12"><img src="{{asset('web/img/users/5.jpg')}}" alt="user" class="img-fluid rounded" /></div>
                                                        <div class="col-md-9 col-xs-12">
                                                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. </p> <a href="javascript:void(0)" class="btn btn-success"> Design weblayout</a></div>
                                                    </div>
                                                    <div class="like-comm m-t-20"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="sl-item">
                                            <div class="sl-left"> <img src="{{asset('web/img/users/5.jpg')}}" alt="user" class="rounded-circle" /> </div>
                                            <div class="sl-right">
                                                <div><a href="javascript:void(0)" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                    <p class="m-t-10"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer nec odio. Praesent libero. Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nibh elementum imperdiet. Duis sagittis ipsum. Praesent mauris. Fusce nec tellus sed augue semper </p>
                                                </div>
                                                <div class="like-comm m-t-20"> <a href="javascript:void(0)" class="link m-r-10">2 comment</a> <a href="javascript:void(0)" class="link m-r-10"><i class="fa fa-heart text-danger"></i> 5 Love</a> </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="sl-item">
                                            <div class="sl-left"> <img src="{{asset('web/img/users/5.jpg')}}" alt="user" class="rounded-circle" /> </div>
                                            <div class="sl-right">
                                                <div><a href="javascript:void(0)" class="link">John Doe</a> <span class="sl-date">5 minutes ago</span>
                                                    <blockquote class="m-t-10">
                                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt
                                                    </blockquote>
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                <div class="card-body">
                                    <form class="form-horizontal form-material" method="POST" action="{{route('web.auth.profile')}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" value="{{isset($profile->id) ? $profile->id:''}}" name="profileid">
                                        <div class="form-group">
                                            <label class="col-md-12 col-form-label">Họ và tên</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Họ và tên" class="form-control form-control-line" value="{{\Auth::user()->name}}" name="name">
                                            </div>
                                        </div>
                                        {{-- <div class="form-group">
                                            <label for="email" class="col-md-12 col-form-label">Email</label>
                                            <div class="col-md-12">
                                                <input type="email" placeholder="Địa chỉ email" class="form-control form-control-line" name="email" id="email" value="{{\Auth::user()->email}}" readonly>
                                            </div>
                                        </div> --}}
                                        @php
                                            // dd($profile);
                                        @endphp
                                        <div class="form-group">
                                            <label class="col-md-12 col-form-label">Giới tính</label>
                                            <div class="col-md-12">
                                                <div class="form-check form-check-inline">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" id="genderMale" value="1" checked name="gender" {{ isset($profile) && !empty($profile) && $profile->gender == 1 ? 'checked':''}}>
                                                        <label class="custom-control-label" for="genderMale">Nam</label>
                                                    </div>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" class="custom-control-input" id="genderFemale" value="0" name="gender" {{ isset($profile) && !empty($profile) && $profile->gender == 0 ? 'checked':''}}>
                                                        <label class="custom-control-label" for="genderFemale">Nữ</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12 col-form-label">Sinh nhật</label>
                                            <div class="col-md-12">
                                                <input type="date" value="{{isset($profile->birthday) ? $profile->birthday : ''}}" class="form-control form-control-line" name="birthday">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12 col-form-label">Số điện thoại</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Số điện thoại" class="form-control form-control-line" name="phone" value="{{isset($profile->phone) ? $profile->phone : ''}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12 col-form-label">Địa chỉ</label>
                                            <div class="col-md-12">
                                                <textarea rows="5" class="form-control form-control-line" name="address">{{isset($profile->address) ? $profile->address : ''}}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12 col-form-label">Công ty</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Công ty" class="form-control form-control-line" name="company" value="{{isset($profile->company) ? $profile->company : ''}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                                <label class="col-md-12 col-form-label">Facebook</label>
                                                <div class="col-md-12">
                                                    <input type="text" placeholder="Facebook URL" class="form-control form-control-line" name="facebook_url" value="{{isset($profile->facebook_url) ? $profile->facebook_url : ''}}">
                                                </div>
                                            </div>
                                        <div class="form-group">
                                            <label class="col-md-12 col-form-label">Twitter</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Twitter URL" class="form-control form-control-line" name="twiter_url" value="{{isset($profile->twitter_url) ? $profile->twitter_url : ''}}">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12 col-form-label">Linkedin</label>
                                            <div class="col-md-12">
                                                <input type="text" placeholder="Linkedin URL" class="form-control form-control-line" name="linkin_url" value="{{isset($profile->linkin_url) ? $profile->linkin_url : ''}}">
                                            </div>
                                        </div>
                                                
                                        <div class="form-group">
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-success">Lưu thông tin</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                       
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div>
    </div>
@endsection