@if ($most_comment_article->count() > 0)
    <div class="mnmd-widget-indexed-posts-c mnmd-widget widget">
        <div class="widget__title">
            <h4 class="widget__title-text"><span class="first-word">Bài viết nhiều bình luận</h4>
            <div class="widget__title-seperator"></div>
        </div>
        <ol class="posts-list list-space-md list-seperated-exclude-first list-unstyled">
            @foreach ($most_comment_article as $index=>$article)
                @if ($index == 0)
                    <li>
                        <article class="post post--overlay post--overlay-bottom cat-4">
                        <div class="background-img background-img--darkened" style="background-image: url({{ MediaHelper::show($article->picture) }})"></div>
                        <div class="post__text inverse-text">
                            <div class="post__text-inner">
                                <div class="media">
                                <div class="media-left media-middle"><span class="list-index">{{$index + 1}}</span></div>
                                    <div class="media-body media-middle">
                                    <h3 class="post__title typescale-1">{{$article->title}}</h3>
                                    <div class="post__meta"><a href="{{'/'.$article->ct_slug}}" class="post__cat post__cat--bg cat-theme-bg">{{$article->ct_name}}</a> <a href="{{'/'.$article->ct_slug.'/'.$article->slug}}" class="post__comments"><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}} bình luận</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <a href="{{'/'.$article->ct_slug.'/'.$article->slug}}" class="link-overlay"></a>
                        </article>
                    </li>
                @else
                    <li>
                        <article class="post cat-5">
                        <div class="media">
                            <div class="media-left media-middle"><span class="list-index">{{$index + 1}}</span></div>
                            <div class="media-body media-middle">
                                <h3 class="post__title typescale-0"><a href="{{'/'.$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                            <div class="post__meta"><a href="{{'/'.$article->ct_slug}}" class="post__cat post__cat--bg cat-theme-bg">{{$article->ct_name}}</a> <a href="{{'/'.$article->ct_slug.'/'.$article->slug}}" class="post__comments"><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}} bình luận</a></div>
                            </div>
                        </div>
                        </article>
                    </li>
                @endif
            @endforeach
        </ol>
    </div> 
@endif