<div class="mnmd-sub-col js-sticky-sidebar" role="complementary" style="position: relative; overflow: visible; box-sizing: border-box; min-height: 1px;">
    
    <div class="theiaStickySidebar" style="padding-top: 0px; padding-bottom: 1px; position: static;">
        {{-- ads --}}
        @include('web.shared.widget_ads_sidebar')
        {{-- Popular article --}}
        @include('web.shared.widget_popular_article')
        {{-- Most Comments --}}
        @include('web.shared.widget_most_comment')
        {{-- Social widget --}}
        {{-- @include('web.shared.widget_social') --}}
    </div>
</div>