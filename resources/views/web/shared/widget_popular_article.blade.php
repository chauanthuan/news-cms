@if ($popular_article->count() > 0)
    <div class="mnmd-widget-indexed-posts-c mnmd-widget widget">
        <div class="widget__title">
            <h4 class="widget__title-text">Được quan tâm</h4>
        </div>
        <ol class="posts-list list-space-md list-seperated-exclude-first list-unstyled">
            @foreach ($popular_article as $key=>$article)
                @if ($key == 0)
                    <li>
                        <article class="post post--overlay post--overlay-bottom cat-4">
                            <div class="background-img background-img--darkened" style="background-image: url({{ MediaHelper::show($article->picture) }})"></div>
                            <div class="post__text inverse-text">
                                <div class="post__text-inner">
                                    <div class="media">
                                    <div class="media-left media-middle"><span class="list-index">{{$key + 1 }}</span></div>
                                    <div class="media-body media-middle">
                                        <h3 class="post__title typescale-2">{{$article->title}}</h3>
                                        <div class="post__meta"><a href="{{'/'.$article->ct_slug}}" class="post__cat post__cat--bg cat-theme-bg">{{$article->ct_name}}</a> 
                                            @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                <a href="#"><i class="mdicon mdicon-chat_bubble_outline"></i>68 comments</a>
                                            @endif
                                        </div> 
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <a href="{{'/'.$article->ct_slug.'/'.$article->slug}}" class="link-overlay"></a>
                        </article>
                        </li>
                @else
                    <li>
                        <article class="post cat-5">
                            <div class="media">
                                <div class="media-left media-middle"><span class="list-index">{{$key + 1 }}</span></div>
                                <div class="media-body media-middle">
                                    <h3 class="post__title typescale-0"><a href="{{'/'.$article->ct_slug.'/'.$article->slug}}">{{$article->title}}</a></h3>
                                    <div class="post__meta"><a href="{{'/'.$article->ct_slug}}" class="post__cat post__cat--bg cat-theme-bg">{{$article->ct_name}}</a> 
                                    @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                        <a href="#"><i class="mdicon mdicon-chat_bubble_outline"></i>68 comments</a>
                                    @endif
                                </div>
                                </div>
                            </div>
                        </article>
                        </li>
                @endif
            @endforeach
            
            
        </ol>
        </div>
    
@endif