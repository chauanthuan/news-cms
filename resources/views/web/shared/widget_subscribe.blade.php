<div class="mnmd-widget widget widget-subscribe widget-subscribe--stack-bottom">
    <div class="widget-subscribe__inner">
        <div class="subscribe-form subscribe-form--center">
            <p><b class="typescale-3">Đừng bỏ lỡ!</b></p>
            <p>Nhập email của bạn để nhận tin mới mỗi ngày</p>
            <div class="subscribe-form__fields">
                <p><label>Địa chỉ email:</label><input type="email" name="email" placeholder="Email của bạn" required=""></p>
                <p><input type="submit" value="Subscribe" class="btn-block"></p>
            </div>
            <small>Đừng lo lắng, chúng tôi không phải spam!</small>
        </div>
    </div>
</div>