{{-- Site header --}}
<header class="site-header site-header--skin-1" id="my-header">
   {{-- Header content --}}
   <div class="header-main header-main--inverse hidden-xs hidden-sm">
      <div class="container">
         <div class="row row--flex row--vertical-center">
            <div class="col-xs-8">
               <div class="site-logo header-logo text-left"><a href="/"><img src="{{ asset('web/img/logo-white.png') }}" alt="logo" width="200"></a></div>
            </div>
            <div class="col-xs-4">
               <div class="site-header__social inverse-text">
                  <ul class="social-list social-list--lg list-horizontal text-right">
                     <li><a href="{{isset($configs) && !empty($configs['FACEBOOK_URL']) ? $configs['FACEBOOK_URL']:'#'}}"><i class="mdicon mdicon-facebook"></i></a></li>
                     <li><a href="{{isset($configs) && !empty($configs['TWITTER_URL']) ? $configs['TWITTER_URL']:'#'}}"><i class="mdicon mdicon-twitter"></i></a></li>
                     <li><a href="{{isset($configs) && !empty($configs['YOUTUBE_URL']) ? $configs['YOUTUBE_URL']:'#'}}"><i class="mdicon mdicon-youtube"></i></a></li>
                     <li><a href="{{isset($configs) && !empty($configs['GOOGLE_PLUS_URL']) ? $configs['GOOGLE_PLUS_URL']:'#'}}"><i class="mdicon mdicon-google-plus"></i></a></li>
                  </ul>
               </div>
            </div>
         </div>
      </div>
   </div>
   {{-- Header content --}}
   {{-- Mobile header --}}
   <div id="mnmd-mobile-header" class="mobile-header mobile-header--inverse visible-xs visible-sm">
      <div class="mobile-header__inner mobile-header__inner--flex">
         <div class="header-branding header-branding--mobile mobile-header__section text-left">
            <div class="header-logo header-logo--mobile flexbox__item text-left"><a href="/"><img src="{{ asset('web/img/logo-white.png') }}" alt="logo"></a></div>
         </div>
         <div class="mobile-header__section text-right">
            <button type="submit" class="mobile-header-btn js-search-dropdown-toggle">
               <span class="hidden-xs">Search</span>
               <i class="mdicon mdicon-search mdicon--last hidden-xs"></i>
               <i class="mdicon mdicon-search visible-xs-inline-block"></i>
               </button> 
               <a href="#mnmd-offcanvas-primary" class="offcanvas-menu-toggle mobile-header-btn js-mnmd-offcanvas-toggle">
                  <span class="hidden-xs">Menu</span>
                  <i class="mdicon mdicon-menu mdicon--last hidden-xs"></i>
                  <i class="mdicon mdicon-menu visible-xs-inline-block"></i>
               </a>
            </div>
      </div>
   </div>
   {{-- Mobile header --}}
   {{-- Navigation bar --}}
   <nav class="navigation-bar navigation-bar--inverse navigation-bar--fullwidth hidden-xs hidden-sm js-sticky-header-holder">
      <div class="container  container--wide">
         <div class="navigation-bar__inner">
               {{-- <div class="navigation-bar__section">
               <a href="#mnmd-offcanvas-primary" class="offcanvas-menu-toggle navigation-bar-btn js-mnmd-offcanvas-toggle">
                  <i class="mdicon mdicon-menu"></i>
                  </a>
               </div> --}}
            <div class="navigation-wrapper navigation-bar__section js-priority-nav">
               <ul id="menu-main-menu" class="navigation navigation--main navigation--inline">
                  <li class="">
                     <a href="/">Trang chủ</a>
                  </li>
                  @foreach ($all_category as $item)
                     <li class="menu-item-cat-{{$item->id}}">
                     <a href="{{'/'.$item->slug}}">{{$item->name}}</a>
                     </li>
                  @endforeach
               </ul>
            </div>
            <div class="navigation-bar__section">
               @if (\Auth::user())
                  @php
                     $role = \Auth::user()->roles;
                     $role_name = isset($role) && !$role->isEmpty() ? $role[0]->slug : '';
                  @endphp
                  <ul id="" class="navigation navigation--main navigation--inline nav-account">
                     <li class="menu-item-has-children">
                        <a href="#">Xin chào {{!empty(\Auth::user()->name) ? \Auth::user()->name : 'Bạn'}}</a>
                        <ul class="sub-menu">
                           @if (in_array($role_name, ['supper-admin','manager','editor']))
                              <li><a target="_blank" href="{{route('cms.dashboard')}}">Vào CMS</a></li>
                           @endif
                           
                           <li><a href="#">Hoạt động bình luận</a></li>
                           <li><a href="{{route('web.auth.profile')}}">Chỉnh sửa tài khoản</a></li>
                           <li><a href="{{route('web.auth.changepass')}}">Đổi mật khẩu</a></li>
                           <li><a href="{{route('web.auth.logout')}}">Thoát</a></li>
                        </ul>
                     </li>
                  </ul>
               @else
                  <a href="#login-modal" class="navigation-bar__login-btn navigation-bar-btn" data-toggle="modal" data-target="#login-modal">
                     <i class="mdicon mdicon-person"></i>
                  </a> 
               @endif
               
               <button type="submit" class="navigation-bar-btn js-search-dropdown-toggle">
                  <i class="mdicon mdicon-search"></i>
               </button>
            </div>
         </div>
         {{-- .navigation-bar__inner --}}
         <div id="header-search-dropdown" class="header-search-dropdown ajax-search is-in-navbar js-ajax-search">
            <div class="container container--narrow">
               <form class="search-form search-form--horizontal" method="get" action="{{route('web.seach')}}">
               <div class="search-form__input-wrap">
                  {!! Form::text('s', '', ['class'=>'search-form__input', 'placeholder'=>'Tên bài viết ...'])!!}
               </div>
                  <div class="search-form__submit-wrap"><button type="submit" class="search-form__submit btn btn-primary">Tìm kiếm</button></div>
               </form>
               <div class="search-results">
                  <div class="typing-loader"></div>
                  <div class="search-results__inner"></div>
               </div>
            </div>
         </div>
         {{-- .header-search-dropdown --}}
      </div>
      {{-- .container --}}
   </nav>
   {{-- Navigation-bar --}}
</header>
{{-- Site header --}}

<div id="mnmd-sticky-header" class="sticky-header site-header--skin-1 js-sticky-header">
   {{-- Navigation bar --}}
   <nav class="navigation-bar navigation-bar--inverse navigation-bar--fullwidth hidden-xs hidden-sm">
      <div class="navigation-bar__inner">
         {{-- <div class="navigation-bar__section">
            <a href="#mnmd-offcanvas-primary" class="offcanvas-menu-toggle navigation-bar-btn js-mnmd-offcanvas-toggle"><i class="mdicon mdicon-menu icon--2x"></i></a>
            <div class="site-logo header-logo"><a href="#"><img src="{{ asset('web/img/logo-mark-color.png') }}" alt="logo"></a></div>
         </div> --}}
         <div class="navigation-wrapper navigation-bar__section js-priority-nav">
            <ul id="menu-main-menu-1" class="navigation navigation--main navigation--inline">
               <li class="">
                  <a href="/">Trang chủ</a>
               </li>
               @foreach ($all_category as $item)
                  <li class="menu-item-cat-{{$item->id}}">
                  <a href="{{'/'.$item->slug}}">{{$item->name}}</a>
                  </li>
               @endforeach
            </ul>
         </div>
         <div class="navigation-bar__section">
            @if (\Auth::user())
               @php
                  $role = \Auth::user()->roles;
                  $role_name = isset($role) && !$role->isEmpty() ? $role[0]->slug : '';
               @endphp
               <ul id="" class="navigation navigation--main navigation--inline nav-account">
                  <li class="menu-item-has-children">
                     <a href="#">Xin chào {{!empty(\Auth::user()->name) ? \Auth::user()->name : 'Bạn'}}</a>
                     <ul class="sub-menu">
                        @if (in_array($role_name, ['supper-admin','manager','editor']))
                           <li><a target="_blank" href="{{route('cms.dashboard')}}">Vào CMS</a></li>
                        @endif
                        
                        <li><a href="#">Hoạt động bình luận</a></li>
                        <li><a href="{{route('web.auth.profile')}}">Chỉnh sửa tài khoản</a></li>
                        <li><a href="{{route('web.auth.changepass')}}">Đổi mật khẩu</a></li>
                        <li><a href="{{route('web.auth.logout')}}">Thoát</a></li>
                     </ul>
                  </li>
               </ul>
            @else
               <a href="#login-modal" class="navigation-bar__login-btn navigation-bar-btn" data-toggle="modal" data-target="#login-modal">
                  <i class="mdicon mdicon-person"></i>
               </a> 
            @endif
            <button type="submit" class="navigation-bar-btn js-search-dropdown-toggle">
               <i class="mdicon mdicon-search"></i>
            </button>
         </div>
      </div>
      {{-- .navigation-bar__inner --}}
   </nav>
   {{-- Navigation-bar --}}
</div>
