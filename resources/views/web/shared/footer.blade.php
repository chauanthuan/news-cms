<footer class="site-footer site-footer--inverse inverse-text">
    <div class="site-footer__section site-footer__section--flex site-footer__section--seperated">
       <div class="container">
          <div class="site-footer__section-inner">
             <div class="site-footer__section-left">
                <div class="site-logo"><a href="#"><img src="{{ asset('web/img/logo-white.png') }}" alt="logo" width="200"></a></div>
             </div>
             <div class="site-footer__section-right">
                <ul class="social-list social-list--lg list-horizontal">
                   <li><a href="#"><i class="mdicon mdicon-facebook"></i></a></li>
                   <li><a href="#"><i class="mdicon mdicon-twitter"></i></a></li>
                   <li><a href="#"><i class="mdicon mdicon-youtube"></i></a></li>
                   <li><a href="#"><i class="mdicon mdicon-google-plus"></i></a></li>
                </ul>
             </div>
          </div>
       </div>
    </div>
    <div class="site-footer__section site-footer__section--flex site-footer__section--bordered-inner">
       <div class="container">
          <div class="site-footer__section-inner">
             <div class="site-footer__section-left">{{isset($configs) && !empty($configs['SITE_FOOTER_TEXT']) ? $configs['SITE_FOOTER_TEXT'] : ''}}</div>
             <div class="site-footer__section-right">
                <nav class="footer-menu">
                   <ul id="menu-footer-menu" class="navigation navigation--footer">
                        <li><a href="/">Trang chủ</a></li> 
                        <li><a href="{{route('web.contact')}}">Liên hệ</a></li>
                   </ul>
                </nav>
             </div>
          </div>
       </div>
    </div>
</footer>
{{-- Login modal --}}
<div class="modal fade login-modal" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="login-modal-label">
   <div class="modal-dialog" role="document">
      <div class="modal-content login-signup-form">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">✕</span></button>
            <div class="modal-title" id="login-modal-label">
               <ul class="nav nav-tabs js-login-form-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#login-tab" aria-controls="login-tab" role="tab" data-toggle="tab">Đăng nhập</a></li>
                  {{-- <li role="presentation"><a href="#signup-tab" aria-controls="signup-tab" role="tab" data-toggle="tab">Đăng ký</a></li> --}}
               </ul>
            </div>
         </div>
         <div class="modal-body">
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane fade in active" id="login-tab">
                  {{-- <div class="login-with-social">
                     <p>Log in with social account</p>
                     <ul class="social-list social-list--circle list-center">
                        <li><a href="#" class="facebook-theme-bg text-white"><i class="mdicon mdicon-facebook"></i></a></li>
                        <li><a href="#" class="twitter-theme-bg text-white"><i class="mdicon mdicon-twitter"></i></a></li>
                        <li><a href="#" class="googleplus-theme-bg text-white"><i class="mdicon mdicon-google-plus"></i></a></li>
                     </ul>
                  </div>
                  <div class="block-divider"><span>or</span></div> --}}
                  <form name="login-form" id="loginform" action="{{route('web.auth.login')}}" method="post">
                     {{ csrf_field() }}
                     <div class="form-group">
                        <label for="username">Tên đăng nhập</label>
                        <input type="text" name="username" id="username" class="input form-control" value="">
                     </div>
                     <div class="form-group">
                        <label for="password">Mật khẩu</label>
                        <input type="password" name="password" id="password" class="input form-control" value="">
                     </div>
                     <div class="form-group">
                        <div class="custom-control custom-checkbox">
                           <input type="checkbox" class="custom-control-input" id="customCheck1">
                           <label class="custom-control-label" for="customCheck1">Ghi nhớ lần sau</label>
                       </div>
                     </div>
                     <div class="form-group">
                        <input type="submit" name="login-submit" id="login-submit" class="btn btn-block btn-primary" value="Đăng nhập"> 
                     </div>
                     <p class="login-lost-password"><a href="{{route('web.auth.resetpass')}}" class="link link--darken">Quên mật khẩu?</a></p>
                  </form>
               </div>
               {{-- <div role="tabpanel" class="tab-pane fade" id="signup-tab">
                  <form name="login-form" id="registerform" action="{{route('web.auth.register')}}" method="post">
                     {{ csrf_field() }}
                     <div class="form-group">
                        <label for="user_fname">Họ và tên</label>
                        <input type="text" name="name" id="user_fname" class="input form-control" value="">
                     </div>
                     <div class="form-group">
                        <label for="user_email">Email</label>
                        <input type="email" name="email" id="user_email" class="input form-control" value="">
                     </div>
                     <div class="form-group">
                        <label for="user_register">Tên đăng nhập</label>
                        <input type="text" name="username" id="user_register" class="input form-control" value="">
                     </div>
                     <div class="form-group">
                        <label for="pass_register">Mật khẩu</label>
                        <input type="password" name="password" id="pass_register" class="input form-control" value="">
                     </div>
                     <div class="form-group">
                        <input type="submit" name="register-submit" id="register-submit" class="btn btn-block btn-primary" value="Đăng kí"> 
                     </div>
                  </form>
               </div> --}}
            </div>
         </div>
      </div>
   </div>
</div>