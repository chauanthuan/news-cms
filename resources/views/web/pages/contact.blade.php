@extends('web.layouts.base')
@section('content')
<div class="mnmd-block mnmd-block--fullwidth">
    <div class="container">
       <div class="row">
          <div class="mnmd-main-col">
             <div class="single-content">
                <div class="typography-copy">
                   <h3>Liên hệ</h3>
                   <form>
                      <div class="row">
                         <div class="col-sm-4 form-group"><label for="contactform-name">Họ và tên <small>*</small></label><input type="text" id="contactform-name" name="contactform-name" value="" class="form-control required" aria-required="true"></div>
                         <div class="col-sm-4 form-group"><label for="contactform-email">Email <small>*</small></label><input type="email" id="contactform-email" name="contactform-email" value="" class="required email form-control" aria-required="true"></div>
                         <div class="col-sm-4 form-group"><label for="contactform-phone">Số điện thoại</label><input type="text" id="contactform-phone" name="contactform-phone" value="" class="form-control"></div>
                      </div>
                      <div class="row">
                         <div class="col-sm-12 form-group">
                            <label for="contactform-subject">Tiêu đề <small>*</small></label>
                            <input type="text" id="contactform-subject" name="contactform-subject" value="" class="required form-control" aria-required="true">
                        </div>
                      </div>
                      <div class="form-group"><label for="contactform-message">Tin nhắn <small>*</small></label><textarea class="required form-control" id="contactform-message" name="contactform-message" rows="6" cols="30" aria-required="true"></textarea></div>
                      <div class="form-group"><button class="btn btn-primary" type="submit" id="contactform-submit" name="contactform-submit" value="submit">Gửi tin nhắn</button></div>
                   </form>
                </div>
             </div>
             <!-- .single-content -->
          </div>
          <!-- .mnmd-main-col -->
          <div class="mnmd-sub-col">
             <div class="typography-copy">
                <h3>Thông tin liên hệ</h3>
                <dl>
                   <dt><i class="mdicon mdicon-mail_outline"></i><span> Địa chỉ email</span></dt>
                   <dd>contact@viet24h.vn</dd>
                </dl>
             </div>
          </div>
          <!-- .mnmd-sub-col -->
       </div>
       <!-- .row -->
    </div>
    <!-- .container -->
 </div>
@endsection