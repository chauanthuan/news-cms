<!DOCTYPE html>
<html lang="en-US" class="wf-rubik-n3-active wf-rubik-n4-active wf-rubik-n7-active wf-rubik-n9-active wf-active" style="transform: none;">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="google-site-verification" content="Pm5JMzmA0Rwhu83qQW86JuDMkosJnGRNlpWKUsIVjcc" />
      <!-- Google Tag Manager -->
      <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','GTM-M3V7BHB');</script>
      <!-- End Google Tag Manager -->
      {{-- Basic --}}
      @php
         //  dd( \Request::route()->getName());
      @endphp
      @if (isset($article) && \Request::route()->getName() == 'web.article')
         @php
            $ar = \App\Models\Article::find($article->id);
            $listTagByAr = $ar->tags;
            $tagTemp = [];
            if($listTagByAr->count() > 0){
               foreach ($listTagByAr as $key => $tag) {
                  $tagTemp[] = $tag->name;
               }
            }
         @endphp
         <title>{{'Viet24h.vn - '.$article->title}}</title>
         <meta property="thumbnail" content="{{ MediaHelper::show($article->picture) }}">
         <meta property="og:image" content="{{ MediaHelper::show($article->picture) }}">
         <meta property="og:image:width" content="200"/>
         <meta property="og:image:height" content="200"/>
         <meta property="og:description" content="{!! strip_tags($article->sumary) !!}">
         <meta property="og:title" content="{!! $article->title !!}"> 
         <meta property="og:url" content="{{ Request::url() }}"> 
         <meta name="keywords" content="viet24h, viet24h.vn , tin nhanh 24h, tin nhanh viet24h, tin tức 24h, tin tức viet24h.vn, {{count($tagTemp) > 0 ? implode(", ",$tagTemp) :''}}">
         <meta name="description" content="{!! strip_tags($article->sumary) !!}">
         <meta name="author" content="viet24h.vn">
      @elseif(\Request::route()->getName() == 'web.category')
         @php
            $cateName = '';
            if($listArticle[0]){
               $cateName = $listArticle[0]->ct_name;
            }
         @endphp
         <title>{{'Viet24h.vn - '.$cateName}}</title>  
         <meta property="og:title" content="{!! $cateName !!}"> 
         <meta property="og:url" content="{{ Request::url() }}"> 
         <meta name="keywords" content="viet24h, viet24h.vn , tin nhanh 24h, tin nhanh viet24h, tin tức 24h, tin tức viet24h.vn, {{$cateName}}">
         <meta name="description" content="{{$cateName}} - {{ isset($configs) && !empty($configs['SITE_TITLE']) ? $configs['SITE_TITLE'] : env('SITE_TITLE','Viet24.vn - Tin nhanh, thông tin kinh tế, văn hóa, xã hội... cập nhật liên tục 24h') }}">
         <meta name="author" content="viet24h.vn">
      @else
         <title>{{ isset($configs) && !empty($configs['SITE_TITLE']) ? $configs['SITE_TITLE'] : env('SITE_TITLE','Viet24.vn - Tin nhanh, thông tin kinh tế, văn hóa, xã hội... cập nhật liên tục 24h') }}</title>
         <meta name="keywords" content="viet24h, viet24h.vn , tin nhanh 24h, tin nhanh viet24h, tin tức 24h, tin tức viet24h.vn">
         <meta name="description" content="{{ isset($configs) && !empty($configs['SITE_TITLE']) ? $configs['SITE_TITLE'] : env('SITE_TITLE','Viet24.vn - Tin nhanh, thông tin kinh tế, văn hóa, xã hội... cập nhật liên tục 24h') }}">
         <meta name="author" content="viet24h.vn">
      @endif

      <link rel="icon" href="{{asset('web/img/icon.png')}}" sizes="32x32">
      <link rel="icon" href="{{asset('web/img/icon.png')}}" sizes="192x192">
      <link rel="apple-touch-icon-precomposed" href="{{asset('web/img/icon.png')}}">
      <meta name="" content="img/icon.png">
      {{-- Mobile Metas --}}
      <meta name="viewport" content="width=device-width,initial-scale=1">
      {{-- Vendor CSS --}}
      <link href="{{ asset('web/css/vendors.css')}}" rel="stylesheet">
      {{-- Theme CSS --}}
      <link href="{{ asset('web/css/style.css')}}" rel="stylesheet">
      {{-- Theme Custom CSS --}}
      <link  href="{{ asset('web/css/fotorama.css')}}" rel="stylesheet">
     {{-- <link rel="stylesheet" href="{{ asset('web/css/css.css')}}"> --}}
     <link rel="stylesheet" href="{{ asset('web/css/custom.css')}}">
      {{-- Web Fonts --}}
      <link rel="stylesheet" href="{{ asset('web/fonts/stylesheet.css')}}">
      
      @yield('styles')
      <style>.theiaStickySidebar:after {content: ""; display: table; clear: both;}</style>
      @if (isset($configs) && !empty($configs['GOOGLE_ANALYTIC']))
          {!! $configs['GOOGLE_ANALYTIC'] !!}
      @endif
      <script type="text/javascript" src="{{ asset('web/js/jquery.min.js')}}"></script>
   </head>
   <body class="home home-3 has-block-heading-line" cz-shortcut-listen="true" style="transform: none;">
      <!-- Google Tag Manager (noscript) -->
      <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3V7BHB"
         height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
      <!-- End Google Tag Manager (noscript) -->
      {{-- .site-wrapper --}}
      <div class="site-wrapper" style="transform: none;">

        @include('web.shared.header')
        {{-- .site-content --}}
        @yield('content')

        {{-- Site footer --}}
        @include('web.shared.footer')
        {{-- Site footer --}}

         {{-- Off-canvas menu --}}
         <div id="mnmd-offcanvas-primary" class="mnmd-offcanvas js-mnmd-offcanvas js-perfect-scrollbar ps-container ps-theme-default" data-ps-id="d4237d26-5b19-1efd-b427-627fefd12c45">
            <div class="mnmd-offcanvas__title">
               <h2 class="site-logo"><a href="/"><img src="{{ asset('web/img/logo.png') }}" alt="logo" width="140"></a></h2>
               <ul class="social-list list-horizontal">
                  <li><a href="{{isset($configs) && !empty($configs['FACEBOOK_URL']) ? $configs['FACEBOOK_URL']:'#'}}"><i class="mdicon mdicon-facebook"></i></a></li>
                  <li><a href="{{isset($configs) && !empty($configs['TWITTER_URL']) ? $configs['TWITTER_URL']:'#'}}"><i class="mdicon mdicon-twitter"></i></a></li>
                  <li><a href="{{isset($configs) && !empty($configs['YOUTUBE_URL']) ? $configs['YOUTUBE_URL']:'#'}}"><i class="mdicon mdicon-youtube"></i></a></li>
                  <li><a href="{{isset($configs) && !empty($configs['GOOGLE_PLUS_URL']) ? $configs['GOOGLE_PLUS_URL']:'#'}}"><i class="mdicon mdicon-google-plus"></i></a></li>
               </ul>
               <a href="#mnmd-offcanvas-primary" class="mnmd-offcanvas-close js-mnmd-offcanvas-close" aria-label="Close"><span aria-hidden="true">✕</span></a>
            </div>
            <div class="mnmd-offcanvas__section mnmd-offcanvas__section-navigation">
               <ul id="menu-offcanvas-menu" class="navigation navigation--offcanvas">
                  <li class="">
                     <a href="/">
                        Trang chủ
                     </a>
                  </li>
                  @foreach ($all_category as $item)
                     <li class="menu-item-cat-{{$item->id}}">
                     <a href="{{'/'.$item->slug}}">{{$item->name}}</a>
                     </li>
                  @endforeach
                  <li class="menu-item-has-children">
                     @if (\Auth::user())
                        @php
                           $role = \Auth::user()->roles;
                           $role_name = isset($role) && !$role->isEmpty() ? $role[0]->slug : '';
                        @endphp
                        <a href="#">
                              Xin Chào {{\Auth::user()->name}}
                        </a>
                        <ul class="sub-menu">
                           @if (in_array($role_name, ['supper-admin','manager','editor']))
                              <li><a target="_blank" href="{{route('cms.dashboard')}}">Vào CMS</a></li>
                           @endif
                           
                           <li><a href="#">Hoạt động bình luận</a></li>
                           <li><a href="{{route('web.auth.profile')}}">Chỉnh sửa tài khoản</a></li>
                           <li><a href="{{route('web.auth.changepass')}}">Đổi mật khẩu</a></li>
                           <li><a href="{{route('web.auth.logout')}}">Thoát</a></li>
                        </ul>
                     @else
                        <a href="#">
                           Xin Chào Bạn
                        </a>
                        <ul class="sub-menu">
                           <li><a href="{{route('web.auth.login')}}">Đăng nhập</a></li>
                           {{-- <li><a href="{{route('web.auth.register')}}">Đăng ký</a></li> --}}
                        </ul>
                     @endif
                     
                  </li>
               </ul>
            </div>
         </div>
         {{-- Off-canvas menu --}} 
         <a href="#" class="mnmd-go-top btn btn-default hidden-xs js-go-top-el"><i class="mdicon mdicon-arrow_upward"></i></a>
      </div>
      {{-- .site-wrapper --}}
      {{-- Vendor --}}
	  
	  <script type="text/javascript" src="{{ asset('web/js/vendors.min.js')}}"></script>
	  {{-- Theme Scripts --}}
     <script type="text/javascript" src="{{ asset('web/js/scripts.min.js')}}"></script>
     
     
      <script src="{{ asset('web/js/fotorama.min.js')}}"></script>
	  {{-- Theme Custom Scripts --}}
     <script src="{{ asset('web/js/custom.js')}}"></script>
     
     @yield('scripts')

   </body>
</html>