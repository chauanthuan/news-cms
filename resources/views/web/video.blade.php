@extends('web.layouts.base')
@section('content')
    <div class="site-content">
        @if (!empty($listArticleVideo->items()))
            <div class="mnmd-block mnmd-block--fullwidth mnmd-block--contiguous page-heading page-heading--has-background">
                <div class="container">
                    <h2 class="page-heading__title">Tất cả video</h2>
                    {{-- <div class="page-heading__subtitle">Category description.</div> --}}
                </div>
            </div>
        
            <div class="mnmd-block mnmd-block--fullwidth">
                <div class="container">
                    <div class="row">
                    <div class="mnmd-main-col" role="main">
                        <div class="mnmd-block">
                            <div class="posts-list list-unstyled list-space-lg">
                                @foreach ($listArticleVideo as $index=>$article)
                                @if ($index == 0)
                                    <li class="list-item">
                                        <article class="post--overlay post--overlay-floorfade post--overlay-bottom post--overlay-sm post--overlay-padding-lg has-score-badge cat-4">
                                            <div class="background-img" style="background-image: url({{ MediaHelper::show($article->picture) }})"></div>
                                            <div class="post__text inverse-text">
                                            <div class="post__text-wrap">
                                                <div class="post__text-inner">
                                                    <div class="media">
                                                        <div class="media-left media-middle post-type-icon post-type-icon--md"><i class="mdicon mdicon-play_circle_outline"></i></div>
                                                        <div class="media-body media-middle">
                                                            <h3 class="post__title typescale-3">{{$article->title}}</h3>
                                                            <div class="post__meta">
                                                                @if (isset($configs) && !empty($configs['SHOW_VIEW_COUNT']))
                                                                    <span><i class="mdicon mdicon-visibility"></i>{{$article->view_count}}</span> 
                                                                @endif
                                                                @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                                    <span><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}" class="link-overlay"></a>
                                        </article>
                                    </li>
                                @else
                                    <div class="list-item">
                                        <article class="post post--horizontal post--horizontal-sm cat-6">
                                                <div class="post__thumb">
                                                    <a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}">
                                                        <img src="{{MediaHelper::show($article->picture)}}">
                                                        <div class="overlay-item--center-xy post-type-icon"><i class="mdicon mdicon-play_circle_outline"></i></div>
                                                    </a>
                                                    </div>
                                                    <div class="post__text">
                                                    <h3 class="post__title typescale-1"><a href="{{route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug])}}">{{$article->title}}</a></h3>
                                                    <div class="post__meta">
                                                        @if (isset($configs) && !empty($configs['SHOW_VIEW_COUNT']))
                                                            <span><i class="mdicon mdicon-visibility"></i>{{$article->view_count}}</span> 
                                                        @endif
                                                        @if (isset($configs) && !empty($configs['SHOW_COMMENT_COUNT']))
                                                            <span><i class="mdicon mdicon-chat_bubble_outline"></i>{{$article->comment_count}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                        </article>
                                    </div>
                                @endif
                                @endforeach
                            </div>
                            <nav class="mnmd-pagination">
                                {{ $listArticleVideo->render('vendor.pagination.custom') }}
                            </nav>
                        </div>
                        {{-- .mnmd-block --}}
                    </div>
                    {{-- .mnmd-main-col --}}
                    @include('web.shared.sidebar')
                    <div class="mnmd-sub-col mnmd-sub-col--right sidebar js-sticky-sidebar" role="complementary">
                        <!-- .widget subscriber -->
                        {{-- @include('web.shared.widget_subscribe') --}}
                        <!-- .widget social-->
                        {{-- @include('web.shared.widget_social') --}}
                    </div>
                    {{-- .mnmd-sub-col --}}
                    </div>
                    {{-- .row --}}
                </div>
                {{-- .container --}}
            </div>
        @else
            <div class="mnmd-block mnmd-block--fullwidth mnmd-block--contiguous page-heading page-heading--has-background">
                <div class="container">
                    <p>Không có bài viết</p>   
                </div>
            </div>
        @endif
    {{-- .mnmd-block --}}
    </div>
    {{-- .site-content --}}    
@endsection 