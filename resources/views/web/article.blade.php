@extends('web.layouts.base')
@section('content')
    <div class="site-content single-entry">
        <div class="mnmd-block mnmd-block--fullwidth single-entry-wrap"> 
            <div class="container">
                <div class="row">
                    <div class="mnmd-main-col" role="main">
                        <header class="single-header">
                            <a href="{{'/'.$article->ct_slug}}" class="entry-cat cat-theme cat-5">{{$article->ct_name}}</a>
                            <h1 class="entry-title">{{$article->title}}</h1>
                            <div class="entry-teaser entry-teaser--lg">{!! $article->sumary !!}</div>
                            <div class="entry-meta">
                                @if ($article->show_author && !empty($article->author))
                                    <span class="entry-author entry-author--with-ava">
                                    <img alt="Ryan Reynold" src="{{asset('web/img/users/avatar.png')}}" class="entry-author__avatar" height="34" width="34">Đăng bởi <a href="#" class="entry-author__name" title="Đăng bởi {{$article->author}}" rel="author">{{$article->author}}</a> 
                                    </span>
                                @endif
                            
                                @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                    <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time> 
                                @endif 
                            </div>
                        </header>
                        <div class="entry-interaction entry-interaction--horizontal">
                            <div class="entry-interaction__left">
                            <div class="post-sharing post-sharing--simple">
                                <ul>
                                    <li>
                                        <a href="{{Share::load(route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug]), $article->title)->facebook()}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600'); return false;"
                                            class="sharing-btn sharing-btn-primary facebook-btn facebook-theme-bg" data-toggle="tooltip" data-placement="top" title="Share on Facebook" >
                                            <i class="mdicon mdicon-facebook"></i>
                                            <span class="sharing-btn__text">Share</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{Share::load(route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug]), $article->title)->twitter()}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                        class="sharing-btn sharing-btn-primary twitter-btn twitter-theme-bg" data-toggle="tooltip" data-placement="top" title="Share on Twitter"><i class="mdicon mdicon-twitter"></i><span class="sharing-btn__text">Tweet</span></a></li>
                                    <li>
                                        <a href="{{Share::load(route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug]), $article->title)->pinterest()}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                        class="sharing-btn pinterest-btn pinterest-theme-bg" data-toggle="tooltip" data-placement="top" title="Share on Pinterest"><i class="mdicon mdicon-pinterest-p"></i></a></li>
                                    {{-- <li>
                                        <a href="{{Share::load(route('web.article',['categorySlug'=>$cate->slug,'articleSlug'=>$article->slug]), $article->title)->gplus()}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                        class="sharing-btn googleplus-btn googleplus-theme-bg" data-toggle="tooltip" data-placement="top" title="Share on Google+"><i class="mdicon mdicon-google-plus"></i></a>
                                    </li> --}}
                                </ul>
                            </div>
                            </div>
                            @if (isset($article->allow_comment) && $article->allow_comment == 1)
                                <div class="entry-interaction__right"><a href="#comment" class="comments-count entry-action-btn" data-toggle="tooltip" data-placement="top" title="{{$article->comment_count}} Bình luận"><i class="mdicon mdicon-chat_bubble"></i><span>{{$article->comment_count}}</span></a></div>
                            @endif
                        </div>



                    <article class="mnmd-block post post--single post-10 type-post status-publish format-standard has-post-thumbnail hentry category-abroad tag-landscape cat-5" itemscope itemtype="http://schema.org/Article">
                        <div class="single-content">
                            @if ($article->article_type == 1) {{-- normal post type --}}
                                @if (!$article->hide_thumbnail)
                                    <div class="entry-thumb single-entry-thumb">
                                        <img src="{{ MediaHelper::show($article->picture) }}">
                                    </div>
                                @endif
                                <div class="single-body entry-content typography-copy" itemprop="articleBody">
                                    {!! $article->content_html !!}
                                </div>
                            @elseif($article->article_type == 2) {{--Gallerry--}}
                                <div class="single-body entry-content typography-copy" itemprop="articleBody">
                                    {!! $article->content_html !!}
                                </div>
                                <div class="fotorama" 
                                    data-allowfullscreen="native" 
                                    data-nav="thumbs" 
                                    data-width="100%" 
                                    data-ratio="800/600" 
                                    data-minwidth="400"
                                    data-maxwidth="1000"
                                    data-minheight="300"
                                    data-maxheight="100%">
                                    @foreach ($listGallery as $item)
                                        <img src="{{MediaHelper::show($item->path)}}">
                                    @endforeach
                                </div>
                            @elseif($article->article_type == 3) {{--Video--}}
                                <div class="mnmd-responsive-video">
                                    <iframe src="{{$article->video_url}}" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
                                </div>
                                <div class="single-body entry-content typography-copy" itemprop="articleBody">
                                    {!! $article->content_html !!}
                                </div>
                            @endif
                            
                            
                            <footer class="single-footer entry-footer">
                                @if(!empty($article->source))
                                    <div class="row row--space-between grid-gutter-10 pull-right float-right">
                                        <h5>Theo <b>{{$article->source}}</b></h5>
                                    </div>
                                @endif
                                <div class="entry-info">
                                    <div class="row row--space-between grid-gutter-10">
                                        <div class="entry-categories col-sm-6">
                                            <ul>
                                                <li class="entry-categories__icon"><i class="mdicon mdicon-folder"></i><span class="sr-only">Danh mục</span></li>
                                                @foreach ($list_cate as $cate)
                                                    <li><a href="{{'/'.$cate->slug}}" class="entry-cat cat-theme cat-2">{{$cate->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="entry-tags col-sm-6">
                                            <ul>
                                                @if (isset($tags) && !$tags->isEmpty())
                                                    <li class="entry-tags__icon"><i class="mdicon mdicon-local_offer"></i><span class="sr-only">Tagged with</span></li>
                                                    @foreach ($tags as $tag)
                                                        <li><a href="{{route('web.tag',['tagSlug'=>$tag->slug])}}" class="post-tag" rel="tag">{{$tag->name}}</a></li>
                                                    @endforeach
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="entry-interaction entry-interaction--horizontal">
                                    <div class="entry-interaction__left">
                                        <div class="post-sharing post-sharing--simple">
                                            <ul>
                                                <li>
                                                    <a href="{{Share::load(route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug]), $article->title)->facebook()}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600'); return false;"
                                                        class="sharing-btn sharing-btn-primary facebook-btn facebook-theme-bg" data-toggle="tooltip" data-placement="top" title="Share on Facebook" >
                                                        <i class="mdicon mdicon-facebook"></i>
                                                        <span class="sharing-btn__text">Share</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="{{Share::load(route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug]), $article->title)->twitter()}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                                    class="sharing-btn sharing-btn-primary twitter-btn twitter-theme-bg" data-toggle="tooltip" data-placement="top" title="Share on Twitter"><i class="mdicon mdicon-twitter"></i><span class="sharing-btn__text">Tweet</span></a></li>
                                                <li>
                                                    <a href="{{Share::load(route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug]), $article->title)->pinterest()}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                                    class="sharing-btn pinterest-btn pinterest-theme-bg" data-toggle="tooltip" data-placement="top" title="Share on Pinterest"><i class="mdicon mdicon-pinterest-p"></i></a></li>
                                                {{-- <li>
                                                    <a href="{{Share::load(route('web.article',['categorySlug'=>$article->ct_slug,'articleSlug'=>$article->slug]), $article->title)->gplus()}}" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" 
                                                    class="sharing-btn googleplus-btn googleplus-theme-bg" data-toggle="tooltip" data-placement="top" title="Share on Google+"><i class="mdicon mdicon-google-plus"></i></a>
                                                </li> --}}
                                            </ul>
                                        </div>
                                    </div>
                                    @if ($article->allow_comment)
                                        <div class="entry-interaction__right">
                                            <a href="#comment" class="comments-count entry-action-btn" data-toggle="tooltip" data-placement="top" title="{{$article->comment_count}} bình luận">
                                                <i class="mdicon mdicon-chat_bubble"></i>
                                                <span>{{$article->comment_count}}</span>
                                            </a>
                                        </div>
                                    @endif
                                    
                                </div>
                            </footer>
                            {{-- ads widget --}}
                             @include('web.shared.widget_ads_under_article')
                        </div>
                        {{-- .single-content --}}
                    </article>
                    {{-- .post--single --}}
                    {{-- Posts navigation --}}
                    <div class="posts-navigation single-entry-section clearfix">
                        @if($prev_article)
                            <div class="posts-navigation__prev">
                                <article class="post--overlay post--overlay-bottom post--overlay-floorfade">
                                    <div class="background-img" style="background-image: url({{MediaHelper::show($prev_article->picture)}})"></div>
                                    <div class="post__text inverse-text">
                                    <div class="post__text-wrap">
                                        <div class="post__text-inner">
                                            <h3 class="post__title typescale-1">{{$prev_article->title}}</h3>
                                        </div>
                                    </div>
                                    </div>
                                    <a href="{{'/'.$prev_article->ct_slug.'/'.$prev_article->slug}}" class="link-overlay"></a>
                                </article>
                                <a class="posts-navigation__label" href="{{'/'.$prev_article->ct_slug.'/'.$prev_article->slug}}"><span><i class="mdicon mdicon-arrow_back"></i>Bài trước đó</span></a>
                            </div>
                        @endif
                        
                        @if ($next_article)
                            <div class="posts-navigation__next">
                                <article class="post--overlay post--overlay-bottom post--overlay-floorfade">
                                    <div class="background-img" style="background-image: url({{MediaHelper::show($next_article->picture)}})"></div>
                                    <div class="post__text inverse-text">
                                    <div class="post__text-wrap">
                                        <div class="post__text-inner">
                                            <h3 class="post__title typescale-1">{{$next_article->title}}</h3>
                                        </div>
                                    </div>
                                    </div>
                                    <a href="{{'/'.$next_article->ct_slug.'/'.$next_article->slug}}" class="link-overlay"></a>
                                </article>
                                <a class="posts-navigation__label" href="{{'/'.$next_article->ct_slug.'/'.$next_article->slug}}"><span>Bài tiếp theo<i class="mdicon mdicon-arrow_forward"></i></span></a>
                            </div>
                        @endif
                        
                    </div>
                    {{-- Posts navigation --}}
                    

                    {{-- Related posts --}}
                    @if (count($related_article) > 0)
                        <div class="related-posts single-entry-section">
                            <div class="block-heading block-heading--line">
                                <h4 class="block-heading__title">Có thể bạn quan tâm</h4>
                            </div>
                            <div class="row row--space-between">
                                @foreach ($related_article as $rl_article)
                                    <div class="col-xs-12 col-sm-4">
                                        <article class="post--vertical cat-4">
                                        <div class="post__thumb"><a href="{{'/'.$rl_article->ct_slug.'/'.$rl_article->slug}}"><img src="{{ MediaHelper::show($rl_article->picture) }}"></a></div>
                                        <div class="post__text">
                                            <h3 class="post__title typescale-1"><a href="{{'/'.$rl_article->ct_slug.'/'.$rl_article->slug}}">{{$rl_article->title}}</a></h3>
                                            <div class="post__meta">
                                                @if (isset($configs) && !empty($configs['SHOW_TIME_ARTICLE']))
                                                    <time class="time published" datetime="{{$article->created_at}}" title="{{Helpers::time_elapsed_string($article->created_at)}}"><i class="mdicon mdicon-schedule"></i>{{Helpers::time_elapsed_string($article->created_at)}}</time> 
                                                @endif 
                                            </div>
                                        </div>
                                        </article>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    @endif
                    @if (isset($article->allow_comment) && $article->allow_comment == 1)
                        {{-- Comments section --}}
                    <div class="comments-section single-entry-section">
                            <div id="comments" class="comments-area">
                                <h2 class="comments-title">{{$comments->total()}} Bình luận</h2>
                                <ol class="comment-list">
                                    @foreach($comments as $comment)
                                        @if ($comment->comment_parent_id == 0)
                                            <li id="comment-{{$comment->comment_id}}" class="comment even thread-even depth-1 parent">
                                                <article id="div-comment-{{$comment->comment_id}}" class="comment-body">
                                                    <footer class="comment-meta">
                                                        <div class="comment-author vcard"><img src="{{asset('web/img/users/avatar.png')}}" class="avatar photo"><b class="fn">{{$comment->username}}</b><span class="says">says:</span></div>
                                                        <div class="comment-metadata">
                                                            <a href="#">
                                                                <time datetime="{{$comment->created_at}}">{{Helpers::time_elapsed_string($comment->created_at)}}</time>
                                                            </a>
                                                        </div>
                                                    </footer>
                                                    <div class="comment-content">
                                                        <p>{!! $comment->content !!}</p>
                                                    </div>
                                                    <div class="reply"><a rel="nofollow" class="comment-reply-link" href="javascript:void(0)" aria-label="Reply to Sculpture Fan" data-parentid="{{$comment->comment_id}}">Phản hồi</a></div>
                                                </article>
                                                @php
                                                    $childComment = $comment->children;
                                                @endphp
                                                @if ($childComment->count() > 0)
                                                    <ol class="children">
                                                        @foreach($childComment as $child)
                                                            <li id="comment-{{$child->comment_id}}" class="comment byuser comment-author-melchoyce bypostauthor odd alt depth-2 parent">
                                                                <article id="div-comment-{{$child->comment_id}}" class="comment-body">
                                                                    <footer class="comment-meta">
                                                                        <div class="comment-author vcard"><img src="{{asset('web/img/users/avatar.png')}}" class="avatar photo"><a href="#" rel="external nofollow" class="url">{{$child->username}}</a><span class="says">says:</span></div>
                                                                        <div class="comment-metadata"><a href="#"><time datetime="{{$child->created_at}}">{{Helpers::time_elapsed_string($child->created_at)}}</time> </a></div>
                                                                    </footer>
                                                                    <div class="comment-content">
                                                                        <p>{!! $child->content !!}</p>
                                                                    </div>
                                                                    <div class="reply"><a rel="nofollow" class="comment-reply-link" href="javascript:void(0)" aria-label="Reply to Sculpture Fan" data-parentid="{{$child->comment_id}}">Phản hồi</a></div>
                                                                </article>
                                                                @php
                                                                    $childComment2 = $child->children;
                                                                @endphp
                                                                @if ($childComment2->count() > 0)
                                                                    <ol class="children">
                                                                        @foreach ($childComment2 as $child2)
                                                                            <li id="comment-{{$child2->comment_id}}" class="comment even depth-3">
                                                                                <article id="div-comment-{{$child2->comment_id}}" class="comment-body">
                                                                                    <footer class="comment-meta">
                                                                                        <div class="comment-author vcard"><img src="{{asset('web/img/users/avatar.png')}}" class="avatar photo"><b class="fn">{{$child2->username}}</b><span class="says">says:</span></div>
                                                                                        <div class="comment-metadata"><a href="#"><time datetime="{{$child2->created_at}}">{{Helpers::time_elapsed_string($child2->created_at)}}</time></a></div>
                                                                                    </footer>
                                                                                    <div class="comment-content">
                                                                                        <p>{!! $child2->content !!}</p>
                                                                                    </div>
                                                                                    {{-- <div class="reply"><a rel="nofollow" class="comment-reply-link" href="javascript:void(0)" aria-label="Reply to Sculpture Fan" data-parentid="{{$child2->comment_id}}">Phản hồi</a></div> --}}
                                                                                </article>
                                                                                </li>
                                                                        @endforeach
                                                                    </ol>
                                                                    {{-- .children --}}
                                                                @endif
                                                            </li>
                                                            {{-- #comment-## --}}
                                                        @endforeach
                                                    </ol>
                                                    {{-- .children --}}
                                                @endif
                                            </li>
                                        @endif
                                    @endforeach
                                </ol>
                                <div id="respond" class="comment-respond">
                                    <h3 id="reply-title" class="comment-reply-title">Gửi bình luận <small><a rel="nofollow" id="cancel-comment-reply-link" href="#" style="display: none">Hủy bình luận</a></small></h3>
                                    <form action="{{route('postcomment')}}" method="post" id="commentform" class="comment-form">
                                        {{ csrf_field() }}
                                        <p class="comment-notes"><span id="email-notes">Địa chỉ email của bạn sẽ không xuất hiện.</span> Tất cả các trường đánh dấu (<span class="required">*</span>) là bắt buộc</p>
                                        <p class="comment-form-comment">
                                            <label for="comment">Nội dung <span class="required">*</span></label>
                                            <textarea id="comment" name="content" cols="45" rows="8" maxlength="65525" aria-required="true" required="required">{{Input::old('content')}}</textarea></p>
                                            @if ($errors->has('content')) <span class="help-block">{{ $errors->first('content') }}</span> @endif
                                        <p class="comment-form-author">
                                            <label for="author">Tên <span class="required">*</span></label>
                                            <input id="author" name="username" type="text" value="{{ Input::old('username') }}" size="30" maxlength="245" aria-required="true" required="required"></p>
                                            @if ($errors->has('username')) <span class="help-block">{{ $errors->first('username') }}</span> @endif
                                        <p class="comment-form-email">
                                            <label for="email">Email <span class="required">*</span></label>
                                            <input id="email" name="email" type="email" value="{{ Input::old('email') }}" size="30" maxlength="100" aria-describedby="email-notes" aria-required="true" required="required"></p>
                                            @if ($errors->has('email')) <span class="help-block">{{ $errors->first('email') }}</span> @endif
                                        <p class="form-submit">
                                            <input name="submit" type="submit" id="submit" class="submit" value="Gửi"> 
                                            <input type="hidden" name="article_id" value="{{$article->id}}" id="article_id"> 
                                            <input type="hidden" name="comment_parent_id" id="comment_parent" value="0">
                                        </p>
                                    </form>
                                </div>
                                {{-- #respond --}}
                            </div>
                            {{-- #comments --}}
                        </div>
                        {{-- Comments section --}}
                    @endif
                    
                    </div>
                    {{-- .mnmd-main-col --}}
                    @include('web.shared.sidebar')
                    {{-- .mnmd-sub-col --}}
                </div>
            </div>
        </div>
    </div>
    {{-- .site-content --}}
@endsection

@section('scripts')
    <script>
    $(document).ready(function(){
        $('.comment-reply-link').on('click',function(e){
			var parent_id = $(this).data().parentid;
			$('input[name="comment_parent_id"]').val(parent_id);
			var form = $("#respond").clone();
			$("#respond").css({'display':'none'});
			$(this).closest('li.comment').append(form);
			$("#respond #reply-title").html('').append('Bình luận <small><a rel="nofollow" id="cancel-comment-reply-link" href="javascript:void(0)" style="">Hủy bình luận</a></small>')
		});

        $('body').on('click','#cancel-comment-reply-link',function(){
            $(this).closest('.comment-list #respond').remove();
			$('input[name="comment_parent_id"]').val(0);
			$("#respond").css({'display':'block'});
			$("#respond #reply-title").html('').append('Bình luận')
		});
    })
    </script>
@endsection