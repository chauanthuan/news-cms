<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li>
                    <!-- User Profile-->
                    <div class="user-profile dropdown m-t-20">
                        <div class="user-pic">
                            <img src="{{asset('admin/assets/images/users/1.jpg')}}" alt="users" class="rounded-circle img-fluid" />
                        </div>
                        <div class="user-content hide-menu m-t-10">
                            <h5 class="m-b-10 user-name font-medium">{{\Auth::user()->username}}</h5>
                            <a href="javascript:void(0)" class="btn btn-circle btn-sm m-r-5" id="Userdd" role="button" data-toggle="dropdown" aria-haspopup="true"
                                aria-expanded="false">
                                <i class="ti-settings"></i>
                            </a>
                            <a href="{{route('cms.auth.logout')}}" title="Logout" class="btn btn-circle btn-sm">
                                <i class="ti-power-off"></i>
                            </a>
                            <div class="dropdown-menu animated flipInY" aria-labelledby="Userdd">
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)">
                                    <i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('cms.account')}}">
                                    <i class="ti-settings m-r-5 m-l-5"></i> Cài đặt tài khoản</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{route('cms.auth.logout')}}">
                                    <i class="fa fa-power-off m-r-5 m-l-5"></i> Thoát</a>
                            </div>
                        </div>
                    </div>
                    <!-- End User Profile-->
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('web.index')}}" aria-expanded="false">
                        <i class="sl-icon-loop"></i>
                        <span class="hide-menu">View Web Page</span>
                    </a>
                </li>
                @role('supper-admin')
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="{{route('cms.dashboard')}}" aria-expanded="false">
                            <i class="icon-Car-Wheel"></i>
                            <span class="hide-menu">Dashboards </span>
                        </a>
                    </li>

                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Administrator"></i>
                            <span class="hide-menu">Tài khoản </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                            <a href="{{route('cms.account')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{route('cms.account.create')}}" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Folder-Add"></i>
                            <span class="hide-menu">Danh mục </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.category')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả danh mục </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{route('cms.category.create')}}" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Receipt"></i>
                            <span class="hide-menu">Bài viết </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.article')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả bài viết </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{route('cms.article.create')}}" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    {{-- <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">Media </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.media')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả file </span>
                                </a>
                            </li>
                        </ul>
                    </li> --}}
                    
                    
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Files"></i>
                            <span class="hide-menu">Trang </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả trang </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="mdi mdi-creation"></i>
                            <span class="hide-menu">Bình luận </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.comment')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả bình luận </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Receipt-3"></i>
                            <span class="hide-menu">Role </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.role')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả Role </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{route('cms.role.create')}}" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Security-Check"></i>
                            <span class="hide-menu">Permission </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.permission')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả permission </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{route('cms.permission.create')}}" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="sidebar-item">
                        <a class="sidebar-link waves-effect waves-dark" href="{{route('cms.configure')}}" aria-expanded="false">
                            <i class="icon-Car-Wheel"></i>
                            <span class="hide-menu">Cấu hình website </span>
                        </a>
                    </li>
                @endrole


                {{-- Manager menu --}}
                @role('manager')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">Bài viết </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.article')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả bài viết </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{route('cms.article.create')}}" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">Danh mục </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.category')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả danh mục </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{route('cms.category.create')}}" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">file </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.media')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả file </span>
                                </a>
                            </li>
                        </ul>
                    </li> --}}
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">Bình luận </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.comment')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả bình luận </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">Trang </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả trang </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="#" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endrole


                @role('editor')
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">Bài viết </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.article')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả bài viết </span>
                                </a>
                            </li>
                            <li class="sidebar-item">
                                <a href="{{route('cms.article.create')}}" class="sidebar-link">
                                    <i class="mdi mdi-email-alert"></i>
                                    <span class="hide-menu"> Tạo mới </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    {{-- <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">file </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.media')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả file </span>
                                </a>
                            </li>
                        </ul>
                    </li> --}}
                    <li class="sidebar-item">
                        <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="icon-Mailbox-Empty"></i>
                            <span class="hide-menu">Bình luận </span>
                        </a>
                        <ul aria-expanded="false" class="collapse  first-level">
                            <li class="sidebar-item">
                                <a href="{{route('cms.comment')}}" class="sidebar-link">
                                    <i class="mdi mdi-email"></i>
                                    <span class="hide-menu"> Tất cả bình luận </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endrole
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
