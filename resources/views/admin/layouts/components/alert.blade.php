<div class="row">
    <div class="col-sm-12">
    @if (session('status'))
    <div class="alert bg-success alert-dismissible mb-2">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
        {{ session('status') }}
    </div>
    @endif
    @if( isset($message) && !empty($message) )
        <div class="alert bg-danger alert-dismissible fade in" role="alert">
        {{$message}}
        </div>
    @endif
    @if(isset($errors) && !is_array($errors) && $errors->all())
    <div class="alert bg-danger alert-dismissible mb-2" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">×</span>
        </button>
        <ul style=" margin-bottom: 0px; padding-left: 10px;">
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
    @endif
    </div>
</div>
