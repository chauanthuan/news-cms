<!-- Large modal -->
<div class="modal fade text-left bs-item-modal-lg" id="default" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
        <h4 class="modal-title">{{ @$pTitle }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        </div>
      </div>
    </div>
  </div>
  
  <!-- Small modal -->
  <div class="modal fade bs-item-modal-sm" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">{{ @$pTitle }}</h4>
        </div>
        <div class="modal-body">
        </div>
      </div>
    </div>
  </div>
  
  <div class="modal fade js-delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="myModalLabel">Confirm Delete?</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure to delete "<strong class="js-title-delete"></strong>" ?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-danger js-delete-item">Delete</button>
        </div>
      </div>
    </div>
  </div>