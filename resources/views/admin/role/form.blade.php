@extends('admin.layouts.base')
@section('content')

<div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Quản lý Role</h4>
                <div class="d-flex align-items-center">

                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('cms.role')}}">Tất cả Role</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        
        <!-- Row -->
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{(isset($role) && !empty($role)) ? 'Cập nhật':'Tạo'}} Role
                        </h4>
                        @if(isset($errors) && !is_array($errors) && $errors->all())
                            <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <ul style="padding: 0px;list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert bg-success alert-dismissible mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('status') }}
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{(isset($role) && !empty($role)) ? route('cms.role.update', ['roleId' => $role->id]) :route('cms.role.create')}}" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            @if (isset($role) && !empty($role))
                                <input type="hidden" name="id" value="{{ $role->id }}">
                            @endif
                            <div class="form-group m-t-40 row">
                                <label for="name-input" class="col-2 col-form-label">Tên Role</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('name',isset($role) && !empty($role) ? $role->name: '')}}" id="name-input" name="name" placeholder="Tên Role">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="slug-input" class="col-2 col-form-label">Slug</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('slug',isset($role) && !empty($role) ? $role->slug: '')}}" id="slug-input" name="slug" placeholder="Slug">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="select-permission" class="col-2 col-form-label">Permission</label>
                                <div class="col-10">
                                    <div class="row">

                                        @foreach ($listPermission as $i=>$permission)
                                            @if(isset($list_permission_arr) && in_array($permission->id, $list_permission_arr))
                            
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" checked class="custom-control-input" id="permission_checkbox_{{$permission->id}}" name="permission_id[]" value="{{$permission->id}}">
                                                        <label class="custom-control-label" for="permission_checkbox_{{$permission->id}}">{{$permission->name}}</label>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="permission_checkbox_{{$permission->id}}" name="permission_id[]" {{ (is_array(old('permission_id')) && in_array($permission->id, old('permission_id'))) ? ' checked' : '' }} value="{{$permission->id}}">
                                                        <label class="custom-control-label" for="permission_checkbox_{{$permission->id}}">{{$permission->name}}</label>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label"></label>
                                <div class="col-10">
                                <button class="btn btn-info" type="submit">{{isset($role) && !empty($role) ? 'Cập nhật': 'Tạo'}} role</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    
@endsection
