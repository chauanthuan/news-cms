@extends('admin.layouts.base')
@section('content')

<div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Quản lý danh mục</h4>
                <div class="d-flex align-items-center">

                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('cms.account')}}">Tất cả tài khoản</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        
        <!-- Row -->
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{(isset($account) && !empty($account)) ? 'Cập nhật':'Tạo'}} quản trị viên
                        </h4>
                        @if(isset($errors) && !is_array($errors) && $errors->all())
                            <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <ul style="padding: 0px;list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert bg-success alert-dismissible mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('status') }}
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{(isset($account) && !empty($account)) ? route('cms.account.update', ['accountId' => $account->id]) :route('cms.account.create')}}" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            @if (isset($account) && !empty($account))
                                <input type="hidden" name="id" value="{{ $account->id }}">
                            @endif
                            <div class="form-group m-t-40 row">
                                <label for="name-input" class="col-2 col-form-label">Họ tên</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('name',isset($account) && !empty($account) ? $account->name: '')}}" id="name-input" name="name" placeholder="Họ tên">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email-input" class="col-2 col-form-label">Email</label>
                                <div class="col-10">
                                    <input class="form-control" type="email" value="{{Input::old('email',isset($account) && !empty($account) ? $account->email: '')}}" id="email-input" name="email" placeholder="Địa chỉ email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="username-input" class="col-2 col-form-label">Tên đăng nhập</label>
                                <div class="col-10">
                                <input class="form-control username" type="text" id="username-input" name="username" value="{{Input::old('username', isset($account) && !empty($account) ? $account->username: '')}}" {{isset($account) && !empty($account) ? 'readonly': ''}} placeholder="Tên đăng nhập">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="password-input" class="col-2 col-form-label">Password</label>
                                <div class="col-10">
                                    <input class="form-control" type="password" value="" id="password-input" name="password" placeholder="Mật khẩu">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label">Quản trị viên</label>
                                <div class="col-10">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="1" class="custom-control-input" id="is_admin-input" name="is_admin" {{ isset($account) && !empty($account) && $account->is_admin == 1 ? 'checked': ''}}>
                                        <label class="custom-control-label" for="is_admin-input"></label>
                                    </div>
                                    {{-- <input class="" type="checkbox" value="1" id="is_admin-input" {{ isset($account) && !empty($account) && $account->is_admin == 1 ? 'checked': ''}} name="is_admin"> --}}
                                </div>
                                

                            </div>
                            <div class="form-group row">
                                {{-- $listRole = $this->role->get(); --}}
                                <label for="role-input" class="col-2 col-form-label">Role</label>
                                <div class="col-10">
                                    @php
                                        $roleName = '';
                                        $roleId = '';
                                    @endphp
                                    <select name="role" class="form-control">
                                        <option value="">Chọn Role...</option>
                                        @foreach ($listRole as $role)
                                            @php
                                                if(!empty($account) && !empty($account->roles()->first())){
                                                    $roleName = $account->roles()->first()->name;
                                                    $roleId = $account->roles()->first()->id;
                                                }
                                            @endphp
                                            <option value="{{$role->id}}" {{(!empty($account) && !empty($account->roles()->first()) && $account->roles()->first()->id == $role->id) ? 'selected':''}}>{{$role->name}}</option>
                                        @endforeach
                                    </select>
                                    @if (isset($listPermissionByRole))
                                        <hr>     
                                        <p>Dưới đây là các permission được setup theo Role <b>{{$roleName}}</b>, muốn thay đổi thì click vào <a href="{{route('cms.role.update',['roleId'=> $roleId])}}">đây</a></p>
                                        <p>Hoặc để xem tất cả các Role có những Permission nào thì click vào <a href="{{route('cms.role')}}">đây</a></p>
                                        <div class="row">
                                            @foreach ($listPermissionByRole as $i=>$permission)
                                                @if(isset($list_permission_arr_by_role) && in_array($permission->id, $list_permission_arr_by_role))
                                
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-checkbox">
                                                            <input disabled type="checkbox" checked class="custom-control-input" id="permission_by_role_checkbox_{{$permission->id}}" name="permission_by_role_id[]" value="{{$permission->id}}">
                                                            <label class="custom-control-label" for="permission_by_role_checkbox_{{$permission->id}}">{{$permission->name}}</label>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="col-md-4">
                                                        <div class="custom-control custom-checkbox">
                                                            <input disabled type="checkbox" class="custom-control-input" id="permission_by_role_checkbox_{{$permission->id}}" name="permission_by_role_id[]" {{ (is_array(old('permission_id')) && in_array($permission->id, old('permission_id'))) ? ' checked' : '' }} value="{{$permission->id}}">
                                                            <label class="custom-control-label" for="permission_by_role_checkbox_{{$permission->id}}">{{$permission->name}}</label>
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                        <hr>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="select-permission" class="col-2 col-form-label">Permission <br><small>(Ngoài việc chọn Role thì có thể chọn những quyền năng đặc biệt)</small></label>
                                <div class="col-10">
                                    <div class="row">
                                        @foreach ($listPermission as $i=>$permission)
                                            @if(isset($list_permission_arr) && in_array($permission->id, $list_permission_arr))
                            
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" checked class="custom-control-input" id="permission_checkbox_{{$permission->id}}" name="permission_id[]" value="{{$permission->id}}">
                                                        <label class="custom-control-label" for="permission_checkbox_{{$permission->id}}">{{$permission->name}}</label>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="permission_checkbox_{{$permission->id}}" name="permission_id[]" {{ (is_array(old('permission_id')) && in_array($permission->id, old('permission_id'))) ? ' checked' : '' }} value="{{$permission->id}}">
                                                        <label class="custom-control-label" for="permission_checkbox_{{$permission->id}}">{{$permission->name}}</label>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                    <hr>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-2 col-form-label">Kích hoạt</label>
                                <div class="col-10">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="1" class="custom-control-input" id="state-input" name="state" {{ isset($account) && !empty($account) && $account->state == 1 ? 'checked': ''}}>
                                        <label class="custom-control-label" for="state-input"></label>
                                    </div>
                                </div>
                            </div>
                            
                            
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label"></label>
                                <div class="col-10">
                                <button class="btn btn-info" type="submit">{{isset($account) && !empty($account) ? 'Cập nhật': 'Tạo'}} quản trị viên</button>
                                </div>
                            </div>
                        </form>

                        @if (isset($account) && !empty($account))
                        <hr>
                        <h4>Thông tin cá nhân</h4>
                        <form>
                            <div class="form-group row">
                                <label for="gendor-input" class="col-2 col-form-label">Giới tính</label>
                                <div class="col-10">
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="genderMale" value="1" checked name="gender" {{ isset($profile) && !empty($profile) && $profile->gender == 1 ? 'checked':''}}>
                                            <label class="custom-control-label" for="genderMale">Nam</label>
                                        </div>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <div class="custom-control custom-radio">
                                            <input type="radio" class="custom-control-input" id="genderFemale" value="0" name="gender" {{ isset($profile) && !empty($profile) && $profile->gender == 0 ? 'checked':''}}>
                                            <label class="custom-control-label" for="genderFemale">Nữ</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="phone-input" class="col-2 col-form-label">Số điện thoại</label>
                                <div class="col-10">
                                    <input class="form-control" type="tel" value="{{Input::old('phone',isset($profile) && !empty($profile) ? $profile->phone: '')}}" id="phone-input" name="phone" placeholder="Số điện thoại">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="birthday-input" class="col-2 col-form-label">Ngày sinh</label>
                                <div class="col-10">
                                    <input class="form-control" type="date" value="{{Input::old('birthday',isset($profile) && !empty($profile) ? $profile->birthday: '')}}" id="birthday-input" name="birthday" placeholder="Sinh nhật">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="address-input" class="col-2 col-form-label">Địa chỉ</label>
                                <div class="col-10">
                                <textarea class="form-control address" type="text" id="address-input" name="address" placeholder="Địa chỉ">{{Input::old('address', isset($profile) && !empty($profile) ? $profile->address: '')}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="company-input" class="col-2 col-form-label">Công ty</label>
                                <div class="col-10">
                                <input class="form-control company" type="text" id="company-input" name="company" placeholder="Công ty" value="{{Input::old('company', isset($profile) && !empty($profile) ? $profile->company: '')}}">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label"></label>
                                <div class="col-10">
                                <a class="btn btn-info btn-profile"  href="javascript:void(0)" data-title="{{isset($profile) && !empty($profile) ? 'Cập nhật' : 'Tạo mới'}}" data-id="{{isset($profile) && !empty($profile) ? $profile->id : ''}}" data-url="{{route('cms.account.profile')}}">{{isset($profile) && !empty($profile) ? 'Cập nhật': 'Tạo mới'}}</a>
                                </div>
                            </div>
                        </form>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    
@endsection
