@extends('admin.layouts.base')
@section('content')

<div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Quản lý bài viết</h4>
                <div class="d-flex align-items-center">

                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('cms.article')}}">Tất cả bài viết</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        
        <!-- Row -->
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{(isset($article) && !empty($article)) ? 'Cập nhật':'Tạo'}} bài viết
                        </h4>
                        @if(isset($errors) && !is_array($errors) && $errors->all())
                            <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <ul style="padding: 0px;list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert bg-success alert-dismissible mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('status') }}
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{(isset($article) && !empty($article)) ? route('cms.article.update', ['articleId' => $article->id]) :route('cms.article.create')}}" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            @if (isset($article) && !empty($article))
                                <input type="hidden" name="id" value="{{ $article->id }}">
                                <input type="hidden" name="user_id_edited" value="{{ Auth::user()->id }}">
                            @else
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            @endif
                            <div class="form-group m-t-40 row">
                                <label for="title-input" class="col-2 col-form-label">Tiêu đề</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('title',isset($article) && !empty($article) ? $article->title: '')}}" id="title-input" name="title" placeholder="Tiêu đề bài viết">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="select-cate" class="col-2 col-form-label">Thể loại</label>
                                <div class="col-10">
                                    <div class="row">
                                        @foreach ($listCate as $i=>$cate)
                                            @if(isset($list_cate_by_ar) && in_array($cate->id, $list_cate_by_ar))
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" checked class="custom-control-input" id="cate_checkbox_{{$cate->id}}" name="category_id[]" value="{{$cate->id}}">
                                                        <label class="custom-control-label" for="cate_checkbox_{{$cate->id}}">{{$cate->name}}</label>
                                                    </div>
                                                </div>
                                            @else
                                                <div class="col-md-4">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="cate_checkbox_{{$cate->id}}" name="category_id[]" {{ (is_array(old('category_id')) && in_array($cate->id, old('category_id'))) ? ' checked' : '' }} value="{{$cate->id}}">
                                                        <label class="custom-control-label" for="cate_checkbox_{{$cate->id}}">{{$cate->name}}</label>
                                                    </div>
                                                </div>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="sumary-input" class="col-2 col-form-label">Mô tả</label>
                                <div class="col-10">
                                    <textarea class="form-control sumary texteditor-basic" type="text" id="sumary-input" name="sumary">{{Input::old('sumary', isset($article) && !empty($article) ? $article->sumary: '')}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="content-input" class="col-2 col-form-label">Nội dung</label>
                                <div class="col-10">
                                    <textarea class="form-control texteditor" type="text" id="content-input" name="content_html">{{Input::old('content_html', isset($article) && !empty($article) ? $article->content_html: '')}}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="source-input" class="col-2 col-form-label">Nguồn</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('source', isset($article) && !empty($article) ? $article->source: '')}}" id="source-input" name="source" placeholder="Dân trí, Báo lao động, VnExpress...">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="author-input" class="col-2 col-form-label">Tác giả</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('author', isset($article) && !empty($article) ? $article->author: '')}}" id="author-input" name="author" placeholder="Tên tác giả">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="tags" class="col-2 col-form-label">Tags</label>
                                <div class="col-10">
                                    <select id="tags" multiple="multiple" class="form-control" name="tag[]">
                                        @if (old('tags', isset($list_tag) && !empty($list_tag) ? $list_tag : ''))
                                            @foreach(old('tags', isset($list_tag) && !empty($list_tag) ? $list_tag : '') as $tag)
                                                <option value="{{ $tag->name }}" selected="selected">{{ $tag->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="picture-input" class="col-2 col-form-label">Ảnh đại diện</label>
                                <div class="col-10">
                                    <div class="form-group">
                                        <div class="row-item">
                                            <div class="list-image js-droppable">
                                            {!! MediaHelper::renderSingle('picture', Helpers::getObj(isset($article) && !empty($article) ? $article : '' , 'picture'), '' ) !!}
                                            </div>
                                            {!! MediaHelper::renderUploadButton('picture') !!}
                                        </div>
                                    </div>
                                </div>       
                            </div> 
                            


                            <div class="form-group row">
                                <label for="article_type-input" class="col-2 col-form-label">Loại bài viết</label>
                                <div class="col-10">
                                    <select name="article_type" class="form-control">
                                        <option value="1" {{Input::old('article_type', isset($article) && !empty($article) ? $article->article_type: '') == 1 ? 'selected':'' }}>Bài viết</option>
                                        <option value="2" {{Input::old('article_type', isset($article) && !empty($article) ? $article->article_type: '') == 2 ? 'selected':'' }}>Gallery</option>
                                        <option value="3" {{Input::old('article_type', isset($article) && !empty($article) ? $article->article_type: '') == 3 ? 'selected':'' }}>Video</option>
                                    </select>
                                </div>
                            </div>
                                
                            <div class="form-group row multi-img hide">
                                <label class="col-2 col-form-label">Ảnh Gallery</label>
                                <div class="col-10">
                                    <div class="row-item">
                                        <div class="list-image js-droppable">
                                            <div class="row">
                                                {!! MediaHelper::renderMulti('gallery[]', isset($list_img_gallery) && !empty($list_img_gallery) ? $list_img_gallery : '' , '' ) !!}
                                            </div>
                                        </div>
                                        {!! MediaHelper::renderUploadButton('gallery[]','image',1) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row video-url hide">
                                <label class="col-2 col-form-label">Video Url</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('video_url', isset($article) && !empty($article) ? $article->video_url: '')}}" id="video_url-input" name="video_url" placeholder="Liên kết video từ Youtube hoặc Vimeo ...">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="state-input" class="col-2 col-form-label">Kích hoạt</label>
                                <div class="col-10">
                                    <input class="" type="checkbox" value="1" id="state-input" {{ isset($article) && !empty($article) && $article->state == 1 ? 'checked': ''}} name="state">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="featured-input" class="col-2 col-form-label">Hiện ở trang chủ (Bài viết nổi bật)</label>
                                <div class="col-10">
                                    <input class="" type="checkbox" value="1" id="featured-input" {{ isset($article) && !empty($article) && $article->featured == 1 ? 'checked': ''}} name="featured">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="allow-comment-input" class="col-2 col-form-label">Cho phép bình luận</label>
                                <div class="col-10">
                                    <input class="" type="checkbox" value="1" id="allow-comment-input" {{ isset($article) && !empty($article) && $article->allow_comment == 1 ? 'checked': ''}} name="allow_comment">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="show-author-input" class="col-2 col-form-label">Hiện tên tác giả</label>
                                <div class="col-10">
                                    <input class="" type="checkbox" value="1" id="show-author-input" {{ isset($article) && !empty($article) && $article->show_author == 1 ? 'checked': ''}} name="show_author">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="hide-thumbnail-input" class="col-2 col-form-label">Ẩn ảnh đại diện trong bài chi tiết</label>
                                <div class="col-10">
                                    <input class="" type="checkbox" value="1" id="show-thumbnail-input" {{ isset($article) && !empty($article) && $article->hide_thumbnail == 1 ? 'checked': ''}} name="hide_thumbnail">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-datetime-local-input" class="col-2 col-form-label">Hẹn ngày publish</label>
                                <div class="col-10">
                                <input class="form-control" type="datetime-local" value="{{ isset($article) && !empty($article->published_date) ? date("Y-m-d\TH:i:s",strtotime($article->published_date)): ''}}" id="example-datetime-local-input" name="published_date">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label"></label>
                                <div class="col-10">
                                    <button class="btn btn-info" type="submit">{{isset($article) && !empty($article) ? 'Cập nhật': 'Tạo'}} bài viết</button>
                                    <a href="{{route('cms.article')}}" class="btn btn-dark waves-effect waves-light">Đóng</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
@section('scripts')
    <script>
        $(document).ready(function(){
            if($('select[name="article_type"]').val() == 2) {
                $('.multi-img').removeClass('hide');
            }
            if($('select[name="article_type"]').val() == 3) {
                $('.video-url').removeClass('hide');
            }
            $('select[name="article_type"]').on('change', function(){
                if($(this).val() == 2){ //2: Gallery
                    $('.multi-img').removeClass('hide');
                    $('.video-url').addClass('hide');
                }
                else if($(this).val() == 3){
                    $('.video-url').removeClass('hide');
                    $('.multi-img').addClass('hide');
                }
                else{
                    $('.multi-img').addClass('hide');
                    $('.video-url').addClass('hide');
                }
            })

            $(".js-select2-tags").select2({
                tags: true,
                tokenSeparators: [',', ' ']
            });

            $('#tags').select2({
                data: [],
                tags: true,
                tokenSeparators: [','], 
                placeholder: "Tên nhân vật, tên địa danh hoặc địa điểm, tên thương hiệu..."
            });
        })
    </script>
@endsection