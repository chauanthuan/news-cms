@extends('admin.layouts.base')
@section('content')
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Quản lý bài viết</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                                            </li>
                                            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('cms.article.create')}}">Tạo bài viết</a></li>
                                        </ol>
                                    </nav>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="container-fluid">
                <div class="row">
                        <div class="col-12">
                            <form class="form" method="get" action="#">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                                {!! Form::text('keyword', '', ['class'=>'form-control', 'placeholder'=>'Tên bài viết ...'])!!}
                                        </div>
                                    </div> 
                                    <div class="col-md-3">
                                            <div class="form-group">
                                                <select name="category" class="form-control">
                                                    <option value="0">Danh mục</option>
                                                    @foreach ($listCate as $cate)
                                                        <option value="{{$cate->id}}" @if(isset($category) && $category=="$cate->id"){{"selected"}} @endif >{{$cate->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>              
                                    <div class="col-md-1">
                                        <div class="form-group">
                                        <button type="submit" class="btn btn-primary">
                                            Search
                                        </button>
                                        </div>
                                    </div>    
                                </div>
                            </form>       
                        </div>
                    </div>
                <div class="row">
                    <div class="col-12">
                        <!-- Column -->
                        <div class="card">
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert bg-success alert-dismissible mb-2">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                      </button>
                                      {{ session('status') }}
                                    </div>
                                @endif
                                <h4 class="card-title">Tất cả bài viết</h4>
                                <div class="table-responsive">
                                    <table class="tablesaw table-bordered table-hover table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th scope="col">Tiêu đề</th>
                                                <th scope="col">Mô tả ngắn</th>
                                                <th scope="col">Thể loại</th>
                                                <th scope="col">Tags(từ khóa)</th>
                                                <th scope="col">Lượt xem</th>
                                                <th scope="col">Ảnh đại diện</th>
                                                <th scope="col">Ngày tạo</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if ($listArticle)
                                            
                                                @php
                                                    $page = isset($_GET['page']) ? $_GET['page'] : 1;
                                                    $size = count($listArticle->items());
                                                @endphp
                                                @foreach ($listArticle as $index=>$article)
                                                    @php
                                                        //get list category by article
                                                        $ar = \App\Models\Article::find($article->id);
                                                        $listCateByAr = $ar->categories;
                                                        $listTagByAr = $ar->tags;
                                                        
                                                    @endphp
                                                    <tr>
                                                        <td>{{(($page - 1) * 10) + $index + 1}}</td>
                                                    <td class="title"><a class="link" target="_blank" href="{{env('APP_URL','http://viet24h.vn/').$listCateByAr[0]->slug.'/'.$article->slug}}">{{$article->title}}</a></td>
                                                        <td>{!! $article->sumary !!}</td>
                                                        <td>
                                                            @foreach ($listCateByAr as $item)
                                                            <span style="border: 1px solid rgba(78,78,78,0.2); padding: 5px;
                                                            border-radius: 4px; display: inline-block;margin: 3px;">{{$item->name}}</span>
                                                            @endforeach
                                                        </td>
                                                        <td>
                                                            @foreach ($listTagByAr as $item)
                                                                <span style="border: 1px solid rgba(78,78,78,0.2); padding: 5px;
                                                                border-radius: 4px; display: inline-block;margin: 3px;">{{$item->name}}</span>
                                                            @endforeach
                                                        </td>
                                                        <td>{{$article->view_count}}</td>
                                                        <td>
                                                            <img src="{{ MediaHelper::show($article->picture) }}" alt="" width="100px">
                                                        </td>
                                                        <td>{{$article->created_at}}</td>
                                                        <td class="" style="white-space: nowrap;">
                                                            <a href="{{route('cms.article.update',['articleId'=>$article->id])}}" class="btn btn-outline-warning"><i class="far fa-edit"></i> Sửa</a>
                                                            <a href="javascript:void(0)" class="btn btn-outline-danger delete-btn" data-id="{{$article->id}}" data-title="Bài viết '{{$article->title}}'" data-url="{{route('cms.article.delete')}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else    
                                                <p>Không có dữ liệu</p>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center pagination-center">
                                    {{ $listArticle->links('vendor/pagination/bootstrap-4') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
@endsection 
@section('scripts')

@endsection