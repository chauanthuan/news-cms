@extends('admin.layouts.base')
@section('content')

<div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Quản lý permission</h4>
                <div class="d-flex align-items-center">

                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('cms.permission')}}">Tất cả Permission</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        
        <!-- Row -->
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{(isset($permission) && !empty($permission)) ? 'Cập nhật':'Tạo'}} permission
                        </h4>
                        @if(isset($errors) && !is_array($errors) && $errors->all())
                            <div class="alert bg-danger alert-dismissible mb-2" permission="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <ul style="padding: 0px;list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert bg-success alert-dismissible mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('status') }}
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{(isset($permission) && !empty($permission)) ? route('cms.permission.update', ['permissionId' => $permission->id]) :route('cms.permission.create')}}" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            @if (isset($permission) && !empty($permission))
                                <input type="hidden" name="id" value="{{ $permission->id }}">
                            @endif
                            <div class="form-group m-t-40 row">
                                <label for="name-input" class="col-2 col-form-label">Tên permission</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('name',isset($permission) && !empty($permission) ? $permission->name: '')}}" id="name-input" name="name" placeholder="Tên permission">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="slug-input" class="col-2 col-form-label">Slug</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('slug',isset($permission) && !empty($permission) ? $permission->slug: '')}}" id="slug-input" name="slug" placeholder="Slug">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label"></label>
                                <div class="col-10">
                                <button class="btn btn-info" type="submit">{{isset($permission) && !empty($permission) ? 'Cập nhật': 'Tạo'}} permission</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    
@endsection
