@extends('admin.layouts.base')
@section('content')

<div class="page-breadcrumb">
        <div class="row">
            <div class="col-5 align-self-center">
                <h4 class="page-title">Quản lý danh mục</h4>
                <div class="d-flex align-items-center">

                </div>
            </div>
            <div class="col-7 align-self-center">
                <div class="d-flex no-block justify-content-end align-items-center">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">
                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active" aria-current="page"><a href="{{route('cms.category')}}">Tất cả danh mục</a></li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        
        <!-- Row -->
        <!-- .row -->
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">
                            {{(isset($category) && !empty($category)) ? 'Cập nhật':'Tạo'}} danh mục
                        </h4>
                        @if(isset($errors) && !is_array($errors) && $errors->all())
                            <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                                <ul style="padding: 0px;list-style: none;">
                                    @foreach($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        @if (session('status'))
                            <div class="alert bg-success alert-dismissible mb-2">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                            {{ session('status') }}
                            </div>
                        @endif
                        <form class="form" method="POST" action="{{(isset($category) && !empty($category)) ? route('cms.category.update', ['categoryId' => $category->id]) :route('cms.category.create')}}" enctype="multipart/form-data" >
                            {{ csrf_field() }}
                            @if (isset($category) && !empty($category))
                                <input type="hidden" name="id" value="{{ $category->id }}">
                                <input type="hidden" name="user_id_edited" value="{{ Auth::user()->id }}">
                            @else
                                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                            @endif
                            <div class="form-group m-t-40 row">
                                <label for="name-input" class="col-2 col-form-label">Tên danh mục</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('name',isset($category) && !empty($category) ? $category->name: '')}}" id="name-input" name="name" placeholder="Tiêu đề danh mục">
                                </div>
                            </div>
                         
                            <div class="form-group row">
                                <label for="description-input" class="col-2 col-form-label">Mô tả</label>
                                <div class="col-10">
                                    <textarea class="form-control description" type="text" id="description-input" name="description">{{Input::old('description', isset($category) && !empty($category) ? $category->description: '')}}</textarea>
                                </div>
                            </div>
                            
                            <div class="form-group m-t-40 row">
                                <label for="iconClass-input" class="col-2 col-form-label">Class Icon</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" value="{{Input::old('iconClass',isset($category) && !empty($category) ? $category->iconClass: '')}}" id="iconClass-input" name="iconClass" placeholder="Class icon menu">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="state-input" class="col-2 col-form-label">Kích hoạt</label>
                                <div class="col-10">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="state-input" name="state" {{ isset($category) && !empty($category) && $category->state == 1 ? 'checked': ''}} value="1">
                                        <label class="custom-control-label" for="state-input"></label>
                                    </div>
                                    {{-- <input class="" type="checkbox" value="1" id="state-input" {{ isset($category) && !empty($category) && $category->state == 1 ? 'checked': ''}} name="state"> --}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label">Hiển thị ở trang chủ</label>
                                <div class="col-10">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="showHome-input" name="showHome"  {{ isset($category) && !empty($category) && $category->showHome == 1 ? 'checked': ''}} value="1">
                                        <label class="custom-control-label" for="showHome-input"></label>
                                    </div>
                                    {{-- <input class="" type="checkbox" value="1" id="show-home-input" {{ isset($category) && !empty($category) && $category->showHome == 1 ? 'checked': ''}} name="showHome"> --}}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label">Hiển thị ở Menu</label>
                                <div class="col-10">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="showMenu-input" name="showMenu"  {{ isset($category) && !empty($category) && $category->showMenu == 1 ? 'checked': ''}} value="1">
                                        <label class="custom-control-label" for="showMenu-input"></label>
                                    </div>
                                    {{-- <input class="" type="checkbox" value="1" id="show-menu-input" {{ isset($category) && !empty($category) && $category->showMenu == 1 ? 'checked': ''}} name="showMenu"> --}}
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="" class="col-2 col-form-label"></label>
                                <div class="col-10">
                                <button class="btn btn-info" type="submit">{{isset($category) && !empty($category) ? 'Cập nhật': 'Tạo'}} danh mục</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Right sidebar -->
        <!-- ============================================================== -->
        <!-- .right-sidebar -->
        <!-- ============================================================== -->
        <!-- End Right sidebar -->
        <!-- ============================================================== -->
    </div>
    
@endsection
