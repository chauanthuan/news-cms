@extends('admin.layouts.base')
@section('content')
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Quản lý danh mục</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                                            </li>
                                        <li class="breadcrumb-item active" aria-current="page"><a href="{{route('cms.category.create')}}">Tạo danh mục</a></li>
                                        </ol>
                                    </nav>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Column -->
                        <div class="card">
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert bg-success alert-dismissible mb-2">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                      </button>
                                      {{ session('status') }}
                                    </div>
                                @endif
                                <h4 class="card-title">Tất cả danh mục</h4>
                                {{-- <h5 class="card-subtitle">Swipe Mode, ModeSwitch, Minimap, Sortable, SortableSwitch</h5> --}}
                                <div class="search"></div>
                                <table class="tablesaw table-bordered table-hover table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th scope="col">Tiêu đề</th>
                                            <th scope="col">Mô tả ngắn</th>
                                            <th scope="col">Trạng thái</th>
                                            <th scope="col">Hiện ở Trang chủ</th>
                                            <th scope="col">Hiện ở Menu</th>
                                            <th scope="col">Ngày tạo</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($listCate)
                                        
                                            @php
                                                $page = isset($_GET['page']) ? $_GET['page'] : 1;
                                                $size = count($listCate->items());
                                               
                                            @endphp
                                            @foreach ($listCate as $index=>$cate)
                                                
                                                <tr>
                                                    <td>{{(($page - 1) * 10) + $index + 1}}</td>
                                                    <td class="title"><a class="link" href="javascript:void(0)">{{$cate->name}}</a></td>
                                                    <td>{!! $cate->description !!}</td>
                                                    <td>
                                                        {{$cate->state == 1 ? 'Active':'In-Active'}}
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" value="" {{$cate->showHome == 1 ? 'checked':''}} onclick="return false;"> 
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" value="" {{$cate->showMenu == 1 ? 'checked':''}} onclick="return false;">
                                                    </td>
                                                    <td>{{$cate->created_at}}</td>
                                                    <td class="" style="white-space: nowrap;">
                                                        <a href="{{route('cms.category.update',['cateId'=>$cate->id])}}" class="btn btn-outline-warning"><i class="far fa-edit"></i> Sửa</a>
                                                        <a href="javascript:void(0)" class="btn btn-outline-danger delete-btn" data-id="{{$cate->id}}" data-title="Danh mục '{{$cate->name}}'" data-url="{{route('cms.category.delete')}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else    
                                            <p>Không có dữ liệu</p>
                                        @endif
                                    </tbody>
                                </table>
                                
                                <div class="text-center pagination-center">
                                    {{ $listCate->links('vendor/pagination/bootstrap-4') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
@endsection 
@section('scripts')

@endsection