@extends('admin.layouts.base')
@section('content')

<div class="page-breadcrumb">
    <div class="row">
        <div class="col-5 align-self-center">
            <h4 class="page-title">Quản lý file</h4>
            <div class="d-flex align-items-center">

            </div>
        </div>
        <div class="col-7 align-self-center">
            <div class="d-flex no-block justify-content-end align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">
                                    <a href="{{route('cms.dashboard')}}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="javascript:void(0)" class="add_media">Tải file mới</a></li>
                            </ol>
                        </nav>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row upload-section hide">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('cms.media.create' , ['media_type' => $media_type]) }}" class="dropzone needsclick dz-clickable"  enctype="multipart/form-data">
                        <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <form class="form" method="get" action="">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::text('keyword', '', ['class'=>'form-control', 'placeholder'=>'Tên file ...'])!!}
                        </div>
                    </div>              
                    <div class="col-md-1">
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary">
                            Tìm kiếm
                        </button>
                        </div>
                    </div>    
                </div>
            </form>       
        </div>
    </div>
    
    <div class="row">
        <div class="col-12">
            <!-- Column -->
            <div class="card">
                <div class="card-body">
                    <table class="tablesaw table-bordered table-hover table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th scope="col">Ảnh</th>
                                <th scope="col">Tên</th>
                                <th scope="col">Người tạo</th>
                                <th scope="col">Được gắn vào Bài viết</th>
                                <th scope="col">Ngày tạo</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($medias)
                                @foreach($medias as $i => $media)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td><a href="{{ MediaHelper::show($media->filename) }}" class="d-block athumb" data-fancybox>
                                                <img src="{{ MediaHelper::show($media->filename) }}" class="" width="150px">
                                            </a>
                                        </td>
                                        <td>
                                            {{$media->filename}}
                                        </td>
                                        <td><a href="{{route('cms.account.update',['accountId'=>$media->user_id])}}">{{$media->name}}</a></td>
                                        <td>
                                            @if (isset($media->title) && !empty($media->title))
                                                <a href="{{route('cms.article.update',['articleId'=>$media->article_id])}}">{{$media->title}}</a>
                                            @else
                                                Chưa gắn 
                                        <a href="javascript:void(0)" class="attach_img btn btn-xs btn-info" data-id="{{$media->media_id}}">Gắn ngay</a>
                                            @endif
                                            
                                        </td>
                                        <td>{{ Helpers::convertDate($media->created_at, 'd/m/Y') }}</td>
                                        <td>
                                            <a href="javascript:void(0)" class="btn btn-outline-danger delete-btn" data-title="Hình ảnh" data-id="{{$media->media_id}}" data-url="{{ route('cms.media.delete' , ['media_id' => $media->media_id ]) }}"><i class="fas fa-trash-alt"></i> Xóa</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>

                    <div class="text-center pagination-center">
                        {{ $medias->render() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Large modal -->
<div class="modal fade text-left bs-item-modal-lg" id="attachArticle" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Gắn hình đại diện cho bài viết</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="seachForm">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="article_title" placeholder="Tên bài viết">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="number" class="form-control" name="limit" value="10" placeholder="Số lượng bài viết">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <a class="btn btn-default search-attach-ar" href="javascript:void(0)">Tìm kiếm</a>
                        </div>
                    </div>
                </div>
                <div class="listAr"></div>
            </div>
            <div class="modal-footer">
                <a href="javascript:void(0)" class="btn btn-primary confirm-attach">OK</a>
            </div>
        </div>
    </div>
</div>
<input type="hidden" value="" name="img_attach_id">
@endsection

@section('scripts')
    <script>
        $(document).ready(function(){
            $('.add_media').click(function(){
                $('.upload-section').show();
            });
            $('.attach_img').click(function(){
                var id = $(this).data().id;
                $('input[name="img_attach_id"]').val(id);
            })
            $('.attach_img,.search-attach-ar').click(function(){
                var name = $('input[name="article_title"]').val();
                var limit = $('input[name="limit"]').val();
                $.ajax({
                    url: "{{route('cms.article')}}",
                    method: 'GET',
                    dataType: 'json',
                    data: {
                        'keyword':name,
                        'limit': (limit == '' || limit == 0) ? 10: limit
                    },
                    success: function(response){
                        var html = '';
                        if(response.data.length > 0){
                            html += '<ul class="list-unstyled">';
                                $.each(response.data, function(index, value){
                                    html += '<li><div class="form-group">'+
                                        '<div class="form-check form-check-inline">'+
                                        '<div class="custom-control custom-radio">'+
                                            '<input type="radio" class="custom-control-input" id="attach'+index+'" value="'+value.id+'" name="attach_article">'+
                                            '<label class="custom-control-label" for="attach'+index+'">'+value.title+'</label>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div></li>';
                                })
                                
                            html += '</ul>';
                        }
                        $('#attachArticle .modal-body .listAr').html('').append(html);
                        $('#attachArticle').modal('show');
                    }
                })
            });

            $('body').on('click','.confirm-attach', function(){
                var articleId   = $('input[name="attach_article"]:checked').val();
                var imgId       = $('input[name="img_attach_id"]').val();
                var route       = "{{route('cms.article.updatemainimg')}}" ;
                var self = $(this);
                swal({   
                    title: "Bạn có chắc?",   
                    text: "Cập nhật hình ảnh chính cho bài viết này này!",   
                    type: "warning",   
                    showCancelButton: true,   
                    confirmButtonColor: "#DD6B55",   
                    confirmButtonText: "Cập nhật ngay!",   
                    cancelButtonText: "Đóng",   
                    allowOutsideClick: false
                }).then(function(isConfirm){  
                    if (isConfirm.value == true) {  
                        $.ajax({
                            url: route,
                            method: 'POST',
                            dataType: 'json',
                            data: {article_id: articleId, img_id: imgId},
                            success: function(response){
                                if(response.stt == 1){
                                    swal("Thành công!", "Cập nhật hình ảnh chính cho bài viết này này!", "success"); 
                                    location.reload(); 
                                }
                                else{
                                    swal("Thất bại", "Có lỗi xảy ra khi cập nhật hình ảnh.", "error");   
                                }
                            }
                        }).done({
                            
                        })
                    } 
                    else {  
                    
                    } 
                });

                return false;
                $.ajax({
                    url: "{{route('cms.article.updatemainimg')}}",
                    method: 'POST',
                    dataType: 'json',
                    data:{article_id:articleId, img_id: imgId},
                    success: function(response){
                        var html = '';
                        if(response.data.length > 0){
                            html += '<ul class="list-unstyled">';
                                $.each(response.data, function(index, value){
                                    html += '<li><div class="form-group">'+
                                        '<div class="form-check form-check-inline">'+
                                        '<div class="custom-control custom-radio">'+
                                            '<input type="radio" class="custom-control-input" id="attach'+index+'" value="1" name="attach_article">'+
                                            '<label class="custom-control-label" for="attach'+index+'">'+value.title+'</label>'+
                                        '</div>'+
                                        '</div>'+
                                        '</div></li>';
                                })
                                
                            html += '</ul>';
                        }
                        $('#attachArticle .modal-body .listAr').append(html);
                        $('#attachArticle').modal('show');
                    }
                })
            })
        })
    </script>
@endsection