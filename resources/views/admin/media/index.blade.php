@if ($page == 1)
<section class="gallery-env">

    <div role="tabpanel" class="block-media">
         <ul class="nav nav-tabs">
            <li role="presentation" class="nav-item">
                <a href="#tab-gallery" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true" class="js-list-media active nav-link" >@if($media_type == 'image') Hình ảnh @endif</a>
            </li>
            <li role="presentation" class="nav-item ">
                <a href="#tab-upload" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true" class="nav-link">@if($media_type == 'image') Tải hình ảnh @endif</a>
            </li>
        </ul>
        <div class="tab-content container-fluid">
            <div role="tabpanel" class="tab-pane active in" id="tab-gallery" aria-labelledby="home-tab">
                <div class="row">
                    <div class="block-scroll  col-12">
                        <div class="images-list album-images row ui-sortable js-image-list">
@endif
                        @foreach($medias as $i => $media)
                            <div class="col-md-4 col-sm-6 js-media-item" data-media-id="{{ $media->media_id }}">
                                <div class="file-man-box">
                                    <a href="{{ route('cms.media.delete' , ['media_id' => $media->media_id ]) }}" class="file-close js-delete-media"><i class="mdi mdi-close-circle"></i></a>
                                    <div class="file-img-box">
                                        <a href="{{$media->filename }}" class="d-block athumb">
                                          <img src="{{ MediaHelper::show($media->filename) }}" class="img-responsive">
                                        </a>
                                    </div>
                                    {{-- <a href="#" class="file-download"><i class="mdi mdi-download"></i> </a> --}}
                                    <div class="">
                                        <h5 class="mb-0 text-overflow">{{ $media->filename }}</h5>
                                        <p class="mb-0"><small>{{ Helpers::convertDate($media->created_at, 'd/m/Y') }}</small></p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
@if ($page == 1)
                       </div>
                        @if ($maxPage > 1)
                            <a href="{{ route('cms.mediamodal' , ['media_type' => 'image' ]) }}" data-maxpage="{{ $maxPage }}" data-currentPage="2" class="btn btn-success btn-block js-load-image">
                                <i class="fa fa-bars"></i> Load More
                            </a>
                        @endif
                    </div>
                  <div class="ln_solid"></div>
                  <div class="form-group pull-right block-accept">
                    <div class="col-md-12">
                      <a href="#" class="btn btn-success js-choose-image">Choose</a>
                    </div>
                  </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane fade in" id="tab-upload" aria-labelledby="home-tab">
                <div class="row">
                    <form action="{{ route('cms.media.create' , ['media_type' => $media_type]) }}" class="dropzone needsclick dz-clickable"  enctype="multipart/form-data">
                        <div class="dz-default dz-message"><span>Drop files here to upload</span></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
