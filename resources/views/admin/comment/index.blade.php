@extends('admin.layouts.base')
@section('content')
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Quản lý bình luận</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                                            </li>
                                        </ol>
                                    </nav>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- Column -->
                        <div class="card">
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert bg-success alert-dismissible mb-2">
                                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                      </button>
                                      {{ session('status') }}
                                    </div>
                                @endif
                                <h4 class="card-title">Tất cả bình luận</h4>
                                {{-- <h5 class="card-subtitle">Swipe Mode, ModeSwitch, Minimap, Sortable, SortableSwitch</h5> --}}
                                <div class="search"></div>
                                <table class="tablesaw table-bordered table-hover table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th scope="col">Bài viết</th>
                                            <th scope="col">Nội dung bình luận</th>
                                            <th scope="col">Trạng thái</th>
                                            <th scope="col">Tên / Email người BL</th>
                                            <th scope="col">Ngày tạo</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @if ($listComment)
                                        
                                            @php
                                                $page = isset($_GET['page']) ? $_GET['page'] : 1;
                                                $size = count($listComment->items());
                                            @endphp
                                            @foreach ($listComment as $index=>$comment)
                                                
                                                <tr>
                                                    <td>{{(($page - 1) * 10) + $index + 1}}</td>
                                                    <td class="title"><a class="link" href="javascript:void(0)">{{$comment->title}}</a></td>
                                                    <td>{!! $comment->content !!}</td>
                                                    <td class="status">
                                                        {!! $comment->state == 1 ? 'Đã phê duyệt'.'<br><a href="javascript:void(0)">'.$comment->fname.'</a>':'Chưa phê duyệt' !!}
                                                        
                                                    </td>
                                                    <td>
                                                        {{$comment->username}}<br>{{$comment->email}}
                                                    </td>
                                                    <td>{{$comment->created_at}}</td>
                                                    <td class="" style="white-space: nowrap;">
                                                        @if ($comment->state != 1)
                                                            <a href="javascript:void(0)" class="btn btn-outline-warning update-cmt" data-commentid="{{$comment->comment_id}}"><i class="far fa-edit"></i> Duyệt ngay</a>
                                                        @endif
                                                        <a href="javascript:void(0)" class="btn btn-outline-danger delete-btn" data-id="{{$comment->comment_id}}" data-title="Bình luận" data-url="{{route('cms.comment.delete')}}"><i class="fas fa-trash-alt"></i> Xóa</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @else    
                                            <p>Không có dữ liệu</p>
                                        @endif
                                    </tbody>
                                </table>
                                
                                <div class="text-center pagination-center">
                                    {{ $listComment->links('vendor/pagination/bootstrap-4') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
@endsection 
@section('scripts')
    <script>
        $(document).ready(function(){
            $('.update-cmt').click(function(){
                var commentId = $(this).data().commentid;
                var adminId = $('input[name="user_id"]').val();
                var self = $(this);
                $.ajax({
                    url: '/cms/comment/update/'+commentId,
                    method: 'POST',
                    dataType: 'json',
                    data: {user_id_edited: adminId},
                    success: function(response){
                        if(response.stt == 1){
                            self.hide()
                            self.closest('tr').find('.status').text('Đã phê duyệt');
                        }
                        else{
                            console.log('Failed');
                        }
                    }
                })
            });
        });
    </script>
@endsection