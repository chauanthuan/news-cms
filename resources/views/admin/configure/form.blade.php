@extends('admin.layouts.base')

@section('content')
<div class="content-wrapper">
  <div class="content-header row">
    {{-- @include('admin._layouts.components.breadcrumb') --}}
  </div>
  <div class="content-body">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-content collapse show">
            <div class="card-body">
              {{-- @include('admin._layouts.components.alert') --}}
              
              <form class="form" method="post" action="#" data-parsley-validate novalidate>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-body">
 
                  <div class="row">
                    <div class="col-md-12">
                      @foreach($configures as $key => $configure)
                      <div class="form-group row">
                        @if($configure->type == 'text')
                          <label class="col-2 col-form-label" for="{{ $configure->key }}">{{ $configure->title }}</label>
                          <div class="col-md-10">
                            <input type="text" class="form-control" name="configure[{{ $configure->key }}]" required="required" value="{{ Helpers::getObj($configure , 'value', old($configure->key)) }}">
                          </div>
                        @elseif($configure->type == 'checkbox')
                        <label for="" class="col-2 col-form-label">{{ $configure->title }}</label>
                        <div class="col-10">
                          <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="{{ $configure->key }}" name="configure[{{ $configure->key }}]" @if(Helpers::getObj($configure , 'value', old($configure->key)) == 1) checked="" @endif value="1">
                              <label class="custom-control-label" for="{{ $configure->key }}"></label>
                          </div>
                        </div>
                        @elseif($configure->type == 'textarea')
                          <label class="col-2 col-form-label" for="{{ $configure->key }}">{{ $configure->title }}</label>
                          <div class="col-md-10">
                            <textarea class="form-control" name="configure[{{ $configure->key }}]">{{ Helpers::getObj($configure , 'value', old($configure->key)) }}</textarea>
                          </div>
                        @endif
                      </div>
                      @endforeach
                    </div>                 
                  </div>      

                </div>

                <div class="form-actions">
                  <a href="{{ route('cms.dashboard') }}" class="btn btn-warning mr-1">
                    <i class="ft-x"></i> Cancel
                  </a>
                  <button type="submit" class="btn btn-primary">
                    <i class="la la-check-square-o"></i> Save
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection