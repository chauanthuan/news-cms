@extends('admin.layouts.base')
@section('content')
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Quản lý chung</h4>
                        <div class="d-flex align-items-center">

                        </div>
                    </div>
                    <div class="col-7 align-self-center">
                        <div class="d-flex no-block justify-content-end align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item">
                                                <a href="{{route('cms.dashboard')}}">Dashboard</a>
                                            </li>
                                        </ol>
                                    </nav>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="container-fluid">
                <div class="col-lg-12">
                    <div class="card bg-light">
                        <div class="card-body">
                            <div class="d-flex no-block align-items-center m-b-10">
                                <h4 class="card-title">User Details</h4>
                            </div>
                            <div class="table-responsive">
                                <table id="file_export" class="table bg-white table-bordered nowrap display">
                                    <thead>
                                        <tr>
                                            <th>Họ và tên</th>
                                            <th>Email</th>
                                            <th>Số ĐT</th>
                                            <th>Quyền</th>
                                            <th>Giới tính/Ngày sinh</th>
                                            <th>Ngày đăng ký</th>
                                            @if (\Auth::user()->hasRole('supper-admin'))
                                                <th>Action</th>
                                            @endif
                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($listUser as $user)
                                            <tr>
                                                <td>
                                                    <a href="{{\Auth::user()->hasRole('supper-admin') ? route('cms.account.update',['accountId'=>$user->id]) : 'javascript:void(0)'}}">
                                                        <img src="{{asset('web/img/users/avatar.png')}}" alt="user" class="rounded-circle" width="30" /> {{$user->name}}
                                                    </a>
                                                </td>
                                                <td>{{$user->email}}</td>
                                                <td>{{$user->phone}}</td>
                                                <td>
                                                    @if (!empty($user->role_name) && $user->role_slug  == 'supper-admin')
                                                        <span class="label label-danger">{{$user->role_name}}</span>
                                                    @elseif (!empty($user->role_name) && $user->role_slug  == 'manager')
                                                        <span class="label label-warning">{{$user->role_name}}</span>
                                                    @elseif (!empty($user->role_name) && $user->role_slug  == 'editor')
                                                        <span class="label label-info">{{$user->role_name}}</span>
                                                    @endif
                                                    
                                                </td>
                                                <td>{{!empty($user->gender) ? ($user->gender == 1 ? 'Nam': 'Nữ') : ''}}<br>{{$user->birthday}}</td>
                                                <td>{{$user->created_at}}</td>
                                                @if (\Auth::user()->hasRole('supper-admin'))
                                                <td>
                                                    <button type="button" class="btn btn-sm btn-icon btn-pure btn-outline delete-row-btn" data-toggle="tooltip" data-original-title="Delete">
                                                        <i class="ti-close" aria-hidden="true"></i>
                                                    </button>
                                                </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
@endsection 
@section('scripts')
    <script src="{{asset('admin/assets/extra-libs/DataTables/datatables.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $('#file_export').DataTable({
                dom: 'Bfrtip',
                buttons: ['csv', 'excel']
            });
        })
    </script>
@endsection