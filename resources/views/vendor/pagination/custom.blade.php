@if ($paginator->hasPages())
    <div class="mnmd-pagination__links text-center">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="mnmd-pagination__item mnmd-pagination__item-prev" href="#" disabled>
                <i class="mdicon mdicon-arrow_back"></i>
            </a> 
        @else
            <a class="mnmd-pagination__item mnmd-pagination__item-prev" href="{{ $paginator->previousPageUrl() }}">
                <i class="mdicon mdicon-arrow_back"></i>
            </a> 
        @endif
        
        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <span class="mnmd-pagination__item mnmd-pagination__item-current">{{ $page }}</span>
                    @elseif (($page == $paginator->currentPage() + 1 || $page == $paginator->currentPage() + 2) || $page == $paginator->lastPage())
                        <a class="mnmd-pagination__item" href="{{ $url }}">{{ $page }}</a>
                    @elseif ($page == $paginator->lastPage() - 1)
                        <span class="mnmd-pagination__item mnmd-pagination__dots">…</span>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="mnmd-pagination__item mnmd-pagination__item-next" href="{{ $paginator->nextPageUrl() }}">
                <i class="mdicon mdicon-arrow_forward"></i>
            </a>
        @else
            <a class="mnmd-pagination__item mnmd-pagination__item-next disabled" href="#">
                <i class="mdicon mdicon-arrow_forward"></i>
            </a>
        @endif
    </div>
@endif