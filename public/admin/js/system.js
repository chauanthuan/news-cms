/*
* @Author: XuanDo
* @Date:   2017-12-18 15:20:51
 * @Last modified by:   Xuan Do
 * @Last modified time: 2018-02-21T16:20:30+07:00
*/

var SYSTEM = {
  _alertOpen : 0,
  _webRoot   : $('meta[name=webRoot]').attr("content"),
  init : function() {

    SYSTEM.form.init();
    SYSTEM.Popup.init();
    SYSTEM.Select.init();
    SYSTEM.Media.init();

    $('.js-delete-item').off('click').on('click' , SYSTEM.form.deleteItem);
    $('.js-copy-text').on('change', function(e){
      var name = $(this).attr('name');
      $(this).parents('.js-parents').find('input[name='+name+'_name]').val($(this).find('option:selected').text());
    });

    // Remember checkbox
    if($('.chk-custom').length){
      $('.chk-custom').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
      });
    }
    
  },
  showWaiting : function() {
    SYSTEM.alert('INFO' , 'Processing......');
  },
  hideWaiting : function() {
    toastr.clear()
  }
};

SYSTEM.alert = function(_type , message , $title , $options) {
  switch(_type) {
    case 'INFO':
      toastr.info(message , $title , $options);
    break;
    case 'SUCCESS':
       toastr.success(message , $title , $options);
    break;
    case 'ERROR':
       toastr.error(message , $title , $options);
    break;
  }
}

SYSTEM.Select = {
  init : function() {
    $('.js-load-select').off().on('change', SYSTEM.Select.changeValue);
  },
  changeValue: function(e) {
    e.preventDefault();
    var _this = $(this);
    var append = _this.attr('data-append');
    SYSTEM.showWaiting();
    SYSTEM.httpRequest.send($(this).attr('data-href') + $(this).val() , 'get');
    SYSTEM.httpRequest.request.done(function (response, textStatus, jqXHR){
      SYSTEM.hideWaiting();
      if(response.status == 1) {
        $('.'+ append).html(response.data.lists);
        SYSTEM.Select.init();
      } 
    });

  }
};

SYSTEM.Popup = {
  init : function() {
    $('.js-popup-delete').on('click' , SYSTEM.Popup.delete);
  },
  delete : function(e) {
    e.preventDefault();
    var element  = $(this).parents('tr');
    var deleteUrl = $(this).attr('href');
    swal({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#0CC27E',
      cancelButtonColor: '#FF586B',
      confirmButtonText: 'Delete',
      cancelButtonText: "No, cancel"
    }).then(function (isConfirm) {
      if (isConfirm) {
        SYSTEM.httpRequest.send( deleteUrl, 'get');
        SYSTEM.httpRequest.request.done(function (jqXHR, textStatus, jqXHR){
          swal(
            'Deleted!',
            'This item deleted!!',
            'success'
          );
          $(element).remove();
        });
      }
    }).catch(swal.noop);

  }
};

SYSTEM.form = {
  _container : '.bs-item-modal-lg',
  _delete : Object,
  init : function() {

    $('.js-load-form').off('click').on('click' , SYSTEM.form.loadForm);
    $('.js-hide-modal').off('click').on('click' , SYSTEM.form.hideModal);
    $('.js-submit-modal').off('click').on('click' , SYSTEM.form.submit);
    $('.js-show-delete').off('click').on('click' , SYSTEM.form.showDelete);
  },
  deleteTR : function(e) {
    $(this).parents('tr').remove();
  },
  hideModal : function(e) {
    $(SYSTEM.form._container).modal('hide');
  },
  loadForm : function(e) {
    e.preventDefault();
    var element = $(this);

    SYSTEM.showWaiting();
    SYSTEM.httpRequest.send($(this).attr('href'), 'get');
    SYSTEM.httpRequest.request.done(function (response, textStatus, jqXHR){
      $(SYSTEM.form._container +' .modal-title').html(response.data.title);
      $(SYSTEM.form._container +' .modal-body').html(response.data.template);
      $(SYSTEM.form._container).modal('show');      
      SYSTEM.hideWaiting();
      SYSTEM.form.init();

      if(element.attr("data-function") != 'undefined' && element.attr("data-function") != '' ) {
        var fn = element.attr("data-function");
        window[fn].call();
      }
      //$(".select2_multiple").select2({ width: '100%' });
    });
  },
  showDelete : function(e) {
    e.preventDefault();
    $('.js-delete-modal').modal('show');
    $('.js-delete-item').attr('data-href' , $(this).attr('href'));
    $('.js-title-delete').text( $(this).parents('tr').find('.js-title').text() );

    SYSTEM.form._delete = $(this);
  },
  deleteItem : function(e) {
    e.preventDefault();
    SYSTEM.httpRequest.send($(this).attr('data-href'), 'get');
    SYSTEM.httpRequest.request.done(function (response, textStatus, jqXHR){
      if(response.status == 1 || response.status == 15) {
          if(response.message == '') {
            SYSTEM.alert('SUCCESS' , 'The item is deleted......');
          } else {
            SYSTEM.alert('SUCCESS' , response.message);
          }
          if(response.status == 15) {
            location.reload();
          }
      } else {
        SYSTEM.alert('ERROR' , response.message );
      }
    });
    $(SYSTEM.form._delete).parents('tr').remove();
    $('.js-delete-modal').modal('hide');
  },
  submit : function(e) {
    e.preventDefault();
    SYSTEM.showWaiting();
    SYSTEM.httpRequest.send($(this).parents('form').attr('action'), 'post' , $(this).parents('form').serialize());
    SYSTEM.httpRequest.request.done(function (response, textStatus, jqXHR){
      if(response.status == 1 || response.status == 10 || response.status == 15) {
          if(response.message == '') {
            SYSTEM.alert('SUCCESS' , 'Please refresh the page to show new content......');
          } else {
            SYSTEM.alert('SUCCESS' , response.message);
          }
          if(response.status == 15) {
            location.reload();
          }
          if(response.status == 1) {
            SYSTEM.form.hideModal();
          }
      } else {
        SYSTEM.alert('ERROR' , response.message );
      }

      setTimeout(function(){ SYSTEM.hideWaiting();  }, 2000);

    });
  }
}


SYSTEM.Media = {
    _isMultiple : 0,
    _append     : 0,
    _name       : 'media',
    _arrMedia   : [],
    _urlMedia   : '',
    _formMedia  : 'MediaForm',
    _container  : '' ,
    init: function () {
      $('.js-media-form').on('click', SYSTEM.Media.showMediaForm);
      $('.js-delete-image').on('click', SYSTEM.Media.deleteImage);
    },
    tinyMCEMedia: function (mediaType, field_name) {
        SYSTEM.Media._urlMedia  = SYSTEM._webRoot + "/cms/media?mediaType=" + mediaType;
        SYSTEM.Media._formMedia = "MCEMedia"
        SYSTEM.Media._container = field_name;
        SYSTEM.Media.loadMedia();
        return false;
    },
    showMediaForm: function (e) {
      e.preventDefault();
      SYSTEM.Media._container  = $(this);
      SYSTEM.Media._urlMedia   = $(this).attr('href');
      SYSTEM.Media._isMultiple = $(this).attr('data-multiple');
      SYSTEM.Media._name       = $(this).attr('data-name');
      SYSTEM.Media._append     = $(this).attr('data-append');
      SYSTEM.Media.loadMedia();
      return false;
    },
    deleteImage: function(e) {
      e.preventDefault();
      $(this).parents('.block-image').remove();
    },
    loadNewMedia: function (e) {
        e.preventDefault();
        SYSTEM.Media.loadMedia();
    },
    loadMedia: function () {

        SYSTEM.showWaiting();
        SYSTEM.httpRequest.send(SYSTEM.Media._urlMedia  , 'get');
        SYSTEM.httpRequest.request.done(function (response, textStatus, jqXHR) {
            $(SYSTEM.form._container + ' .modal-body').html(response);
            $(SYSTEM.form._container).modal('show');
            SYSTEM.hideWaiting();

            $('.js-delete-media').on('click', SYSTEM.Media.deleteMediaForm);
            $('.js-media-item a.athumb').on('click', SYSTEM.Media.chooseImageForButton);
            $('.js-load-image').off('click').on('click', SYSTEM.Media.loadMoreImage);
            $('.js-list-media').off('click').on('click', SYSTEM.Media.loadNewMedia);
            $('.dropzone').dropzone();

            if (SYSTEM.Media._formMedia == "MCEMedia") {
                $('#mce-modal-block').remove();
                $('.mce-container , #mce-modal-block').css('z-index', 1);
            }
            if(SYSTEM.Media._isMultiple != 1) {
              $('.block-media .block-accept').hide();
            } else {
              $('.js-choose-image').off('click').on('click', SYSTEM.Media.renderMultiImage);
            }
        });
    },
    deleteMediaForm: function (e) {
        e.preventDefault();
        var element = $(this);
        var fileName = $(this).parents(".album-image").find('.js-title').text();
        if (confirm("Bạn có chắc chắn muốn xóa " + fileName + " ?")) {
            SYSTEM.httpRequest.send($(this).attr('href'), 'get');
            SYSTEM.httpRequest.request.done(function (response, textStatus, jqXHR) {
                SYSTEM.alert('SUCCESS', fileName + ' Đã xóa thành công ......');
            });
            $(element).parents('.js-media-item').remove();
        }
        return false;
    },
    loadMoreImage: function (e) {
        e.preventDefault();

        var element = $(this);
        var currentPage = parseInt($(this).attr('data-currentPage'));
        var maxPage = parseInt($(this).attr('data-maxpage'));
        SYSTEM.httpRequest.send($(this).attr('href') + "&page=" + currentPage, 'get');
        SYSTEM.httpRequest.request.done(function (response, textStatus, jqXHR) {
            $(".js-image-list").append(response);

            $('.js-delete-media').on('click', SYSTEM.Media.deleteMediaForm);
            $('.js-media-item a.athumb').on('click', SYSTEM.Media.chooseImageForButton);

            currentPage = currentPage + 1;
            $(element).attr('data-currentPage' , currentPage);
            if (currentPage > maxPage) {
                $(element).css('display', 'none');
            }

        });
    },
    chooseImageForButton: function (e) {
        e.preventDefault();
        var _isHide = true;
        var _mediaLink = $(this).attr('href');
        var _urlImg = $(this).find('img').attr('src');

        if (SYSTEM.Media._formMedia == "MediaForm") {
          var mediaId = $(this).parents('.js-media-item').attr('data-media-id');
          if(SYSTEM.Media._isMultiple != 1) {
            if(SYSTEM.Media._append == 1) {
              $(SYSTEM.Media._container).parents('.form-group').find('img').attr('src',_urlImg);
              $(SYSTEM.Media._container).parents('.form-group').find('input').val(_urlImg);
            } else {
              var htmlMedia = SYSTEM.Media.renderImageHtml(mediaId , _mediaLink , _urlImg);
              $(SYSTEM.Media._container).parents('.form-group').find('.list-image').html(htmlMedia);
            }            
            _isHide = true;
          } else {

            if( $(this).parents('.js-media-item').hasClass('choose-media') ) {

              for (var i = 0; i < SYSTEM.Media._arrMedia.length; i++) {
                var mediaItem = SYSTEM.Media._arrMedia[i];
                if(mediaItem.mediaId == mediaId) {
                  SYSTEM.Media._arrMedia.splice(i, 1);
                }
              }
              $(this).parents('.js-media-item').removeClass('choose-media');
            } else {
              var mediaTemp = {};
              mediaTemp.mediaId   = mediaId;
              mediaTemp.mediaLink = _mediaLink;
              mediaTemp.urlImg    = _urlImg;
              SYSTEM.Media._arrMedia.push(mediaTemp);

              $(this).parents('.js-media-item').addClass('choose-media');
            }
            _isHide = false;
            $('.js-delete-image').on('click', SYSTEM.Media.deleteImage);
          }
        } else if (SYSTEM.Media._formMedia == "MCEMedia") {

            console.log(SYSTEM.Media._container);
            document.getElementById(SYSTEM.Media._container).value = _urlImg;
            $('.mce-container , #mce-modal-block').css('z-index', 999);
        }
        if(_isHide) $(SYSTEM.form._container).modal('hide');
    },
    renderMultiImage : function() {
      var html = '';
      for (var i = 0; i < SYSTEM.Media._arrMedia.length; i++) {
        var mediaItem = SYSTEM.Media._arrMedia[i];
        html += SYSTEM.Media.renderImageHtml(mediaItem.mediaId , mediaItem.mediaLink , mediaItem.urlImg);
      }
      $(SYSTEM.Media._container).parents('.row-item').find('.list-image').append(html);
      $('.js-delete-image').off('click').on('click', SYSTEM.Media.deleteImage);
      $(SYSTEM.form._container).modal('hide');
    },
    renderImageHtml : function(_mediaId , _mediaLink , _urlImg) {
      var html = '';      
      html += '<div class="block-image media-item js-draggable col-md-4 file-man-box mr1">';
      html += '<a href="#" class="file-close js-delete-image"><i class="mdi mdi-close-circle "></i></a>';
      html += '<a class="link-image" href="' + _mediaLink +'">';
      html += '<div class="img"><img src="' + _urlImg + '" class="thumb-detail" /></div>';
      html += '</a>';
      html += '<a class="danger js-delete-image" href="#"><i class="ft-x font-medium-3 mr-2"></i></a>';
      html += '<input type="hidden" name="'+SYSTEM.Media._name+'" value="'+ _mediaLink +'" />';
      html += '</div>';
      return html;
    }
}


SYSTEM.httpRequest = {
  request : Object ,
  send : function(_url , _method , _formData) {
    //if(_method == 'post') _formData._token = csrfToken;
    SYSTEM.httpRequest.request = $.ajax({
      url: _url,
      type: _method,
      data: _formData
    });

    //SYSTEM.httpRequest.request.done(function (response, textStatus, jqXHR){
    //  console.log(response);
   // });
    SYSTEM.httpRequest.request.fail(function (jqXHR, textStatus, errorThrown){
      console.log("The following error occurred: "+textStatus, errorThrown);
    });

  }
}


SYSTEM.Loading = {
  _time : 20000,
  _delay : Object,
  show : function() {
    $('body').loadingModal({text: 'Vui lòng chờ trong giây lát....'});
    var delay = function(ms){ return new Promise(function(r) { setTimeout(r, ms) }) };
    var time = SYSTEM.Loading._time;
    delay(time).then(function() { $('body').loadingModal('animation', 'rotatingPlane'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'wave'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'wanderingCubes'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'spinner'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'chasingDots'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'threeBounce'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'circle'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'cubeGrid'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'fadingCircle'); return delay(time);})
    .then(function() { $('body').loadingModal('animation', 'foldingCube'); return delay(time); } )
    .then(function() { $('body').loadingModal('color', 'black').loadingModal('text', 'Done :-)');  return delay(time); } )
    .then(function() { $('body').loadingModal('hide').loadingModal('destroy'); } );
  },
  hide : function () {
    $('body').loadingModal('destroy');
  }
};


SYSTEM.SortTable = {
  init : function() {
    if( typeof SORTING != 'undefined' && SORTING == 1 ) {

      RowSorter('table', {
        handler: 'td.js-sorter',
        stickFirstRow : true,
        stickLastRow  : false,
        onDragStart: function(tbody, row, index){
            console.log('start index is ' + index);
        },
        onDrop: function(tbody, row, new_index, old_index) {
          console.log('sorted from ' + old_index + ' to ' + new_index);
          SYSTEM.SortTable.submit(tbody);
        }
      }); 

    }
  },
  submit : function(tbody) {
    var sort = '';
    $(tbody).find('tr').each(function(index , item){
      var content_id = $(this).find('input[name=cid]').val();
      var current_index = $(this).find('input[name=cid]').attr('data-index');
      sort += (parseInt(current_index) + parseInt(index) )  + '-' +content_id + "|";
    });
    var url = $(tbody).parents('table').attr('data-sort');
    SYSTEM.httpRequest.send(url + '?sort=' +sort , 'get');
  }
}

jQuery(document).ready(function ($) {
  $('.js-select2').select2();
  
  // $('img.lazyload').lazyload();
  // $("[data-fancybox]").fancybox({
  //   autoSize: true,
  //   autoScale: true,
  //   autoDimensions: true,
  //   animationEffect : false,
  // });

  // $('.js-daterange').daterangepicker({
  //     autoApply:true
  // });

});


window.Parsley.on('field:error', function (fieldInstance) {
  var attr = fieldInstance.$element.parents('.tab-pane').attr('id');
  if(typeof attr !== typeof undefined && attr !== false) { 
    var tab = fieldInstance.$element.parents('.tab-pane').attr('id');
    if(tab.length > 0 && !fieldInstance.$element.parents('.tab-pane').hasClass('active') ) {
      $('.nav-tabs a[href="#'+tab+'"]').tab('show')
    }
  }
});


$(document).ready(function () {
  SYSTEM.init();
  SYSTEM.SortTable.init();
});
