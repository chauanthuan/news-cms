//Warning Message
$('.delete-btn').click(function(){
    var id = $(this).data().id;
    var route = $(this).data().url;
    var title = $(this).data().title;
    var self = $(this);
    swal({   
        title: "Bạn có chắc?",   
        text: title + " sẽ bị xóa khỏi hệ thống!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: "Xóa ngay!",   
        cancelButtonText: "Đóng",   
        allowOutsideClick: false
    }).then(function(isConfirm){  
        if (isConfirm.value == true) {  
            $.ajax({
                url: route,
                method: 'POST',
                dataType: 'json',
                data: {id: id},
                success: function(response){
                    if(response.stt == 1){
                        self.closest('tr').remove();
                        swal("Xóa thành công!", title + " đã được xóa khỏi hệ thống.", "success");   
                    }
                    else{
                        swal("Thất bại", "Có lỗi xảy ra khi xóa "+title, "error");   
                    }
                }
            }).done({
                
            })
            
        } 
        else {  
        
        } 
    });
});

$('.btn-profile').click(function(){
    var id = $(this).data().id;
    var userid = $('input[name="id"]').val();
    var route = $(this).data().url;
    var title = $(this).data().title;
    var address = $('textarea.address').val();
    var birthday = $('input[name="birthday"]').val();
    var gender = $('input[name="gender"]:checked').val();
    var phone = $('input[name="phone"]').val();
    var company = $('input[name="company"]').val();
    var self = $(this);
    swal({   
        title: "Bạn có chắc?",   
        text: title + " thông tin chi tiết cho tài khoản này!",   
        type: "warning",   
        showCancelButton: true,   
        confirmButtonColor: "#DD6B55",   
        confirmButtonText: title+" ngay!",   
        cancelButtonText: "Đóng",   
        allowOutsideClick: false
    }).then(function(isConfirm){  
        if (isConfirm.value == true) {  
            $.ajax({
                url: route,
                method: 'POST',
                dataType: 'json',
                data: {profileid: id, user_id: userid, address:address, gender: gender, birthday: birthday,phone: phone, company:company},
                success: function(response){
                    if(response.stt == 1){
                        swal("Thành công!", title + " thông tin chi tiết cho tài khoản này.", "success"); 
                        location.reload();  
                    }
                    else{
                        swal("Thất bại", "Có lỗi xảy ra khi "+title+" thông tin.", "error");   
                    }
                }
            }).done({
                
            })
            
        } 
        else {  
        
        } 
    });
});