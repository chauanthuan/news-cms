$(function() {
  tinymce.init({
      entity_encoding : "raw",
      selector: ".texteditor",
      height: 400,
      plugins: [
          "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
          "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
          "save table contextmenu directionality emoticons template paste textcolor colorpicker code media"
    ],
      image_dimensions: false,
      relative_urls: true,
      convert_urls: false,
      remove_script_host: true,
      font_size_style_values: ["12px,13px,14px,16px,18px,20px"],
      extended_valid_elements : 'hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style],p[class|align|style],img[href|src|name|title|onclick|align|alt|title|width|height|vspace|hspace],iframe[id|class|width|size|noshade|src|height|frameborder|border|marginwidth|marginheight|target|scrolling|allowtransparency],style[type]',
      browser_spellcheck: true,
      apply_source_formatting: true,
      file_browser_callback: function (field_name, url, type, win) {
          if (type == "image") {
              SYSTEM.Media.tinyMCEMedia("image", field_name);
          } else if (type == "file" || type == "media") {
            SYSTEM.Media.tinyMCEMedia("document", field_name);
          }

      },
      codemirror: {
        indentOnInit: true,
        path: 'CodeMirror'
      },
    image_advtab: true,
    toolbar1: "fontsizeselect | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | preview fullpage | forecolor backcolor emoticons | responsivefilemanager | image | media | link unlink anchor | print preview  | forecolor backcolor | code"
  });

  tinymce.init({
    entity_encoding : "raw",
    selector: '.texteditor-basic',
    height: 150,
    menubar: false,
    plugins: [
      'advlist autolink lists link image charmap print preview anchor textcolor',
      'searchreplace visualblocks code fullscreen',
      'insertdatetime media table paste code help wordcount'
    ],
    toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help | link unlink anchor'
  });
});
