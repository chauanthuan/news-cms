<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleCategory extends Model
{
    protected $table = 'article_category';
    public $primaryKey = 'article_category_id';
    protected $fillable = ['article_id', 'category_id'];
    
    public function article(){
        return $this->belongsToMany('App\Models\Article');
    }

    public function categories(){
        return $this->belongsToMany('App\Models\Category');
    }

}
