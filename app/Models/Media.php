<?php
namespace App\Models;
use App\Helpers\Helpers;

use Illuminate\Database\Eloquent\Model;
use DB;

class Media extends Model {

  const STATE_INACTIVE  = 0;
  const STATE_ACTIVE    = 1;

  const MEDIA_TYPE_IMAGE  = "IMAGE";
  const MEDIA_TYPE_FILE   = "FILE";

  protected $primaryKey = 'media_id';

  protected $table = 'media';

  protected $fillable = [
    'media_type' , 'filename', 'state' , 'create_by'
  ];

  public function getAllMedia($item = 100, $filter = null, $orderBy = 'media_id', $order = 'DESC') {
    $result = $this->select('media.*','ar.title','ar.id as article_id','u.name','u.id as user_id')
              ->join('users as u','u.id','=','media.create_by')
              ->leftjoin('articles as ar','ar.picture','=','media.media_id');
    if (isset($filter['state']) && !empty($filter['state'])) $result->where('media.state', '=', $filter['state']);
    if (isset($filter['keyword']) && !empty($filter['keyword']) ) $result->whereRaw('media.filename like ("%'.$filter['keyword'].'%")
    OR media.filename like ("%'.Helpers::convertAscii($filter['keyword']).'%")');
    $result->orderBy('media.media_id', 'DESC');
    return $result->paginate($item);
  }

}
