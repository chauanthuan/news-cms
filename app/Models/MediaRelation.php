<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use DB;

class MediaRelation extends Model {

  protected $primaryKey = 'media_relation_id';

  protected $table = 'media_relation';

  protected $fillable = [
    'media_id' , 'obj_id', 'obj_type', 'path'
  ];

  public function articles()
  {
      return $this->belongsToMany('App\Models\Article','obj_id','id');
  }
}
