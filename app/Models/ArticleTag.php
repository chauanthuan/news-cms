<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleTag extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'article_tag';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'article_id','tag_id'
    ];

    public function article(){
        return $this->belongsTo('App\Models\Article');
    }

    public function tags(){
        return $this->hasMany('App\Models\Tag');
    }

}