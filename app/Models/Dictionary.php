<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dictionary extends Model
{
    public $primaryKey = 'dictionary_id';

  	public $table = 'dictionary';

  	public $fillable = ['local_word', 'common_word', 'example', 'user_id', 'user_edited_id', 'state', 'created_at', 'updated_at'];
  	
  	protected $hidden = [];
}
