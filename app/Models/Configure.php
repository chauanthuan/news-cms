<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Configure extends Model  {

  public $primaryKey = 'id';

  public $table     = 'configure';

  public $guarded   = [];            

  protected $hidden = [];

  public function getAll() {
    $result = $this->select('configure.*');
    return $result->get();
  }

}
