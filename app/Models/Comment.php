<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    protected $primaryKey = 'comment_id';
	protected $fillable = ['content', 'comment_parent_id', 'username', 'email', 'state', 'user_id_edited', 'article_id'];

    public function children()
    {
        return $this->hasMany(Comment::class, 'comment_parent_id');
    }
}