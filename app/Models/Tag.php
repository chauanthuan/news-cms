<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'tags';

    protected $fillable = [
        'name','slug'
    ];

    public function article()
    {
        return $this->belongsToMany('App\Models\Article');
    }
}
