<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public $primaryKey = 'customer_id';

  	public $table = 'customer';

  	public $fillable = ['name', 'phone', 'password', 'remember_token', 'isupdate', 'state', 'created_at', 'updated_at'];
  	
  	protected $hidden = [];
}
