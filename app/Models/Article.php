<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    const TYPE_ARTICLE  = 1;
    const TYPE_GALLERY  = 2;
    const TYPE_VIDEO    = 3;

    protected $table = 'articles';
    public $primaryKey = 'id';

    protected $fillable = ['title', 'sumary','slug','article_type',
    'content_html','content_array','user_id','user_id_edited','origin_link','video_url','author',
    'source','picture','allow_comment', 'created_at', 'updated_at','thumb_array','picture_des','state','show_author','published_date','hide_thumbnail','featured'];


    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }
    public function mediaRelation()
    {
        return $this->belongsToMany('App\Models\MediaRelation','id','obj_id');
    }
    /**
     * Set the title attribute and automatically the slug
     *
     * @param string $value
     */
    public function setTitleAttribute($value)
    {
        $this->attributes['title'] = $value;

        if (! $this->exists) {
            $this->setUniqueSlug($value, '');
        }
    }

    /**
     * Recursive routine to set a unique slug
     *
     * @param string $title
     * @param mixed $extra
     */
    protected function setUniqueSlug($title, $extra)
    {
        $slug = str_slug($title.'-'.$extra);

        if (static::whereSlug($slug)->exists()) {
            $this->setUniqueSlug($title, $extra + 1);
            return;
        }

        $this->attributes['slug'] = $slug;
    }
}
