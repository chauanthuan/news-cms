<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';
    public $primaryKey = 'id';
    protected $fillable = ['name','slug'];

    public function roles() {
        return $this->belongsToMany(Role::class,'roles_permissions');
    }
}
