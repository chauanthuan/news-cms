<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    protected $table = 'user_profile';
    public $primaryKey = 'id';
	protected $fillable = ['gender','birthday','address','user_id','user_id_edited','phone','company'];

    public function user() { 
        return $this->belongsTo(User::class); 
    }
}
