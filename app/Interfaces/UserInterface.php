<?php

namespace App\Interfaces;

interface UserInterface {
    public function getAll($limit);
    public function createUser(array $data);
}
