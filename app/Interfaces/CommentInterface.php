<?php

namespace App\Interfaces;

interface CommentInterface
{
    public function createComment(array $data);
    public function adminGetAllComment($limit);
}