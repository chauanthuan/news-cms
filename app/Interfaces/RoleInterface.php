<?php

namespace App\Interfaces;

interface RoleInterface {
    public function getAll($limit);
    public function createRole(array $data);
}