<?php

namespace App\Interfaces;

interface ArticleCategoryInterface
{

    public function addArticleCategory(array $data);
}