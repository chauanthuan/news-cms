<?php

namespace App\Interfaces;

interface PermissionInterface {
    public function getAll($limit);
    public function createPermission(array $data);
}