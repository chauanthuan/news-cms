<?php
namespace App\Interfaces;


interface TagInterface
{
	public function getDetail($tagSlug);
	public function createTag(array $data);
}