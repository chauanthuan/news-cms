<?php

namespace App\Interfaces;

interface ArticleTagInterface
{

    public function addArticleTag(array $data);

    public function getByArticleId($article_id);

    public function findTag($article_id, $tagName);

    public function removeAllTags($article_id);
}