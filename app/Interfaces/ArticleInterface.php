<?php

namespace App\Interfaces;

interface ArticleInterface {

    public function getPopularPost($limit);
    public function getLatestPost($limit);
    public function getListArticleByCate($cateId,$limit = 10);
    public function createArticle(array $data);
    public function adminGetAllArticle($limit);
    public function getArticleByCondition($filter, $limit = 10);
}