<?php
namespace App\Interfaces;

interface CategoryInterface{

	public function getCategories();

	public function createCategory(array $data);
}