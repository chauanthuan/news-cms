<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

class CreateSiteMap extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sitemap:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sitemap = \App::make('sitemap');
        // add home pages mặc định
        $sitemap->add(route('web.index'), Carbon::now(), '1.0', 'daily');

        // add bài viết
        $posts = \DB::table('articles as ar')->select("ar.slug","ar.created_at","ct.slug as ct_slug")
                ->join('article_category as ac','ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->where('ar.state',1)
                ->where('ct.state',1)
                ->limit(500)
                ->orderBy('ar.created_at','DESC')->get();

        foreach ($posts as $post) {
            $sitemap->add(route('web.article', ['categorySlug'=>$post->ct_slug,'articleSlug'=>$post->slug,]), $post->created_at, '0.9', 'daily');
        }

        // lưu file và phân quyền
        $sitemap->store('xml', 'sitemap');
        if (\File::exists(public_path() . '/sitemap.xml')) {
                chmod(public_path() . '/sitemap.xml', 0777);
        }
    }
}
