<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\Interfaces\ArticleInterface;
class ChangeArticleState extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'article_state:active';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Article State to active';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ArticleInterface $article)
    {
        parent::__construct();
        $this->article = $article;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::transaction(function(){
            $count_article = $this->article->where('state','<>',1)
                ->where('published_date','<>','')
                ->where('published_date', '<=', Carbon::now())->update(['state'=>1,'created_at'=>\DB::raw("`published_date`")]);
             \Log::info('Have '.$count_article.' change state to Active');
        });
    }
}
