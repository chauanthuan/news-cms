<?php
# @Author: Xuan Do
# @Date:   2018-02-01T17:34:00+07:00
# @Email:  ngocxuan2255@gmail.com
# @Last modified by:   Xuan Do
# @Last modified time: 2018-03-06T17:17:42+07:00

namespace App\Helpers;

use Request;
use Carbon\Carbon;

class Helpers {

  public static $lang      = 'en';
  public static $languages = [
    'en' => ['code' => 'en', 'name' => 'English'],
    'vi' => ['code' => 'vi', 'name' => 'Việt Nam']    
  ];

  public static function isWeekend($date) {
    return (date('N', strtotime($date)) >= 6);
  }

  public static function trans($key , $type = 'content', $replace = [] , $locale = '') {   
    // $myfile = fopen(storage_path("lang.txt"), "a") or die("Unable to open file!");
    // fwrite($myfile, "\n".'-'.$key.'|'.$key.'_');
    // fclose($myfile);

    $key = \App::getLocale() == 'vi' ? $type.'.'.$key : $key;
    return __($key, $replace, $locale);
  }

  public static function checkActive($route , $page , $result = 'class=active') {
    return $route == $page ? $result : '';
  }
  
  public static function storagePath($file = '') {
    return storage_path($file);
  }

  public static function setLocale($lang , $default = 'en') {
    $locale = isset($lang) && in_array($lang , Helpers::$laguages) ? $lang : $default;
    \App::setLocale($locale);
  }

  public static function getSlug($id, $slug) {
    return $id.'-'.$slug;
  }

  public static function getJson($path) {
    if(!empty($path) && file_exists(Helpers::storagePath($path)) ) {
      $json = file_get_contents(Helpers::storagePath($path));
      return json_decode($json, true);
    }
    return [];
  }

  public static function searchValInArray($arr , $field , $value) {
    $temp = [];
    $value = Helpers::slugify($value);
    foreach ($arr as $key => $item) {
      $search = Helpers::slugify($item[$field]);
      if (strpos($search, $value) !== FALSE) {
       $temp[$key] = $item;
      }
    }
    return $temp;
  }

  public static function addMDate($date , $mins , $format = 'Y-m-d H:i:s') {
    return date($format , strtotime($date . $mins." minutes"));
  }

  public static function getInterger($string) {
    return preg_replace('/[^0-9]/', '', $string);
  }

  public static function replaceArr ($str , $arr , $isTrim = true) {
    foreach ($arr as $find => $replace) {
      $str = Helpers::replace($str , $find , $replace);
    }
    return $isTrim ? trim($str) : $str;
  }

  public static function replace ($str , $find , $replace) {
    return str_replace($find, $replace, $str);
  }

  public static function isNumeric($number) {
    return is_numeric($number);
  }


  public static function arrKeys($arr) {
    return array_keys($arr);
  }

  public static function convertDate($date , $format = 'Y-m-d H:i:s') {
    return !empty($date) ? date($format , strtotime($date)) : $date;
  }

  public static function parseDateRange($dateRange , $hasDefault = true , $comas = '-' , $format='Y-m-d H:i:s') {
    if(!empty($dateRange))  {
      $dateRange = explode($comas, $dateRange);
      if(count($dateRange) > 0) {
        return ['start_date' => Helpers::convertDate($dateRange[0] , $format)  ,
                'end_date' => Helpers::convertDate($dateRange[1] , $format)];
      }
    }    
    return $hasDefault ? ['start_date' => Carbon::now()->startOfMonth()->toDateTimeString()  ,'end_date' => Carbon::now()->endOfMonth()->toDateTimeString() ] : array();
  }

  public static function parseYearRange($dateRange , $hasDefault = true , $comas = '-' , $format='Y-m-d H:i:s') {
    if(!empty($dateRange))  {
      $dateRange = explode($comas, $dateRange);
      if(count($dateRange) > 0) {
        return ['start_date' => Helpers::convertDate($dateRange[0] , $format)  ,
                'end_date' => Helpers::convertDate($dateRange[1] , $format)];
      }
    }
    return $hasDefault ? ['start_date' => Carbon::now()->startOfYear()  ,'end_date' => Carbon::now()->endOfYear() ] : array();    
  }

  public static function formatNumberWithLabel($land_area, $number = 2 , $label = ' (Ha)'){
    return number_format($land_area, $number, ',', '.').$label;
  }

  public static function jsonDecode($arr, $isArray = false, $default = []) {
    return !empty($arr) ? json_decode($arr , $isArray) : $default;
  }

  public static function jsonEncode($arr , $default = array()) {
    return !empty($arr) ? json_encode($arr) : $default;
  }

  public static function prepareJson($item , $key) {
    $item = (array)$item;
    if(isset($item[$key]) && !empty($item[$key])) {
      return Helpers::jsonEncode($item[$key]);
    }
    return '';
  }

  public static function socialUrl($socialId , $socialType = '') {
    return !empty($socialId) ? 'https://facebook.com/'.$socialId : '';
  }

  public static function explode ($separator , $str ) {
    return !empty($str) ? explode($separator, $str ) : [];
  }

  public static function isEmpty($item , $key) {
    $item = (array)$item;
    return isset($item[$key]) && !empty($item[$key]) ? false : true;
  }

  public static function getVal($item , $key , $default = '') {
    $item = (array)$item;
    return isset($item[$key]) && !empty($item[$key]) ? $item[$key] : $default;
  }

  public static function getInt($item , $key , $default = 0) {
    $item = (array)$item;
    return isset($item[$key]) ? intval($item[$key]) : $default;
  }

  public static function getObj($item , $key , $default = 0) {
    return isset($item->{$key}) ? $item->{$key} : $default;
  }

  public static function getObjLang($item , $key , $default = '' , $lang = '') {
    $locale = empty($lang) ? \App::getLocale() : $lang;
    return isset($item->{$key.'_'.$locale}) && !empty($item->{$key.'_'.$locale}) ? $item->{$key.'_'.$locale} : $default;
  }

  public static function limit($text , $limit = 0 , $comas = '...') {
    return $limit > 0 ? str_limit($text, $limit,$comas) : $text;
  }  

  public static function selected($item , $val , $selected = 'selected') {
    return $item ==  $val ? $selected : '';
  }  

  public static function getFisrt($array) {
    return count($array) > 0 ? reset($array) : array() ;
  }

  public static function slugify($str  = NULL, $sperator="-"){
    return str_slug($str , $sperator);
  }

  public static function generateTokenId($table = '') {
    $time = explode(' ', microtime());
    return $table.date('YmdHis') . substr($time[0], 2, 6);
  }

  public static function sanitize($str  = NULL, $sperator="-"){
    if(!$str) return NULL;
    $str = html_entity_decode($str);
    $unicode = array(
        'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
        'd'=>'đ|Đ',
        'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|È|Ẻ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
        'i'=>'í|ì|ỉ|ĩ|ị|Í|Ì|Ỉ|Ĩ|Ị',
        'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|O|Ò|Ó|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ỡ|Ờ|Ợ',
        'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ù|Ú|Ư|Ữ|Ừ|Ự|Ử|Ủ|Ừ|Ứ|Ũ',
        'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ|Y',
        ' '=>'-'|'---'|'_'|'&'|'^'|'"'
    );
    foreach($unicode as $nonUnicode=>$uni) $str = preg_replace("/($uni)/i",$nonUnicode,$str);
    $str = str_replace(array('&amp;', '&quot;', '&lt;', '&gt;', '*', '/')," ",$str);
    $str = preg_replace("/[^a-zA-Z0-9- ]/", "-", $str);
    $str = preg_replace('/\s\s+/', ' ', $str );
    $str = trim($str);
    $str = preg_replace('/\s+/', $sperator, $str );
    $str = str_replace("----","-",$str);
    $str = str_replace("---","-",$str);
    $str = str_replace("--","-",$str);
    return strtolower($str);
  }

  public static function parseErrorToString($arr) {
    if(!is_array($arr) && empty($arr) ) return '';

    $response = '';
    foreach ($arr as $key => $error) {
      $response .= $error. '<br />';
    }
    return rtrim($response , '<br />');
  }

  public static function getRequest($url) {
    $client = new \GuzzleHttp\Client(['verify' =>false, 'http_errors' => false]);
    $requestGuzzle  = $client->request('GET', $url, []);
    $response = $requestGuzzle->getBody()->getContents();
    return Helpers::jsonDecode($response);
  }


  public static function getIdBySlug($slug) {
    $ids = explode('-', $slug);
    return intval(reset($ids));
  }

  public static function isAjax() {
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
      return true;
    }
    return false;
  }

  public static function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
      $ip= isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
    }
    return $ip;
  }

  public static function createDateRange($startDate, $endDate, $format = "Y-m-d") {
    $begin = new \DateTime($startDate);
    $end = new \DateTime($endDate);
    $interval = new \DateInterval('P1D'); // 1 Day
    $dateRange = new \DatePeriod($begin, $interval, $end->add($interval)  );
    $range = [];
    foreach ($dateRange as $date) {
      $range[] = $date->format($format);
    }
    if(!$range) $range[] = $begin->format($format);
    return $range;
  }

  public static function isMobile() {
    $userAgent = $_SERVER['HTTP_USER_AGENT'];
    if (!$userAgent) return false;
    return (
      preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i', $userAgent) ||
      preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($userAgent,0,4))
    );
  }

  public static function validatePhone($phone) {
    $regEx = '/^(03|09|08|07|05)[0-9]{8}$/';
    return preg_match($regEx, $phone);
  }

  public static function formatPhoneNumber($number) {
    return preg_replace('/^0/','84',$number);
  }

  public static function removeEmoji($string) {

    // Match Emoticons
    $regex_emoticons = '/[\x{1F600}-\x{1F64F}]/u';
    $clear_string = preg_replace($regex_emoticons, '', $string);

    // Match Miscellaneous Symbols and Pictographs
    $regex_symbols = '/[\x{1F300}-\x{1F5FF}]/u';
    $clear_string = preg_replace($regex_symbols, '', $clear_string);

    // Match Transport And Map Symbols
    $regex_transport = '/[\x{1F680}-\x{1F6FF}]/u';
    $clear_string = preg_replace($regex_transport, '', $clear_string);

    // Match Miscellaneous Symbols
    $regex_misc = '/[\x{2600}-\x{26FF}]/u';
    $clear_string = preg_replace($regex_misc, '', $clear_string);

    // Match Dingbats
    $regex_dingbats = '/[\x{2700}-\x{27BF}]/u';
    $clear_string = preg_replace($regex_dingbats, '', $clear_string);

    return $clear_string;
  }

  public static function checkEmoji($str) {
    $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
    preg_match($regexEmoticons, $str, $matches_emo);
    if (!empty($matches_emo[0])) {
      return false;
    }
    
    // Match Miscellaneous Symbols and Pictographs
    $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
    preg_match($regexSymbols, $str, $matches_sym);
    if (!empty($matches_sym[0])) {
      return false;
    }

    // Match Transport And Map Symbols
    $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
    preg_match($regexTransport, $str, $matches_trans);
    if (!empty($matches_trans[0])) {
        return false;
    }
   
    // Match Miscellaneous Symbols
    $regexMisc = '/[\x{2600}-\x{26FF}]/u';
    preg_match($regexMisc, $str, $matches_misc);
    if (!empty($matches_misc[0])) {
      return false;
    }

    // Match Dingbats
    $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
    preg_match($regexDingbats, $str, $matches_bats);
    if (!empty($matches_bats[0])) {
      return false;
    }
    return true;
  }


  public static function isDateInRange($startDate, $endDate, $cdate = '' ) {
    $cdate  = empty($cdate) ? strtotime(date('Y-m-d')) : strtotime($cdate); 
    $startT = strtotime($startDate);
    $endT   = strtotime($endDate);
    return (($cdate >= $startT) && ($cdate <= $endT));

  }

  public static function convertAscii ($str){
    // In thường
    $str = preg_replace("/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/", 'a', $str);
    $str = preg_replace("/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/", 'e', $str);
    $str = preg_replace("/(ì|í|ị|ỉ|ĩ)/", 'i', $str);
    $str = preg_replace("/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/", 'o', $str);
    $str = preg_replace("/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/", 'u', $str);
    $str = preg_replace("/(ỳ|ý|ỵ|ỷ|ỹ)/", 'y', $str);
    $str = preg_replace("/(đ)/", 'd', $str);
    // In đậm
    $str = preg_replace("/(À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ)/", 'A', $str);
    $str = preg_replace("/(È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ)/", 'E', $str);
    $str = preg_replace("/(Ì|Í|Ị|Ỉ|Ĩ)/", 'I', $str);
    $str = preg_replace("/(Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ)/", 'O', $str);
    $str = preg_replace("/(Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ)/", 'U', $str);
    $str = preg_replace("/(Ỳ|Ý|Ỵ|Ỷ|Ỹ)/", 'Y', $str);
    $str = preg_replace("/(Đ)/", 'D', $str);
    return $str; // Trả về chuỗi đã chuyển
  }

  public static function time_elapsed_string($datetime, $full = false) {
    $now = new \DateTime;
    $ago = new \DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'năm',
        'm' => 'tháng',
        'w' => 'tuần',
        'd' => 'ngày',
        'h' => 'giờ',
        'i' => 'phút',
        's' => 'giây',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v;
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? 'Cách đây '.implode(', ', $string) : 'Vừa xong';
}

}
