<?php
# @Author: XuanDo
# @Date:   2018-02-08T20:49:12+07:00
# @Email:  ngocxuan2255@gmail.com
# @Last modified by:   Xuan Do
# @Last modified time: 2018-03-07T15:34:18+07:00

namespace App\Helpers\Media;
use App\Helpers\Helpers;
use App\Models\Eloquent\MediaRelation;

class MediaHelper {

  public static function rootUploadDir($folder = 'storage') {
    // return public_path($folder);
    return storage_path('app/public');
  }

  public static function uploadDir($folder = 'storage') {
    $rootUploadDir = MediaHelper::rootUploadDir($folder);
    return MediaHelper::createUploadFolder($rootUploadDir , $folder);
  }

  public static function createUploadFolder($rootPath ) {
    $month = date('m'); $year = date('Y');
    if (!file_exists($rootPath.'/'.$year)) {
      mkdir($rootPath.'/'.$year, 0777);
    } 
    if (!file_exists($rootPath.'/'.$year.'/'.$month)) {
      mkdir($rootPath.'/'.$year.'/'.$month, 0777);
    }
    return $year.'/'.$month;
  }

  public static function makeImageFromBase64($b64Encode, $path) {
    $fileName = '';
    try {
      if( !empty($b64Encode) ) {
        $uploadDir  = MediaHelper::rootUploadDir();
        $fileName = $path.'/'.time().rand(10000 , 99999999).'.jpg';
        $image = \Intervention\Image\ImageManagerStatic::make($b64Encode);
        $image->save($uploadDir.'/'.$fileName);
      }
    } catch (\Exception $e) {      
      \App\Helpers\AppLog::error("MEDIAHELPER - CREATE IMAGE BASE64 - ".$e->getMessage());
      return '';
    }
    return $fileName;
  }

  public static function show($link , $default = '' ){
    
    if(empty($link) && empty($default)) return '';
    $default = empty($default) ? asset('admin/images/default.jpg') : $default;
    if(strpos($link, 'http://') !== 0 && strpos($link, 'https://') !== 0) {
    	if(!empty($link)) {
        $imgUrl = env('IMG_SERVER', env('APP_URL').'storage/');
        if (strpos($link, $imgUrl) === false) {
          return $imgUrl.$link;
        } else {
          return $link;
        }
    	} else {
    		$imgUrl = $default;
    	}
    } else {
      if(!empty($link)) {
        $imgUrl = $link;
      } else {
        $imgUrl = $default;
      }
    }
    return $imgUrl;
  }

 public static function renderLazyImg($imageUrl, $options = []) {
    return '<img class="lazyload '.Helpers::getVal($options, 'class').'" '.Helpers::getVal($options, 'attr').' data-original="'.MediaHelper::show($imageUrl).'"  src="'.asset('admin/images/avatar.png').'">';
  }

  public static function renderSingle($name , $media, $col = 'col-md-4 file-man-box'){
    $html = '';
    if(!empty($media)) {
    $html = '<div class="block-image media-item js-draggable '.$col.' mr1">
             <a href="#" class="file-close js-delete-image"><i class="mdi mdi-close-circle "></i></a>
              <a class="link-image" href="'.MediaHelper::show($media).'" data-fancybox>
                <div class="img" ><img src="'.MediaHelper::show($media).'" class="thumb-detail" /></div>
              </a>
              <a class="danger js-delete-image" href="#"><i class="ft-x font-medium-3 mr-2"></i></a>
              <input type="hidden" name="'.$name.'" value="'.$media.'" />
            </div>';
    }
    return $html;
  }

  public static function renderMulti($name , $medias){
    $html = '';
    if(!empty($medias) > 0) {
      foreach ($medias as $key => $media) {
        $html .= MediaHelper::renderSingle($name, $media);
      }
    }
    return $html;
  }

  public static function renderUploadButton($name , $type = 'image' , $isMulti = 0){
    $html = '';
    if(!empty($name)) {
    $html = '<a href="'.route('cms.media' , ['media_type' => $type]).'" data-multiple="'.$isMulti.'" data-name="'.$name.'" class="btn btn-info btn-icon js-media-form">
              <i class="la la-cloud-upload"></i><span>Upload'.($isMulti ? ' Multi':'').' Image</span>
            </a>';
    }
    return $html;
  }

  public static function createRelation($path , $obj , $type) {
    return MediaRelation::create([
            'path'     => $path,
            'obj_id'   => $obj,
            'obj_type' => $type,
          ]);
  }

  public static function removeObjRelation($obj, $type) {
    return MediaRelation::where('obj_id', $obj)->where('obj_type', $type)->delete();
  }

}
