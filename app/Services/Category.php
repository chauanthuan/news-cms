<?php

namespace App\Services;

use App\Interfaces\CategoryInterface;

class Category extends \App\Models\Category implements CategoryInterface
{

	
	public function getCategories ()
	{
        $query = $this->select ( "id", "name", "slug", "state", "showMenu", "showHome",'iconClass','order'  )
        ->where ( "state", 1 )
        ->orderBy ( "order" );
        return $query->get();
    }
    
    public function getDetail($categorySlug){
        $query = $this->select('*')->where('slug','=',$categorySlug)->where('state',1);
        return $query->first();
    }

    public function createCategory(array $data)
	{
		return $this->create($data);
    }
    
    public function adminGetAllCategory($limit){
        $query = $this->select('categories.*', \DB::raw('count(ac.category_id) as article_count'))
                    ->leftjoin('article_category as ac','ac.category_id','=','categories.id')
                    ->groupBy('categories.id');
        return $query->paginate($limit);
    }
}
