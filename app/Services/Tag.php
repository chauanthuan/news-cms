<?php
namespace App\Services;


use App\Interfaces\TagInterface;

class Tag extends \App\Models\Tag implements TagInterface
{
	public function getTagsName($productId){
        
    }

	public function createTag(array $data) {
		return $this->create($data);
	}

	public function getDetail($tagSlug){
		$query = $this->select('*')->where('slug','=',$tagSlug)->where('state',1);
		return $query->first();
	}
}