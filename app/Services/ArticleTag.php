<?php
namespace App\Services;


use App\Interfaces\ArticleTagInterface;

class ArticleTag extends \App\Models\ArticleTag implements ArticleTagInterface
{
    public function addArticleTag(array $data){
        
    }

    public function getByArticleId($article_id){
        
    }

    public function findTag($article_id, $tagName){
       
    }

    public function removeAllTags($article_id){
       
    }
}