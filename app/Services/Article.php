<?php

namespace App\Services;

use App\Interfaces\ArticleInterface;

class Article extends \App\Models\Article implements  ArticleInterface {
    public function getPopularPost($limit){
        $query = \DB::table('articles as ar')->select("ar.title", "ar.slug","ar.sumary","ar.view_count","ct.name as ct_name","ct.slug as ct_slug")
                ->join('article_category as ac','ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->where('ct.state',1)
                ->orderBy('ar.view_count','DESC')
                ->limit($limit);
        return $query->get();
    }
    public function getLatestPost($limit){
        $query = \DB::table('articles as ar')->select("ar.id","ar.title", "ar.slug","ar.sumary","ar.view_count","ar.picture","ar.thumb_array","ar.article_type","ct.name as ct_name","ct.slug as ct_slug"
                ,\DB::raw("COUNT(cm.article_id) AS comment_count"),
                \DB::raw("( SELECT COUNT( mr.obj_id ) FROM media_relation AS mr WHERE mr.obj_id = ar.id AND mr.obj_type = 'article' ) AS media_count"))
                ->join('article_category as ac','ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->leftjoin('comments as cm','cm.article_id','=','ar.id')
                ->where('ar.state',1)
                ->where('ct.state',1)
                ->where('ar.featured',1)
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC')
                ->limit($limit);
        return $query->get();
    }

    public function getListArticleByCate2($cateId,$limit = 10){
        $query = \DB::table('articles as ar')->select("ar.id","ar.title", "ar.slug","ar.sumary","ar.view_count","ar.picture","ar.thumb_array","ar.article_type","ct.name as ct_name","ct.slug as ct_slug"
                ,\DB::raw("COUNT(cm.article_id) AS comment_count"),
                \DB::raw("( SELECT COUNT( mr.obj_id ) FROM media_relation AS mr WHERE mr.obj_id = ar.id AND mr.obj_type = 'article' ) AS media_count"))
                ->join('article_category as ac','ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->leftjoin('comments as cm','cm.article_id','=','ar.id')
                ->where('ar.state',1)
                ->where('ct.state',1)
                ->where('ct.id',$cateId)
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC')
                ->limit(10);
        return $query->get();
    }

    public function getListArticleByCate($cateId , $limit = 10){
        $query = \DB::table('articles as ar')->select("ar.*",'ct.name as ct_name','ct.slug as ct_slug',
                    \DB::raw("( SELECT COUNT( mr.obj_id ) FROM media_relation AS mr WHERE mr.obj_id = ar.id AND mr.obj_type = 'article' ) AS media_count"))
                
                ->join('article_category as ac', 'ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                // ->leftjoin('comments as cm','cm.article_id','=','ar.id')
                ->where('ar.state',1)
                ->where('ct.state',1)
                ->where('ct.id',$cateId)
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC');
        return $query->paginate($limit);
    }


    public function getArticleByCate($cateId,$limit = 10){
        $query = \DB::table('articles as ar')->select("ar.id","ar.title", "ar.slug","ar.sumary","ar.view_count","ar.picture","ar.thumb_array","ar.article_type","ct.name as ct_name","ct.slug as ct_slug"
                ,\DB::raw("COUNT(cm.article_id) AS comment_count"),
                \DB::raw("( SELECT COUNT( mr.obj_id ) FROM media_relation AS mr WHERE mr.obj_id = ar.id AND mr.obj_type = 'article' ) AS media_count"))
                ->join('categories as ct','ct.id','=','ar.category_id')
                ->leftjoin('comments as cm','cm.article_id','=','ar.id')
                ->where('ar.state',1)
                ->where('ct.state',1)
                ->where('ct.id',$cateId)
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC');

        return $query->paginate($limit);
    }

    public function getDetail($cateId, $articleSlug){
        $query = \DB::table('articles as ar')->select("ar.*","ct.name as ct_name","ct.slug as ct_slug"
                ,\DB::raw("COUNT(cm.article_id) AS comment_count"),
                \DB::raw("( SELECT COUNT( mr.obj_id ) FROM media_relation AS mr WHERE mr.obj_id = ar.id AND mr.obj_type = 'article' ) AS media_count"))
                ->join('article_category as ac','ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->leftjoin('comments as cm','cm.article_id','=','ar.id')
                ->where('ar.state',1)
                ->where('ar.slug','=',$articleSlug)
                ->where('ct.id',$cateId)
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC');

        return $query->first();
    }

    public function getArticleByTag($tagSlug,$limit){
        $query = \DB::table('articles as ar')->select("ar.*","t.name as tag_name","t.slug as tag_slug","ct.name as ct_name","ct.slug as ct_slug"
                ,\DB::raw("COUNT(cm.article_id) AS comment_count"),
                \DB::raw("( SELECT COUNT( mr.obj_id ) FROM media_relation AS mr WHERE mr.obj_id = ar.id AND mr.obj_type = 'article' ) AS media_count")
                )
                ->join('article_category as ac','ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->join('article_tag as art','art.article_id','=','ar.id')
                ->join('tags as t','t.id','=','art.tag_id')
                ->leftjoin('comments as cm','cm.article_id','=','ar.id')
                ->where('t.slug',$tagSlug)
                ->where('ar.state',1)
                ->where('ct.state',1)
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC');
                

        return $query->paginate($limit);
    }

    public function getArticleByCondition($filter, $limit = 10){
        $query = \DB::table('articles as ar')->select("ar.*","ct.name as ct_name","ct.slug as ct_slug"
                ,\DB::raw("COUNT(cm.article_id) AS comment_count"),
                \DB::raw("( SELECT COUNT( mr.obj_id ) FROM media_relation AS mr WHERE mr.obj_id = ar.id AND mr.obj_type = 'article' ) AS media_count")
                )
                ->join('article_category as ac','ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->leftjoin('comments as cm','cm.article_id','=','ar.id')
                ->where('ar.state',1)
                ->where('ct.state',1)
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC');
        
            if(isset($filter['keyword']) && !empty($filter['keyword'])){
                $query->where('ar.title','like','%'.$filter['keyword'].'%');
            }
            if(isset($filter['article_type']) && !empty($filter['article_type'])){
                $query->where('ar.article_type','=',$filter['article_type']);

            }

        return $query->paginate($limit);
    }

    public function getNextOrPrevArticle($type,$cateId,$articleId){
        if($type == 'next'){
            $typeCompare = '>';
            $orderByType = 'ASC';
        }
        else{
            $typeCompare = '<';
            $orderByType = 'DESC';
        }
        $query = $this->select('articles.*','ct.name as ct_name','ct.slug as ct_slug')
                ->join('article_category as ac','ac.article_id','=','articles.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->where('articles.id',$typeCompare,$articleId)->where('articles.state',1)->where('ct.id',$cateId)->orderBy('articles.id',$orderByType);
        return $query->first();
    }

    public function getRelatedArticle(array $tagSlug, $articleId, $limit){
        $query = \DB::table('articles as ar')->select("ar.*","ct.name as ct_name","ct.slug as ct_slug")
                ->join('article_category as ac','ac.article_id','=','ar.id')
                ->join('categories as ct','ct.id','=','ac.category_id')
                ->join('article_tag as art','art.article_id','=','ar.id')
                
                ->where('ar.state',1)
                ->where('ar.id','<>',$articleId)
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC')->limit($limit);
        if(!empty($tagSlug)){
            $query->join('tags as t','t.id','=','art.tag_id')
                ->whereIn('t.slug',$tagSlug);
        }
        return $query->get();
    }

    public function createArticle(array $data){
        return $this->create($data);
    }

    

    public function adminGetAllArticle($limit, $filter = null){
        $query = \DB::table('articles as ar')->select("ar.*",'m.filename as image_path'
                ,\DB::raw("COUNT(cm.article_id) AS comment_count"))
                ->leftjoin('media as m','m.media_id','=','ar.picture')
                ->leftjoin('comments as cm','cm.article_id','=','ar.id')
                ->groupBy('ar.id')
                ->orderBy('ar.created_at','DESC');
        if (isset($filter['category_id']) && !empty($filter['category_id'])) {
            $query->join('article_category as ac','ar.id','=','ac.article_id')
            ->where('ac.category_id', '=', $filter['category_id']);
        }

        if(isset($filter['keyword']) && !empty($filter['keyword'])){
            $query->where('ar.title','like','%'.$filter['keyword'].'%');
        }
        return $query->paginate($limit);
    }

    public function adminGetArticleDetail($articleId){
        $query = $this->select('articles.*','m.filename as image_path')
                ->leftjoin('media as m', 'm.media_id','=','articles.picture')
                ->where('articles.id',$articleId);
        return $query->first();
    }

    public function getArticlebyType($article_type,$limit){

    }
}