<?php

namespace App\Services;

use App\Interfaces\PermissionInterface;

class Permission extends \App\Models\Permission implements  PermissionInterface {

	public function getAll($limit){
        $query = $this->select('*')->orderBy('created_at', 'DESC');
        return $query->paginate($limit);
    }

    public function createPermission(array $data){
        return $this->create($data);
    }
}