<?php

namespace App\Services;

use App\Interfaces\UserInterface;

class User extends \App\Models\User implements  UserInterface {

    public function getAll($limit){
        $query = $this->select('users.*','up.birthday', 'up.gender', 'up.phone','r.name as role_name','r.slug as role_slug')
            ->leftjoin('user_profile as up','up.user_id','=','users.id')
            ->leftjoin('users_roles as ur','ur.user_id','users.id')
            ->leftjoin('roles as r','r.id','=','ur.role_id')
            ->orderBy('users.created_at','DESC');

        return $query->paginate($limit);
    }
    public function createUser(array $data){
        return $this->create($data);
    }
}