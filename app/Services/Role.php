<?php

namespace App\Services;

use App\Interfaces\RoleInterface;

class Role extends \App\Models\Role implements  RoleInterface {

	public function getAll($limit){
        $query = $this->select('*')->orderBy('created_at', 'DESC');
        return $query->paginate($limit);
    }

    public function createRole(array $data){
        return $this->create($data);
    }
}