<?php

namespace App\Services;

use App\Interfaces\CommentInterface;

class Comment extends \App\Models\Comment implements CommentInterface
{
    public function createComment(array $data)
	{
		return $this->create($data);
	}

	public function adminGetAllComment($limit){
		$query = $this->select('comments.*','ar.title','ar.slug','u.name')
				->join('articles as ar','ar.id','=','comments.article_id')
				->leftjoin('users as u', 'u.id','=','comments.user_id_edited')
				->orderBy('comments.created_at');
		// dd($query->toSql());
		return $query->paginate($limit);
	}

}
