<?php

namespace App\Http\Middleware;

use Closure;

class RolePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role, $permission = null)
    {
        $roles = is_array($role) ? $role : explode('|', $role);
        $accept = false;
        foreach($roles as $role){
            if ($request->user()->hasRole($role)) {
                $accept = true;
            }
        }

        if(!$accept){
            if($permission !== null){
                if(!$request->user()->can($permission)){
                    return response(view('admin.errors.403'));
                }
                else{
                    return $next($request);
                }
            }
            else{
                return response(view('admin.errors.403'));
            }
        }
        return $next($request);
    }
}
