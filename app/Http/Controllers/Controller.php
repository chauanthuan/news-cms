<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $status  = -1;
    protected $message = "";
    protected $data = array();
    
    protected function apiResponse($data , $status = 1 , $message = '' ){
        $response = array();
        $response['status']  = $status;
        $response['message'] = $message;
        $response['data']    = empty($data) ? (object)$data : $data ;
        return response()->json($response);
      }
      
}
