<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CommentInterface;
use Validator;


class CommentController extends Controller
{
    /*comment status field
	0:pedding;
	1:active
	*/
    private $comment;

    public function __construct(CommentInterface $commentInterface){
        $this->comment = $commentInterface;
    }
    public function postComment(Request $request){
        $input = $request->all();
        $validator = Validator::make($input, [
            // Do not allow any characters
            'username' => 'required',
            'content' => 'required',
            'email' => 'required|email',
        ]);

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors())->withInput();
        }

        $input['state'] = 0;
        $input['comment_parent_id'] = $request->get('comment_parent_id',0);
    	if($comment = $this->comment->createComment($input)){
    		return redirect()->back();
    	}
    	return redirect()->back();
    }

}
