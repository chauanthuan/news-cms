<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CategoryInterface;
use App\Interfaces\ArticleInterface;

class CategoryController extends Controller
{
    private $category;
    private $article;
    public function __construct(CategoryInterface $categoryInterface, ArticleInterface $articleInterface){
        $this->category = $categoryInterface;
        $this->article = $articleInterface;
    }

    public function index(Request $request,$categorySlug){
        $category = $this->category->getDetail($categorySlug);
       
        if(!isset($category) || empty($category)){
            return view('web.errors.404');
        }
        $listArticle = $this->article->getListArticleByCate($category->id, 10);

        return view('web.category', compact('listArticle'));
    }
}
