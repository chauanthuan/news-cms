<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CategoryInterface;
use App\Interfaces\ArticleInterface;
use App\Interfaces\TagInterface;
use App\Interfaces\CommentInterface;
use App\Models\Article;
use App\Models\Comment;
use App\Models\Media;
use App\Models\MediaRelation;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class ArticleController extends Controller
{
    private $article;
    private $category;
    private $tag;
    private $comment;

    public function __construct(ArticleInterface $articleInterface, CategoryInterface $categoryInterface, TagInterface $tagInterface, CommentInterface $commentInterface){
        $this->article = $articleInterface;
        $this->category = $categoryInterface;
        $this->tag = $tagInterface;
        $this->comment = $commentInterface;
    }

    public function getDetail(Request $request, $categorySlug, $articleSlug){
        $category = $this->category->getDetail($categorySlug);
        if(!isset($category) || empty($category)){
            return view('web.errors.404');
        }

        $article = $this->article->getDetail($category->id, $articleSlug);
      
        if(!isset($article) || empty($article)){
            return view('web.errors.404');
        }
        $tags = Article::find($article->id)->tags;
        $list_cate = Article::find($article->id)->categories;
     
        //get next and prev post
        $next_article = $this->article->getNextOrPrevArticle('next',$category->id, $article->id);
        $prev_article = $this->article->getNextOrPrevArticle('prev',$category->id, $article->id);
       
        $arrTags = [];
        foreach($tags as $tag){
            $arrTags[] = $tag->slug;
        }

        $related_article = $this->article->getRelatedArticle($arrTags,$article->id, 3);

        //get comment
        $comments = Article::find($article->id)->comments()->where('comments.state',1)->paginate(10);

        $listGallery = MediaRelation::where('obj_id',$article->id)->where('obj_type','article')->orderBy('weight','DESC')->get();

        $blogKey = 'article_' . $article->id;
        // Check if blog session key exists
        // If not, update view_count and create session key
        if (!Session::has($blogKey)) {
            $this->article->where('id', $article->id)->increment('view_count');
            Session::put($blogKey, 1);
        }

        return view('web.article', compact('article','tags','list_cate','next_article','prev_article','related_article','comments','listGallery'));
    }

    public function getArtileByTag(Request $request, $tagSlug){
        $tag = $this->tag->getDetail($tagSlug);
        
        if(!isset($tag) || empty($tag)){
            return view('web.errors.404');
        }
        $ListArticleTag = $this->article->getArticleByTag($tagSlug,10);
        return view('web.tag', compact('tag','ListArticleTag'));
    }
}
