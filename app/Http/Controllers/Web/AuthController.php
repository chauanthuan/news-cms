<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\UserInterface;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use App\Mail\WelcomeMail;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    private $user;
    public function __construct(UserInterface $userInterface){
        $this->user = $userInterface;
    }
    public function anyLogin(Request $request){
        if(\Auth::check()){
            return redirect()->route('web.index');
        }

        $errors = array();
        if($request->isMethod('post')){
            $validator = \Validator::make($request->all(), [
                'username'  => 'required',
                'password'  =>  'min:6|max:32'
            ]);
            if(count($validator->errors())){
                $errors = $validator->errors();
            } else {
                $input = $request->all();
                if($request->get('username')){
                    $credentials = ['username'=>$request->get('username'), 'password'=>$request->get('password'),'state'=>1];
                    if (\Auth::attempt($credentials, $request->get('remember', 0))) {
                        return redirect()->route('web.index');
                    } else {
                       $validator->errors()->add('field', 'Tài khoản hoặc mật khẩu không chính xác');
                       $errors = $validator->errors();
                    }
                }
            }
        }
        return view('web.auth.login' , compact('errors'));
    }

    public function getLogout(Request $request){
        \Auth::logout();
        return redirect(route('web.index'));
    }

    public function anyChangePass(Request $request) {
        if($request->isMethod('post')){ 
            //user submit form tạo mật khẩu mới
            //validate mat khau và confirm mật khẩu
            $validator = \Validator::make($request->all(), [
                'password' => 'required|confirmed|min:6',
            ]);
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
            //neu dung
            $user = \Auth::user();
            $user->update(['password'=>$request->get('password')]);
            //return về trang logn in
            \Auth::logout();
            Session::flash('status', 'Xin chúc mừng, đổi mật khẩu thành công. Đăng nhập lại nhé.');
            return redirect()->route('web.auth.login');

        }
        return view('web.auth.change-pass');
    }
    public function anyProfile(Request $request){
        $errors = array();
        $user = \Auth::user();
        $profile = $user->profile;
        if($request->isMethod('post')){
            $validator = \Validator::make($request->all(), [
                'name'  => 'required',
            ]);
            if(count($validator->errors())){
                $errors = $validator->errors();
            } else {
                $data = $request->all();
                // $profile = $this->profile->find($id);
                $data['user_id_edited'] = \Auth::user()->id;
                \DB::begintransaction();
                if(!$profile){
                    $user->update(['name'=>$data['name']]);
                    $affect = $this->profile->create($data);
                    if($affect){
                        \DB::commit();
                        return redirect()->back()->with('status','Thông tin tài khoản "'.$user->email.'" đã được cập nhật thành công.');
                    }
                    else{
                        \DB::rollBack();
                        return redirect()->back()->with('status','Cập nhật thông tin tài khoản "'.$user->email.'" không thành thành công.');
                    }
                    
                }
                else{
                    $user->update(['name'=>$data['name']]);
                    $affect = $profile->fill($data)->save();
                    if($affect){
                        \DB::commit();
                        return redirect()->back()->with('status','Thông tin tài khoản "'.$user->email.'" đã được cập nhật thành công.');
                    }
                    else{
                        \DB::rollBack();
                        return redirect()->back()->with('status','Cập nhật thông tin tài khoản "'.$user->email.'" không thành thành công.');
                    }
                }
                
            }
        }
        return view('web.auth.profile' , compact('errors','profile','user'));
    }

    /**
     * Perform the registration.
     *
     * @param  Request   $request
     * @param  AppMailer $mailer
     * @return \Redirect
     */
    public function anyRegister(Request $request)
    {
        if(\Auth::check()){
            return redirect()->route('web.index');
        }
        if($request->isMethod('post')){
            $validator = \Validator::make($request->all(), [
                'username' => 'required|unique:users',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6'
            ]);
            if ($validator->fails())
            {
                return redirect()->route('web.auth.register')->withErrors($validator->errors())->withInput();
            }
            
            $input = $request->all();
            $input['token'] = str_random(100);
            $user = $this->user::create($input);
           
            // $mailer->sendEmailConfirmationTo($user);
            // Mail::to($input['email'])->send(new WelcomeMail($user));

            Mail::send('web.mail.confirm', array('name'=>$user->name,'email'=>$user->email,'token'=>$user->token,'website'=>env('WEBSITE'),'weblink'=>env('WEBLINK')), function($message) use ($user){
                $message->to($user->email, 'Visitor')->subject('Đăng ký tài khoản tại viet24h.vn!');
            });
            Session::flash('status', 'Vui lòng kiểm tra email. Chúng tôi đã gửi đến email đăng ký của bạn 1 liên kết để kích hoạt tài khoản.');
            return redirect()->route('web.auth.login');
        }
        return view('web.auth.register');
    }

    /**
     * Confirm a user's email address.
     *
     * @param  string $token
     * @return mixed
     */
    public function anyRegisterConfirm(Request $request, $token)
    {
        $user = $this->user->where('token','=',$token)->first();
        if(!$user){
            Session::flash('status', 'Không tìm thấy dữ liệu người dùng đăng kí phù hợp. Vui lòng thử lại.');
            return redirect()->route('web.auth.login');
        }

        if($user->state == 1){
            Session::flash('status', 'Tài khoản đã được kích hoạt từ trước. Vui lòng đăng nhập');
            return redirect()->route('web.auth.login');
        }
        $user->update(['state'=>1]);
        Session::flash('status', 'Xin chúc mừng, Tài khoản của bạn đã được kích hoạt thành công. Đăng nhập ngay nhé.');
        return redirect()->route('web.auth.login');
    }

    public function anyResetPass(Request $request)
    {
        //check và gửi email xác thực là người dùng muốn reset password
        $validateUser = false;
        if(\Auth::check()){
            return redirect()->route('web.index');
        }
        if($request->isMethod('post')){
            $validator = \Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email',
            ]);
            if ($validator->fails())
            {
                return redirect()->route('web.auth.resetpass')->withErrors($validator->errors())->withInput();
            }
            $data = $request->all();
            $user = $this->user->where('email',$data['email'])->first();
            $token = str_random(100);
            if($user){
                $user->update(['token'=>$token]);
            }
            //gửi link reset password về email + token, nếu kiểm tra đúng email + token
            Mail::send('web.mail.resetpass', array('name'=>$data['name'],'email'=>$user->email,'token'=>$token,'website'=>env('WEBSITE'),'weblink'=>env('WEBLINK')), function($message) use ($user){
                $message->to($user->email, 'Visitor')->subject('Lấy lại mật khẩu tài khoản tại viet24h.vn!');
            });
            Session::flash('status', 'Một liên kết đặt lại mật khẩu đã được gửi đến email của bạn, vui lòng kiểm tra và làm theo hướng dẫn.');
        }
        return view('web.auth.reset-pass', compact('validateUser'));
    }

    public function anyResetPassConfirm(Request $request, $token){
        //kiêm tra user có token thì chuyển qua trang tạo mật khẩu mới,
        $user = $this->user->where('token','=',$token)->first();
        if(!$user){
            Session::flash('status', 'Không tìm thấy dữ liệu người dùng đăng kí phù hợp. Vui lòng thử lại.');
            return redirect()->route('web.auth.resetpass');
        }
        $validateUser = true;
        if($request->isMethod('post')){ 
            //user submit form tạo mật khẩu mới
            //validate mat khau và confirm mật khẩu
            $validator = \Validator::make($request->all(), [
                'password' => 'required|confirmed|min:6',
            ]);
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
            //neu dung
            $user->update(['password'=>$request->get('password')]);
            //return về trang logn in
            
            Session::flash('status', 'Xin chúc mừng, Tài khoản của bạn đã được kích hoạt thành công. Đăng nhập ngay nhé.');
            return redirect()->route('web.auth.login');

        }
        
        return view('web.auth.reset-pass',compact('validateUser','token'));
    }
}
