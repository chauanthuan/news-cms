<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CategoryInterface;
use App\Interfaces\ArticleInterface;
use App\Models\Article;
class IndexController extends Controller
{
    private $category;
    private $article;

    public function __construct(CategoryInterface $categoryInterface,ArticleInterface $articleInterface  ){
        $this->category = $categoryInterface;
        $this->article = $articleInterface;
    }

    public function index(Request $request){
        //latest post
        $latest_article = $this->article->getLatestPost(5);

        //lấy tất cả bài viết của các category dc phép show ở homepage(showHome = 1)
        $listCateHome = $this->category->where('showHome',1)->where('state',1)->orderBy('order','DESC')->get();
        
        $listArticleByCate = [];
        foreach ($listCateHome as $key => $value) {
            $listArticleByCate[$key]['category']  = $value;
            $arrArticle = $this->article->getListArticleByCate($value['id'], 5);
            $listArticleByCate[$key]['listArticle'] = $arrArticle; 
        }

        //tất cả bài viết với type
        $listArticleGallery = $this->article->getArticleByCondition(['article_type'=>Article::TYPE_GALLERY],5);
        $listArticleVideo = $this->article->getArticleByCondition(['article_type'=>Article::TYPE_VIDEO],4);

        return view('web.index',compact('latest_article','listArticleByCate','listArticleGallery','listArticleVideo'));
    }

    public function seachData(Request $request){
        $keyword = $request->get('s');
        if(empty($keyword)){
            return redirect('/');
        }
        $listArticle = $this->article->getArticleByCondition(['keyword'=>$keyword],10);
        return view('web.search', compact('listArticle','keyword'));
    }

    public function galleries(Request $request){
        $listArticleGallery = $this->article->getArticleByCondition(['article_type'=>Article::TYPE_GALLERY],10);
        return view('web.gallery', compact('listArticleGallery'));
    }

    public function videos(Request $request){
        $listArticleVideo = $this->article->getArticleByCondition(['article_type'=>Article::TYPE_VIDEO],10);
        return view('web.video', compact('listArticleVideo'));
    }
}
