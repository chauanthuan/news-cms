<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CategoryInterface;
use App\Interfaces\ArticleInterface;

class TagController extends Controller
{
    private $article;
    private $category;

    public function __construct(ArticleInterface $articleInterface, CategoryInterface $categoryInterface){
        $this->category = $categoryInterface;
        $this->article = $articleInterface;
    }

    public function index(Request $request, $tagSlug){
        $tagList = $this->article->getArticleByTag($tagSlug);
    }
}
