<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CategoryInterface;
use App\Interfaces\ArticleCategoryInterface;
use App\Interfaces\ArticleInterface;
use App\Models\Category;
use Validator;

class CategoryController extends Controller
{
    private $category;
    private $article_category;
    private $article;

    public function __construct(CategoryInterface $categoryInterface, ArticleInterface $articleInterface, ArticleCategoryInterface $articleCategoryInterface){
        $this->category = $categoryInterface;
        $this->article = $articleInterface;
        $this->article_category = $articleCategoryInterface;
    }

    public function getIndex(Request $request){
        $listCate = $this->category->adminGetAllCategory(10);
        // dd($listCate);
        return view('admin.category.index', compact('listCate'));
    }

    public function anyCreate(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $validator = Validator::make($data,[
                'name'=>'required',
            ]);
      
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }

            $data['state'] = $request->get('state', null);
            $data['showHome'] = $request->get('showHome', null);
            $data['showMenu'] = $request->get('showMenu', null);
            
            $category = $this->category->createCategory($data);

            return redirect(route('cms.category'))->with('status','Danh mục "'. $data['name'] .'" đã được tạo thành công.');
        }

        return view('admin.category.form');
    }

    public function anyUpdate(Request $request, $categoryId){
        $category = Category::where('id', $categoryId)->first();
        if(!$category){
            abort(500);
        }
        if($request->isMethod('post')){
            $data = $request->all();
            $validator = Validator::make($data,[
                'name'=>'required',
            ]);
  
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
            $data['state'] = $request->get('state', null);
            $data['showHome'] = $request->get('showHome', null);
            $data['showMenu'] = $request->get('showMenu', null);
            $category->fill($data)->save();

            return redirect()->back()->with('status','Danh mục "'. $data['name'] .'" đã được cập nhật thành công.');
        }
        return view('admin.category.form', compact('category'));
    }

    public function anyDelete(Request $request){
        if (!$request->ajax()) {
            abort(403);
        }
        $cateId = $request->get('id',0);
        
        \DB::beginTransaction();
        try{
            //delete all row in article_category
            $this->article_category->where('category_id', $cateId)->delete();
            //delete category
            $this->category->where('id', $cateId)->delete();
            \DB::commit();
            return response()->json(['stt'=>1,'msg'=>'Xóa danh mục thành công']);
        }
        catch (Exception $e){
            \DB::rollBack();
            Log::info($e);
            return response()->json(['stt'=>0,'msg'=>'Xóa danh mục không thành công']);
        }
        
    }
}
