<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Helpers\Helpers;
use App\Models\Configure;
use App\Http\Controllers\Controller;

class ConfigureController extends Controller{
  protected $configure;
  protected $pageSize;

  public function __construct(Configure $configure) {
    $this->configure = $configure;
    $this->pageSize  = 10;
    $this->pTitle    = 'Quản lý cài đặt';
    // $this->route     = 'CONFIGURE';
  }


  public function anyUpdate(Request $request) {    
    if ($request->isMethod('post')) {
      $input = $request->all();   
      $validator = \Validator::make($request->all(), [
        'configure' => 'required',                            
      ]);
      if(count($validator->errors())){
        return \Redirect::back()->withInput()->withErrors($validator);
      } else {
        $input      = $input['configure'];
        $configures = $this->configure->getAll();
        foreach($configures as $key => $configure) {
          $default = $configure->type == 'text' ? '' : NULL;
          $value   = Helpers::getVal($input, $configure->key, $default);
          $this->configure->where('key' ,$configure->key)->update(['value' => $value]);    
        }
      }
      return redirect(route('cms.configure'))->with('status','Cài đặt được cập nhật thành công.');
    }

    $configures = $this->configure->getAll();
    $this->compact['headers']['title']      = $this->pTitle;
    $this->compact['headers']['breadcrumb'] = [
                                                ['url' => '' , 'title' => __('Tất cả cài đặt')],
                                              ];
    return view('admin.configure.form', compact ('configures'));
  }  


}
