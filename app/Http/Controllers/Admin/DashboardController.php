<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\UserInterface;

class DashboardController extends Controller
{
    private $user;
    public function __construct(UserInterface $userInterface){
        $this->user = $userInterface;
    }
    public function getIndex(Request $request){
        $listUser = $this->user->getAll(10);
        // dd($listUser);
        return view('admin.dashboard.index',compact('listUser'));
    }
}
