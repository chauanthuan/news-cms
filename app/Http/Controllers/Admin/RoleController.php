<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\RoleInterface;
use App\Interfaces\PermissionInterface;
use App\Models\RolePermission;
use Validator;

class RoleController extends Controller
{
    private $role;
    private $permission;
    public function __construct(RoleInterface $roleInterface, PermissionInterface $permissionInterface){
        $this->role = $roleInterface;
        $this->permission = $permissionInterface;
    }

    public function getIndex(Request $request){
        $listRole = $this->role->getAll(10);
        $user = $request->user();
        // dd($user);
        // dd($user->hasRole('editor'));

        //$user->givePermissionsTo('update-article');
        //dd($user->can('delete-account'));
        return view('admin.role.index', compact('listRole'));
    }

    public function anyCreate(Request $request){
        $listPermission = $this->permission->getAll(1000);
        if($request->isMethod('post')){
            $data = $request->all();
            $validator = Validator::make($data,[
                'name'=>'required',
                'slug'=>'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
            $role = $this->role->createRole($data);
            return redirect(route('cms.role'))->with('status','Role "'. $data['name'] .'" đã được tạo thành công.');
        }
    
        return view('admin.role.form', compact('listPermission'));
    }

    public function anyUpdate(Request $request, $roleId){
        $listPermission = $this->permission->getAll(1000);
        $list_permission_arr = [];
        
        $role = $this->role->where('id', $roleId)->first();
        if(!$role){
            return redirect()->back()->with('status','Role không tồn tại.');
        }
        $listPermissionByRole = $role->permissions;
        foreach($listPermissionByRole as $item){
            $list_permission_arr[] = $item->id;
        }
        
        if($request->isMethod('post')){
            $data = $request->all();
            $validator = Validator::make($data,[
                'name'=>'required',
                'slug'=>'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
            
            $role->fill($data)->save();

            $this->saveRolePermission($data['permission_id'], $role->id);

            return redirect(route('cms.role'))->with('status','Role "'. $data['name'] .'" đã được cập nhật thành công.');
        }
        return view('admin.role.form', compact('role','list_permission_arr','listPermission'));
    }

    //save tag with array data
    function saveRolePermission($data, $role_id){
        if(!empty($data) && is_array($data)){
            RolePermission::where('role_id', $role_id)->delete();
            foreach ($data as $key=>$val) {
                if ($val === null || empty($val)){
                    unset($data[$key]);
                }
                else{
                    RolePermission::create([
                        'permission_id'   =>  $val,
                        'role_id'  =>  $role_id
                    ]);
                }   
            }
        }
    }
    public function anyDelete(Request $request){
        return response()->json(['stt'=>0]);
    }
}
