<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\UserInterface;
use App\Interfaces\UserProfileInterface;
use App\Interfaces\RoleInterface;
use App\Interfaces\PermissionInterface;
use Validator;

class AccountController extends Controller
{
    private $user;
    private $profile;
    private $role;
    public function __construct(UserInterface $userInteface, UserProfileInterface $userprofileInterface, RoleInterface $roleInterface, PermissionInterface $permissionInterface){
        $this->user = $userInteface;
        $this->profile = $userprofileInterface;
        $this->role = $roleInterface;
        $this->permission = $permissionInterface;
    }

    public function getIndex(Request $request){
        
        // $user = $request->user();
        // dd($user);
        // dd($user->hasRole('developer')); //will return true, if user has role
        // dd($user->givePermissionsTo('create-tasks')); // will return permission, if not null
        // dd($user->can('create-tasks')); // will return true, if user has permission
        $listAccount = $this->user->getAll(10);
        foreach($listAccount as $ac){
            // dd($ac);
            // dd($ac->roles);
        }
        return view('admin.account.index', compact('listAccount'));
    }

    public function anyCreate(Request $request){
        $listRole = $this->role->get();
        $listPermission = $this->permission->get();
        if($request->isMethod('post')){
            $data = $request->all();
            $validator = Validator::make($data,[
                'name'=>'required',
                'username'=>'required|unique:users',
                'password'=>'required|min:6',
                'email'=> 'required|email|unique:users'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }

            $account = $this->user->createUser($data);

            $this->saveUpdateUserRole($account->id, $data['role']);

            return redirect(route('cms.account'))->with('status','Tài khoản "'. $data['name'] .'" đã được tạo thành công.');
        }
        return view('admin.account.form', compact('listRole','listPermission'));
    }

    public function anyUpdate(Request $request, $userId){
        $account = $this->user->where('id',$userId)->first();

        if(empty($account)){
            return redirect()->back()->with('status','Tài khoản không tồn tại.');
        }
        $listRole = $this->role->get();
        $listPermission = $this->permission->get();


        //get list permission by role - role get by account
        $list_permission_arr_by_role = [];
        $listPermissionByRole = [];
        $role = $account->roles->first();
        if($role){
            $listPermissionByRole = $role->permissions;
            foreach($listPermissionByRole as $item){
                $list_permission_arr_by_role[] = $item->id;
            }
        }
        
        //get list permission by account
        $listPermissionByAccount = $account->permissions;
        $list_permission_arr = [];
        foreach($listPermissionByAccount as $item){
            $list_permission_arr[] = $item->id;
        }

        $profile = $this->profile->where('user_id',$account->id)->first();
        if($request->isMethod('post')){
            $data = $request->all();
            
            $validator = Validator::make($data,[
                'name'=>'required',
                'email'=> 'required',
            ]);
  
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
            unset($data['username']);

            if($data['password'] == '')
                unset($data['password']);
            // else
            //     $data['password'] = $data['password'];
            
            $this->saveUpdateUserRole($account->id, $data['role']);
            if(isset($data['permission_id'])){
                $this->saveUpdateUserPermission($account->id, $data['permission_id']);
            }
            else{
                $account->permissions()->detach();
            }
            
            $data['state'] = isset($data['state']) ? (int)$data['state'] : '';
            $data['is_admin'] = isset($data['is_admin']) ? (int)$data['is_admin'] : '';
            $account->fill($data)->save();

            return redirect()->back()->with('status','Tài khoản "'.$account->username.'" đã được cập nhật thành công.');
        }
        return view('admin.account.form', compact('account','profile','listRole','listPermission','list_permission_arr','listPermissionByRole','list_permission_arr_by_role'));
    }

    public function anyDelete(Request $request){
        if (!$request->ajax()) {
            abort(403);
        }
        $userId = $request->get('id', 0);
        $affect = $this->user->where('id',$userId)->delete();
        if($affect)
            return response()->json(['stt'=>1,'msg'=>'Xóa tài khoản thành công']);
        return response()->json(['stt'=>0,'msg'=>'Xóa tài khoản không thành công']);
    }

    public function anyProfile(Request $request){
        $id = $request->get('profileid', 0);
        $data = $request->all();
        // $data = [];
        // $data['birthday'] = $request->get('birthday', null);
        // $data['address'] = $request->get('address', null);
        // $data['gender'] = $request->get('gender',null);
        // $data['user_id'] = $request->get('user_id',null);

        $profile = $this->profile->find($id);
        $data['user_id_edited'] = \Auth::user()->id;

        if(!$profile){
            $affect = $this->profile->create($data);
            if($affect){
                return response()->json(['stt'=>1,'msg'=>'Tạo mới Thành công']);
            }
            return response()->json(['stt'=>0,'msg'=>'Tạo mới Thất bại']);
        }
        else{
            $affect = $profile->fill($data)->save();
            if($affect){
                return response()->json(['stt'=>1,'msg'=>'Cập nhật Thành công']);
            }
            return response()->json(['stt'=>0,'msg'=>'Cập nhật Thất bại']);
        }
    }

    public function uploadAvatar(Request $request) {
        $media_type = $request->get('media_type' , "image");
        if($media_type == 'image') {
    
          $uploadDir    = MediaHelper::rootUploadDir('avatars');
          $uploadFolder = MediaHelper::uploadDir();
          $originalDir  = $uploadDir.'/original/';
    
          $file = $request->file('file');
          if(!empty($file)) {
            $upload = MediaUpload::factory($originalDir);
            $upload->file($_FILES['file']);
            $results = $upload->upload();
            if($upload->resizeImage($uploadDir.'/'.$uploadFolder)) {
              $item = [
                'media_type'        => strtoupper($media_type),
                'filename'          => $uploadFolder.'/'.$results['filename'],
                'state'             => Media::STATE_ACTIVE
              ];
              $this->media->create($item);
              $this->status = 1;
            }
    
          }
        }
        return $this->apiResponse($this->data , $this->status , $this->message);
    }

    public function saveUpdateUserRole($userId,  $roleId){
        // $role = $this->role()->where()
        
        $account = $this->user->where('id',$userId)->first();
        $account->roles()->detach();
        $account->roles()->attach($roleId);
            return true;
        return false;
    }

    public function saveUpdateUserPermission($userId, $permissionId){
        $account = $this->user->where('id',$userId)->first();
        $account->permissions()->detach();
        foreach($permissionId as $permission){
            $account->permissions()->attach($permission);
        }
        return true;
    }
    
}
