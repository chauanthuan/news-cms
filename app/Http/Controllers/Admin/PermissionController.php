<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\PermissionInterface;
use Validator;

class PermissionController extends Controller
{
    private $permission;
    public function __construct(PermissionInterface $permissionInterface){
        $this->permission = $permissionInterface;
    }

    public function getIndex(Request $request){
        $listPermission = $this->permission->getAll(10);
        return view('admin.permission.index', compact('listPermission'));
    }

    public function anyCreate(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $validator = Validator::make($data,[
                'name'=>'required',
                'slug'=>'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }

            $permission = $this->permission->createPermission($data);
            return redirect(route('cms.permission'))->with('status','Permission "'. $data['name'] .'" đã được tạo thành công.');
        }
        return view('admin.permission.form');
    }

    public function anyUpdate(Request $request, $permissionId){
        $permission = $this->permission->where('id', $permissionId)->first();
        if(!$permission){
            return redirect()->back()->with('status','permission không tồn tại.');
        }
        if($request->isMethod('post')){
            $data = $request->all();
            $validator = Validator::make($data,[
                'name'=>'required',
                'slug'=>'required'
            ]);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
            $permission->fill($data)->save();
            
            return redirect(route('cms.permission'))->with('status','Permission "'. $data['name'] .'" đã được cập nhật thành công.');
        }
        return view('admin.permission.form', compact('permission'));
    }
}
