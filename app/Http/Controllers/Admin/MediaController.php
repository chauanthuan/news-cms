<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

use App\Helpers\Helpers;
use App\Helpers\Media\MediaUpload;
use App\Helpers\Media\MediaHelper;
use App\Helpers\Media\MediaCutImage;
use App\Models\Media;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class MediaController extends \App\Http\Controllers\Controller {

	// Declare instance model
	protected $media;

	function __construct(Media $media) {
		//$this->middleware('auth');
		$this->media = $media;
	}

  public function getAll(Request $request){
    $items  = $request->get('items' , 12);
    $page   = $request->get('page', 1);
    $media_type = $request->get('media_type' , "image");
    $keyword = $request->get('keyword');
    $filter = ['keyword' => $keyword , 'media_type' => $media_type];
    $medias = $this->media->getAllMedia($items , $filter);
    $total  = $medias->total();

    $maxPage = $total % $items == 0 ? ($total / $items) : intval($total / $items) + 1;
    return view('admin.media.index',compact('medias','page','items' , 'media_type' , 'maxPage'));
  }

  public function getIndex(Request $request){

    $items  = $request->get('items' , 12);
    $page   = $request->get('page', 1);
    $media_type = $request->get('media_type' , "image");
    $keyword = $request->get('keyword');

    $filter = ['keyword' => $keyword , 'media_type' => $media_type];
    $medias = $this->media->getAllMedia($items , $filter);
    $total  = $medias->total();

    $maxPage = $total % $items == 0 ? ($total / $items) : intval($total / $items) + 1;
    return view('admin.media.index',compact('medias','page','items' , 'media_type' , 'maxPage'));
  }

	public function anyCreate(Request $request) {
    $media_type = $request->get('media_type' , "image");
    $userId = \Auth::user()->id;
    if($media_type == 'image') {

      $uploadDir    = MediaHelper::rootUploadDir('storage');
  
      $uploadFolder = MediaHelper::uploadDir();
      $originalDir  = $uploadDir.'/original/';

      $file = $request->file('file');
      if(!empty($file)) {
        $upload = MediaUpload::factory($originalDir);
        $upload->file($_FILES['file']);
        $results = $upload->upload();
        if($upload->resizeImage($uploadDir.'/'.$uploadFolder)) {
          $item = [
            'media_type'        => strtoupper($media_type),
            'filename'          => $uploadFolder.'/'.$results['filename'],
            'state'             => Media::STATE_ACTIVE,
            'create_by'         => $userId
          ];
          $this->media->create($item);
          $this->status = 1;
        }

      }
    }
		return $this->apiResponse($this->data , $this->status , $this->message);
	}


  public function getDestroy(Request $request) {
    $media_id = $request->get('media_id');
    $affect = $this->media->destroy($media_id);
    if($affect)
      return response()->json(['stt'=>1,'msg'=>'Delete File Successfully']);
    return response()->json(['stt'=>0,'msg'=>'Delete File Failed']);
  }


}
