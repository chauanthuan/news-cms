<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CommentInterface;

class CommentController extends Controller
{
    private $comment;
    public function __construct(CommentInterface $commentInterface){
        $this->comment = $commentInterface;
    }

    public function getIndex(Request $request){
        $listComment = $this->comment->adminGetAllComment(10);
        // dd($listComments);
        return view('admin.comment.index', compact('listComment'));
    }

    public function anyUpdate(Request $request, $commentId){
        if (!$request->ajax()) {
            abort(403);
        }
        $comment = $this->comment->where('comment_id', $commentId)->first();
        $adminId = $request->get('user_id_edited');
        if(!$comment){
            abort(500);
        }
        $affect = $comment->update(['state'=>1,'user_id_edited'=>$adminId]);
        if($affect)
            return response()->json(['stt'=>1,'msg'=>'Phê duyệt bình luận thành công']);
        return response()->json(['stt'=>0,'msg'=>'Phê duyệt bình luận không thành công']);
    }
    
    public function anyDelete(Request $request){
        if (!$request->ajax()) {
            abort(403);
        }
        $commentId = $request->get('id',0);
        $affect = $this->comment->where('comment_id', $commentId)->delete();
        if($affect)
            return response()->json(['stt'=>1,'msg'=>'Xóa bình luận thành công']);
        return response()->json(['stt'=>0,'msg'=>'Xóa bình luận không thành công']);
    }
}
