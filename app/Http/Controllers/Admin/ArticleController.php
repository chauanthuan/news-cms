<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Interfaces\CategoryInterface;
use App\Interfaces\ArticleInterface;
use App\Interfaces\ArticleTagInterface;
use App\Interfaces\ArticleCategoryInterface;
use App\Interfaces\CommentInterface;
use Validator;
use App\Models\Tag;
use App\Models\ArticleTag;
use App\Models\ArticleCategory;
use App\Models\Media;
use App\Models\MediaRelation;

class ArticleController extends Controller
{
    private $category;
    private $article;
    private $article_tag;
    private $article_category;
    private $comment;

    public function __construct(CategoryInterface $categoryInterface, 
                                ArticleInterface $articleInterface, 
                                ArticleTagInterface $articleTagInterface,
                                ArticleCategoryInterface $articleCategoryInterface, 
                                CommentInterface $commentInterface){
        $this->article  = $articleInterface;
        $this->category = $categoryInterface;
        $this->article_tag = $articleTagInterface;
        $this->article_category = $articleCategoryInterface;
        $this->comment = $commentInterface;
    }
    public function getIndex(Request $request){
        if($request->ajax()){
            $limit = $request->get('limit', 10);
            $filter['keyword'] = $request->get('keyword');
            $listArticle = $this->article->adminGetAllArticle($limit, $filter);
            
            return response()->json($listArticle);
        }
        $listCate = $this->category->where('state',1)->get();
        $filter = [];
        $keyword = $request->get('keyword');
        $category = $request->get('category');
        $filter['keyword']= $keyword;
        $filter['category_id'] = $category;

        $listArticle = $this->article->adminGetAllArticle(10, $filter);
        
        return view('admin.article.index', compact('listArticle','listCate','category'));
    }

    
    public function anyCreate(Request $request){
        if($request->isMethod('post')){
            $data = $request->all();
            $validator = Validator::make($data,[
                'title'=>'required',
                'sumary'=>'required',
                'content_html'=> 'required',
                'category_id'=> 'required',
                'picture' => 'required'
            ]);
      
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }

            $data['state'] = isset($data['state']) && !empty($data['state']) ? 1 : 0;
            $data['featured'] = isset($data['featured']) && !empty($data['featured']) ? 1 : 0;

            $data['allow_comment'] = isset($data['allow_comment']) && !empty($data['allow_comment']) ? 1 : 0;

            if($data['article_type'] == 2){
                $validator = Validator::make($data,[
                    'gallery' => 'required'
                ]);
            
                if ($validator->fails())
                {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
            }

            if($data['article_type'] == 3){
                $validator = Validator::make($data,[
                    'video_url' => 'required'
                ]);
            
                if ($validator->fails())
                {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                if(strpos( $data['video_url'], 'youtube') != false){
                    $data['video_url'] = 'https://www.youtube.com/embed/'.substr($data['video_url'], -11);
                }
            }

            $article = $this->article->createArticle($data);

            if(isset($data['gallery'])){
                $this->saveMultiImage($article->id, $data['gallery'],'article');
            }
            $this->saveArticleCategory($data['category_id'], $article->id);
            //create tag
            if(isset($data['tag'])){
                $this->saveTag($data['tag'],$article->id);
            }
            

            return redirect(route('cms.article'))->with('status','Bài viết "'. $data['title'] .'" đã được tạo thành công.');
        }
        
        $listCate = $this->category->where('state',1)->get();


        return view('admin.article.form',compact('listCate'));
    }

    public function anyUpdate(Request $request, $articleId){
        $article = $this->article->adminGetArticleDetail($articleId);
        $list_img_gallery = [];
        $listGallery = MediaRelation::where('obj_id',$article->id)->where('obj_type','article')->select('path')->get();
        if($listGallery){
            foreach($listGallery as $gallery){
                $list_img_gallery[] = $gallery->path;
            }
        }
        $list_tag = $article->tags;
        $listCate = $this->category->where('state',1)->get();
        $list_cate_by_ar = [];
        $listCateByArticle = $article->categories;
        foreach($listCateByArticle as $item){
            $list_cate_by_ar[] = $item->id;
        }

        if($request->isMethod('post')){
            $data = $request->all();
            // dd($data);
            $validator = Validator::make($data,[
                'title'=>'required',
                'sumary'=>'required',
                'content_html'=> 'required',
                'category_id'=> 'required',
                'picture' => 'required'
                // 'picture' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
  
            if ($validator->fails())
            {
                return redirect()->back()->withErrors($validator->errors())->withInput();
            }
         
            if($data['article_type'] == 2){
                $validator = Validator::make($data,[
                    'gallery' => 'required'
                ]);
            
                if ($validator->fails())
                {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
            }
            if(isset($data['gallery'])){
                $this->saveMultiImage($article->id, $data['gallery'],'article');
            }

            if($data['article_type'] == 3){
                $validator = Validator::make($data,[
                    'video_url' => 'required'
                ]);
            
                if ($validator->fails())
                {
                    return redirect()->back()->withErrors($validator->errors())->withInput();
                }
                if(strpos( $data['video_url'], 'youtube') != false){
                    $data['video_url'] = 'https://www.youtube.com/embed/'.substr($data['video_url'], -11);
                }
            }
            
            $data['state'] = isset($data['state']) ? (int)$data['state'] : '';
            $data['featured'] = isset($data['featured']) && !empty($data['featured']) ? 1 : 0;
            $data['allow_comment'] = isset($data['allow_comment']) ? (int)$data['allow_comment'] : '';
            $data['hide_thumbnail'] = isset($data['hide_thumbnail']) ? (int)$data['hide_thumbnail'] : 0;
            $article->fill($data)->save();

            $this->saveArticleCategory($data['category_id'], $article->id);

            //create tag
            if(isset($data['tag'])){
                $this->saveTag($data['tag'],$article->id);
            }

            return redirect()->back()->with('status','Bài viết "'. $data['title'] .'" đã được cập nhật thành công.');
        }
        
        return view('admin.article.form', compact('article','listCate','list_tag','list_cate_by_ar','list_img_gallery'));
    }

    public function updateMainImg(Request $request){
        if(!$request->ajax()){
            abort(404);
        }
        $article_id = $request->get('article_id');
        $img_id     = $request->get('img_id');
        $article = $this->article->where('id',$article_id)->first();
        if($article){
            $media = Media::where('media_id',$img_id)->first();
            if($media){
                $userID = \Auth::user()->id;
                $affect = $article->update(['picture'=>$img_id,'user_edited_id'=>$userID]);
                if($affect){
                    \Log::error('Update main media thành công! MediaID: '.$img_id.' User Edited: '.$userID);
                    return response()->json(['stt'=>1,'msg'=>'Update Media to Article success']);
                }
                else{
                    \Log::error('Update main media Failed! MediaID: '.$img_id.' User Edited: '.$userID);
                    return response()->json(['stt'=>0,'msg'=>'Update Media to Article Faile']);
                }
                
            }
            else{
                \Log::error('Update main media: Không tồn tại media '.$img_id);
                return response()->json(['stt'=>0,'msg'=>'Update Media to Article Faile']);
            }
        }
        else{
            \Log::error('Update main media: Không tồn tại bài viêt '.$article_id);
            return response()->json(['stt'=>0, 'msg'=>'Không tồn tại bài viết']);
        }
        
    }

    //save tag with array data
    function saveArticleCategory($data, $article_id){
        if(!empty($data) && is_array($data)){
            ArticleCategory::where('article_id', $article_id)->delete();
            foreach ($data as $key=>$val) {
                if ($val === null || empty($val)){
                    unset($data[$key]);
                }
                else{
                    ArticleCategory::create([
                        'category_id'   =>  $val,
                        'article_id'  =>  $article_id
                    ]);
                }   
            }
        }
    }


    //save tag with array data
    function saveTag($data, $article_id){
        if(!empty($data) && is_array($data)){
            ArticleTag::where('article_id', $article_id)->delete();
            foreach ($data as $key=>$val) {
                if ($val === null || empty($val))
                   unset($data[$key]);
            }

           $all_tag = array_values($data);

            for ($i=0; $i < count($all_tag); $i++) {
                $temp = trim($all_tag[$i]);
                $checkTag = Tag::where('name','=',$temp)->first();
                if(!isset($checkTag->name)){
                    $tag = Tag::create([
                        'name' => trim($temp),
                        'slug'=>str_slug($temp)
                        ]);

                    ArticleTag::create([
                        'tag_id'   =>  $tag->id,
                        'article_id'  =>  $article_id
                        ]);
                }
                else{
                    ArticleTag::create([
                        'tag_id'   =>  $checkTag->id,
                        'article_id'  =>  $article_id
                        ]);
                }
            }
        }
        
    }
    //save tag with string data. ex: data1,data2.data3
    function makeTags($data, $article_id){
        ArticleTag::where('article_id', $article_id)->delete();
        if(trim($data) != ''){
            $data = explode(',', $data);
            foreach ($data as $key=>$val) {
                if ($val === null || empty($val))
                unset($data[$key]);
            }

            $all_tag = array_values($data);

            for ($i=0; $i < count($all_tag); $i++) {
                $temp = trim($all_tag[$i]);
                $checkTag = Tag::where('name','=',$temp)->first();
                if(!isset($checkTag->name)){
                    $tag = Tag::create([
                        'name' => trim($temp),
                        'slug'=>str_slug($temp)
                        ]);

                    ArticleTag::create([
                        'tag_id'   =>  $tag->id,
                        'article_id'  =>  $article_id
                        ]);
                }
                else{
                    ArticleTag::create([
                        'tag_id'   =>  $checkTag->id,
                        'article_id'  =>  $article_id
                        ]);
                }
            }
        }
    }

    //function using for admin add image
    function saveImage($linkImage, $nameshops){
        $nameshop = ($nameshops);//this is title article
        $year = date("Y");   
        $month = date("m");   
        $year_folder = "images/".$year;   
        $month_folder = "images/".$year."/".$month;

        if(file_exists($year_folder)){
            if(file_exists($month_folder)==false){
                mkdir($month_folder,0777);
            }
        }else{
            mkdir($year_folder,0777);
        }

        $image = $month_folder.'/'.$linkImage->getClientOriginalName();
        //Get image min
        file_put_contents($image, file_get_contents(($linkImage)));
        
        return $image; 
    }

    function cutImage($linkImage){ 
        $path = explode('/',$linkImage);
        $filename = explode('.',$path[3]);//filename[0] =  filename sau khi tachs 
      
        $this->resize_image('force',$linkImage,$path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_1000x750.'.$filename[1],1000,750);
        $this->resize_image('force',$linkImage,$path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_800x600.'.$filename[1],800,600);
        $this->resize_image('force',$linkImage,$path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_570x400.'.$filename[1],570,400);
        $this->resize_image('force',$linkImage,$path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_375x280.'.$filename[1],375,280);
        $this->resize_image('force',$linkImage,$path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_270x195.'.$filename[1],270,195);
        $this->resize_image('force',$linkImage,$path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_150x110.'.$filename[1],150,110);

        $thumb = array(
        '1000' => $path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_1000x750.'.$filename[1],
        '800' => $path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_800x600.'.$filename[1],
        '570' => $path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_570x400.'.$filename[1],
        '375' => $path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_375x280.'.$filename[1],
        '270' => $path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_270x195.'.$filename[1],
        '150' => $path[0].'/'.$path[1].'/'.'/'.$path[2].'/'.$filename[0].'_150x110.'.$filename[1],
            );
        return $thumb;
    }
    function resize_image($method,$image_loc,$new_loc,$width,$height) {

        if (!is_array(@$GLOBALS['errors'])) { $GLOBALS['errors'] = array(); }

        if (!in_array($method,array('force','max','crop'))) { $GLOBALS['errors'][] = 'Invalid method selected.'; }

        if (!$image_loc) { 
            $GLOBALS['errors'][] = 'No source image location specified.'; 
        }else {
            if ((substr(strtolower($image_loc),0,7) == 'http://') || (substr(strtolower($image_loc),0,7) == 'https://')) { /*don't check to see if file exists since it's not local*/ }
            elseif (!file_exists($image_loc)) { $GLOBALS['errors'][] = 'Image source file does not exist.'; }
                $extension = strtolower(substr($image_loc,strrpos($image_loc,'.')));
            if (!in_array($extension,array('.jpg','.jpeg','.png','.gif','.bmp'))) { $GLOBALS['errors'][] = 'Invalid source file extension!'; }
        }

        if (!$new_loc) { $GLOBALS['errors'][] = 'No destination image location specified.'; }
        else {
            $new_extension = strtolower(substr($new_loc,strrpos($new_loc,'.')));
            if (!in_array($new_extension,array('.jpg','.jpeg','.png','.gif','.bmp'))) { $GLOBALS['errors'][] = 'Invalid destination file extension!'; }
        }

        $width = abs(intval($width));
        if (!$width) { $GLOBALS['errors'][] = 'No width specified!'; }

        $height = abs(intval($height));
        if (!$height) { $GLOBALS['errors'][] = 'No height specified!'; }

        if (count($GLOBALS['errors']) > 0) { $this->echo_errors(); return false; }

        if (in_array($extension,array('.jpg','.jpeg'))) { $image = @imagecreatefromjpeg($image_loc); }
        elseif ($extension == '.png') { $image = @imagecreatefrompng($image_loc); }
        elseif ($extension == '.gif') { $image = @imagecreatefromgif($image_loc); }
        elseif ($extension == '.bmp') { $image = @imagecreatefromwbmp($image_loc); }

        if (!$image) { $GLOBALS['errors'][] = 'Image could not be generated!'; }
        else {
            $current_width = imagesx($image);
            $current_height = imagesy($image);
            if ((!$current_width) || (!$current_height)) { $GLOBALS['errors'][] = 'Generated image has invalid dimensions!'; }
        }
        if (count($GLOBALS['errors']) > 0) { @imagedestroy($image); $this->echo_errors(); return false; }

        if ($method == 'force') { $new_image = $this->resize_image_force($image,$width,$height); }
        elseif ($method == 'max') { $new_image = $this->resize_image_max($image,$width,$height); }
        elseif ($method == 'crop') { $new_image = $this->resize_image_crop($image,$width,$height); }

        if ((!$new_image) && (count($GLOBALS['errors'] == 0))) { $GLOBALS['errors'][] = 'New image could not be generated!'; }
        if (count($GLOBALS['errors']) > 0) { @imagedestroy($image); $this->echo_errors(); return false; }

        $save_error = false;
        if (in_array($extension,array('.jpg','.jpeg'))) { imagejpeg($new_image,$new_loc) or ($save_error = true); }
        elseif ($extension == '.png') { imagepng($new_image,$new_loc) or ($save_error = true); }
        elseif ($extension == '.gif') { imagegif($new_image,$new_loc) or ($save_error = true); }
        elseif ($extension == '.bmp') { imagewbmp($new_image,$new_loc) or ($save_error = true); }
        if ($save_error) { $GLOBALS['errors'][] = 'New image could not be saved!'; }
        if (count($GLOBALS['errors']) > 0) { @imagedestroy($image); @imagedestroy($new_image); $this->echo_errors(); return false; }

        @imagedestroy($image);
        @imagedestroy($new_image);

        return true;
    }
    function resize_image_force($image,$width,$height) {
        $w = @imagesx($image); //current width
        $h = @imagesy($image); //current height
        if ((!$w) || (!$h)) { $GLOBALS['errors'][] = 'Image couldn\'t be resized because it wasn\'t a valid image.'; return false; }
        if (($w == $width) && ($h == $height)) { return $image; } //no resizing needed

        $image2 = imagecreatetruecolor ($width, $height);
        imagecopyresampled($image2,$image, 0, 0, 0, 0, $width, $height, $w, $h);

        return $image2;
    }
    function resize_image_max($image,$max_width,$max_height) {
        $w = imagesx($image); //current width
        $h = imagesy($image); //current height
        if ((!$w) || (!$h)) { $GLOBALS['errors'][] = 'Image couldn\'t be resized because it wasn\'t a valid image.'; return false; }

        if (($w <= $max_width) && ($h <= $max_height)) { return $image; } //no resizing needed

        //try max width first...
        $ratio = $max_width / $w;
        $new_w = $max_width;
        $new_h = $h * $ratio;

        //if that didn't work
        if ($new_h > $max_height) {
            $ratio = $max_height / $h;
            $new_h = $max_height;
            $new_w = $w * $ratio;
        }

        $new_image = imagecreatetruecolor ($new_w, $new_h);
        imagecopyresampled($new_image,$image, 0, 0, 0, 0, $new_w, $new_h, $w, $h);
        return $new_image;
    }
    function resize_image_crop($image,$width,$height) {
        $w = @imagesx($image); //current width
        $h = @imagesy($image); //current height
        if ((!$w) || (!$h)) { $GLOBALS['errors'][] = 'Image couldn\'t be resized because it wasn\'t a valid image.'; return false; }
        if (($w == $width) && ($h == $height)) { return $image; } //no resizing needed

        //try max width first...
        $ratio = $width / $w;
        $new_w = $width;
        $new_h = $h * $ratio;

        //if that created an image smaller than what we wanted, try the other way
        if ($new_h < $height) {
            $ratio = $height / $h;
            $new_h = $height;
            $new_w = $w * $ratio;
        }

        $image2 = imagecreatetruecolor ($new_w, $new_h);
        imagecopyresampled($image2,$image, 0, 0, 0, 0, $new_w, $new_h, $w, $h);

        //check to see if cropping needs to happen
        if (($new_h != $height) || ($new_w != $width)) {
            $image3 = imagecreatetruecolor ($width, $height);
            if ($new_h > $height) { //crop vertically
                $extra = $new_h - $height;
                $x = 0; //source x
                $y = round($extra / 2); //source y
                imagecopyresampled($image3,$image2, 0, 0, $x, $y, $width, $height, $width, $height);
            } else {
                $extra = $new_w - $width;
                $x = round($extra / 2); //source x
                $y = 0; //source y
                imagecopyresampled($image3,$image2, 0, 0, $x, $y, $width, $height, $width, $height);
            }
            @imagedestroy($image2);
            return $image3;
        } else {
            return $image2;
        }
    }

    public function anyDelete(Request $request){
        if (!$request->ajax()) {
            abort(403);
        }
        try{
            \DB::beginTransaction();
            $id = $request->get('id', 0);
            $article = $this->article->where('id',$id)->first();
            
            if(!$article){
                return response()->json(['stt'=>0,'msg'=>'Không tồn tại bài viết này trong hệ thống']);
            }
            //delete all tag with article id
            $this->article_tag->where('article_id',$article->id)->delete();

            //delete all article_category with article id 
            $this->article_category->where('article_id',$article->id)->delete();

             //delete all comment with article id 
             $this->comment->where('article_id',$article->id)->delete();
            //delete this article
            $article->delete();

            \DB::commit();
            return response()->json(['stt'=>1,'msg'=>'Delete Article Successfully']);
        }
        catch(\Exception $e){
            \Log::error($e->getMessage() . "\r\n" . $e->getTraceAsString());
            \DB::rollBack();
            return response ()->json (['stt' => 0],401);
        }
    }

    public function saveMultiImage($id, $data, $type){
        if(!empty($data) && is_array($data)){
            MediaRelation::where('obj_id', $id)->delete();
            $all_media = array_values($data);
           
            for ($i=0; $i < count($all_media); $i++) {
                $temp = trim($all_media[$i]);
                MediaRelation::create([
                    'media_id'  =>  Null,
                    'obj_id'    =>  $id,
                    'obj_type'  =>  $type,
                    'path'      =>  $temp
                ]);
            }
        }
    }
}
