<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function anyLogin(Request $request){
        $errors = array();
        if($request->isMethod('post')){
            $validator = \Validator::make($request->all(), [
                'username'  => 'required',
                'password'  =>  'min:6|max:32'
            ]);
            if(count($validator->errors())){
                $errors = $validator->errors();
            } else {
                $input = $request->all();
                if($request->get('username')){
                    $credentials = ['username'=>$request->get('username'), 'password'=>$request->get('password'),'state'=>1];
            //    dd($credentials);
                    if (\Auth::attempt($credentials, $request->get('remember', 0))) {
                        return redirect()->route('cms.dashboard');
                    } else {
                       $validator->errors()->add('field', 'Tài khoản hoặc mật khẩu không chính xác');
                       $errors = $validator->errors();
                    }
                }
            }
        }
        return view('admin.auth.login' , compact('errors'));
    }

    public function getLogout(Request $request){
        \Auth::logout();
        return redirect(route('cms.auth.login'));
    }
}
