<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Helpers\Media\MediaUpload;
use App\Helpers\Media\MediaHelper;
use App\Helpers\Media\MediaCutImage;
use App\Models\Media;
use App\Models\ArticleCategory;

class GetDataController extends Controller
{
    public function getData(){
        return $this->sao_ngoisao_net('http://ngoisao.net/tin-tuc/hau-truong/page/2.html',2);
        //return $this->zing_Phuot('http://news.zing.vn/phuot.html',3);
       // return $this->vnExpress_Dulich('http://dulich.vnexpress.net/tin-tuc/viet-nam',7); 
        return response()->json(['msg'=>'Xong']);
    }
    function sao_ngoisao_net($link,$category_id){
        //dd(123443);
      //include(app_path() . '/Libs/simple_html_dom.php'); 
      $html = file_get_html($link);
    //   dd($html);
      foreach ($html->find('.box_folder_article') as $key => $content) {
        // dd($content);
        foreach($content->find('.content') as $content_top){
            // dd($content_top);
          foreach ($content_top->find('.title_news a') as $key => $value) {
        //    dd($value);
            $this->getDataSaoNgoisao($value->attr['href'],$category_id);
            exit;
          }

        }
      }
    
      //return response()->json(['stt'=>'1','msg'=>'Xong']);
    }

    function getDataSaoNgoisao($link_origin,$category_id){
        $link_origin = 'https://ngoisao.net/hau-truong/bradley-cooper-tay-xach-nach-mang-khi-theo-ban-gai-di-cho-3914374.html';
        // if($this->checkLink($link_origin)){
            $html = file_get_html($link_origin);
            $article = array();
  
            $article['origin_link'] = trim($link_origin);
            $article['source'] = 'Ngoisao.net';
            $article['category_id'] = $category_id;
            $article_image = true; //check content is all image
            $checkthumb = true; //check is thumbnail
            $article['article_type'] = Article::TYPE_ARTICLE;
            // dd($article); 
            foreach ($html->find('.box_stream') as $content) {
    
                $article_image = false;
                //title
                foreach ($content->find('h1.title') as $title) {
                  $article['title'] = trim($title->plaintext);
                 // $article['slug'] = convertUTF8($article['title']);
                  break;
                }
    
                //intro
                foreach ($content->find('p.lead') as $sumary) {
                    $article['sumary'] = '<p>'.trim($sumary->plaintext).'</p>';
                    break;
                }
    
                $content_arr = array();//luu content dang array
                $content_html = '';//luu content dang html
    
                $thumb = true; //luu thumbnail
                $stt =1;
  
                foreach ($content->find('.tplCaption') as $content_detail) {
                    $count_item_content = count($content_detail->children());
                    // dd($count_item_content);
                    for($i =0; $i < $count_item_content - 1; $i++){
                        $stt++;
                  
                        $cont = $content_detail->children($i);
    
                        //keim tra bai viet co video hay ko
                        if($cont->tag == 'video'){
                          $article['video_url'] = $cont->attr['src'];
                          $article['article_type'] = Article::TYPE_VIDEO;
                          break;
                        }
                    
                        $checkcontentimg = false;
                        $img_thumb = '';
                        //tìm image trong bài viết
                        foreach ($cont->find('img') as $image_content) {
                            $img_thumb = trim($image_content->attr['src']);
                            break;
                        }
                        //nếu không có image thi la text
                        if($img_thumb ==''){
                            $content_arr[] = array('type' => 'text', 'content' => trim($cont->plaintext));
                            $content_html .= '<p>'.trim($cont->plaintext).'</p>';
                        }
                        else//nếu có image
                        {
                            $img_des = $article['title'];;
                            //tìm des image trong bài viết
                            foreach ($cont->find('img') as $image_des) {
                                $img_des = trim($image_des->plaintext);
                                break;
                            }
                            //save image
                            // $imgsave = $this->saveImage2($img_thumb, str_slug($article['title']), $stt, 'cn');
                            $imgsave = $this->createMedia($img_thumb);
                            $article['picture'] = $imgsave;
                            $article['picture_des'] = $img_des;
    
                            //check thumb neu chua co thi luu
                            if($thumb){
                            //   $article['thumb_array'] = json_encode($this->cutImage($imgsave));
                            //   $thumb = false;
                            }
                            // $content_arr[] = array('type' => 'img', 'content' => $img_des, 'src'=>$imgsave);
                            $content_html .= '<p class="has_img"><img class="img_article" alt="'.$img_des.'" src="/'.$imgsave.'"><i class="des_img_article">'.$img_des.'.</i></p>';
                        
                        }
                    }
                }
                //dd($article);
                $article['author'] = '';
                foreach ($content->find('p.author') as $author) {
                  $article['author'] = trim($author->plaintext);
                  break;
                }
    
                // $article['content_array']= json_encode($content_arr);
                $article['content_html']= $content_html;
    
                
                $tags= array();
                foreach($content->find('.the-article-tags') as $tag){
                   $tags[] = trim($tag);
                }
                  
                $article['tags'] =$tags;
    
                $article['user_id'] = 3;
 dd($article);
                $art = Article::create($article);

                $this->saveArticleCategory(array($category_id), $art->id);
                //save tag
                if(count($tags) > 0){
                    $this->saveTag($article['tags'],$art->id);
                }
                
            }
        // }
        // else{
        //     return response()->json(456);
        // }
    }
    
    function getDataPhuotViVu($link_origin,$category_id){
        $category_id = 3;
        $link_origin = 'https://phuotvivu.com/blog/du-lich-pha-luong-moc-chau/';
        // if($this->checkLink($link_origin)){
            $html = file_get_html($link_origin);
            $article = array();
  
            $article['origin_link'] = trim($link_origin);
            $article['source'] = 'Ngoisao.net';
            $article['category_id'] = $category_id;
            $article_image = true; //check content is all image
            $checkthumb = true; //check is thumbnail
            $article['article_type'] = Article::TYPE_ARTICLE;
            // dd($article); 
            foreach ($html->find('.box_stream') as $content) {
    
                $article_image = false;
                //title
                foreach ($content->find('h1.title') as $title) {
                  $article['title'] = trim($title->plaintext);
                 // $article['slug'] = convertUTF8($article['title']);
                  break;
                }
    
                //intro
                foreach ($content->find('p.lead') as $sumary) {
                    $article['sumary'] = '<p>'.trim($sumary->plaintext).'</p>';
                    break;
                }
    
                $content_arr = array();//luu content dang array
                $content_html = '';//luu content dang html
    
                $thumb = true; //luu thumbnail
                $stt =1;
  
                foreach ($content->find('.tplCaption') as $content_detail) {
                    $count_item_content = count($content_detail->children());
                    // dd($count_item_content);
                    for($i =0; $i < $count_item_content - 1; $i++){
                        $stt++;
                  
                        $cont = $content_detail->children($i);
    
                        //keim tra bai viet co video hay ko
                        if($cont->tag == 'video'){
                          $article['video_url'] = $cont->attr['src'];
                          $article['article_type'] = Article::TYPE_VIDEO;
                          break;
                        }
                    
                        $checkcontentimg = false;
                        $img_thumb = '';
                        //tìm image trong bài viết
                        foreach ($cont->find('img') as $image_content) {
                            $img_thumb = trim($image_content->attr['src']);
                            break;
                        }
                        //nếu không có image thi la text
                        if($img_thumb ==''){
                            $content_arr[] = array('type' => 'text', 'content' => trim($cont->plaintext));
                            $content_html .= '<p>'.trim($cont->plaintext).'</p>';
                        }
                        else//nếu có image
                        {
                            $img_des = $article['title'];;
                            //tìm des image trong bài viết
                            foreach ($cont->find('img') as $image_des) {
                                $img_des = trim($image_des->plaintext);
                                break;
                            }
                            //save image
                            // $imgsave = $this->saveImage2($img_thumb, str_slug($article['title']), $stt, 'cn');
                            $imgsave = $this->createMedia($img_thumb);
                            $article['picture'] = $imgsave;
                            $article['picture_des'] = $img_des;
    
                            //check thumb neu chua co thi luu
                            if($thumb){
                            //   $article['thumb_array'] = json_encode($this->cutImage($imgsave));
                            //   $thumb = false;
                            }
                            // $content_arr[] = array('type' => 'img', 'content' => $img_des, 'src'=>$imgsave);
                            $content_html .= '<p class="has_img"><img class="img_article" alt="'.$img_des.'" src="/'.$imgsave.'"><i class="des_img_article">'.$img_des.'.</i></p>';
                        
                        }
                    }
                }
                //dd($article);
                $article['author'] = '';
                foreach ($content->find('p.author') as $author) {
                  $article['author'] = trim($author->plaintext);
                  break;
                }
    
                // $article['content_array']= json_encode($content_arr);
                $article['content_html']= $content_html;
    
                
                $tags= array();
                foreach($content->find('.the-article-tags') as $tag){
                   $tags[] = trim($tag);
                }
                  
                $article['tags'] =$tags;
    
                $article['user_id'] = 3;
 dd($article);
                $art = Article::create($article);

                $this->saveArticleCategory(array($category_id), $art->id);
                //save tag
                if(count($tags) > 0){
                    $this->saveTag($article['tags'],$art->id);
                }
                
            }
        // }
        // else{
        //     return response()->json(456);
        // }
    }
    function checkLink($linkstory){
        $data = Article::where('origin_link',$linkstory)->first();
        if(!empty($data)){
            return false;
        }
        return true;
    }
    
    function echo_errors() {
        if (!is_array(@$GLOBALS['errors'])) { $GLOBALS['errors'] = array('Unknown error!'); }
        foreach ($GLOBALS['errors'] as $error) { echo '<p style="color:red;font-weight:bold;">Error: '.$error.'</p>'; }
    }

    //save tag with array data
    function saveTag($data, $article_id){
        if(!empty($data) && is_array($data)){
            ArticleTag::where('article_id', $article_id)->delete();
            foreach ($data as $key=>$val) {
                if ($val === null || empty($val))
                   unset($data[$key]);
           }

           $all_tag = array_values($data);

            for ($i=0; $i < count($all_tag); $i++) {
                $temp = trim($all_tag[$i]);
                $checkTag = Tag::where('name','=',$temp)->first();
                if(!isset($checkTag->name)){
                    $tag = Tag::create([
                        'name' => trim($temp),
                        'slug'=>str_slug($temp)
                        ]);

                    ArticleTag::create([
                        'tag_id'   =>  $tag->id,
                        'article_id'  =>  $article_id
                        ]);
                }
                else{
                    ArticleTag::create([
                        'tag_id'   =>  $checkTag->id,
                        'article_id'  =>  $article_id
                        ]);
                }
            }
        }
        
    }

    //save tag with array data
    function saveArticleCategory($data, $article_id){
        if(!empty($data) && is_array($data)){
            ArticleCategory::where('article_id', $article_id)->delete();
            foreach ($data as $key=>$val) {
                if ($val === null || empty($val)){
                    unset($data[$key]);
                }
                else{
                    ArticleCategory::create([
                        'category_id'   =>  $val,
                        'article_id'  =>  $article_id
                    ]);
                }   
            }
        }
    }

    function createMedia($file){
        $filenameArr = explode('/', $file); 
        $filename = end($filenameArr);
        $uploadDir    = MediaHelper::rootUploadDir('storage');
        $uploadFolder = MediaHelper::uploadDir();
        $result = file_put_contents($uploadDir.'/'.$uploadFolder.'/'.$filename, file_get_contents($file));
        if($result){
            return $uploadFolder.'/'.$filename;
        }
        return;
    }
}
