<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Interfaces\ArticleInterface;
use App\Interfaces\ArticleTagInterface;
use App\Interfaces\ArticleCategoryInterface;
use App\Interfaces\CategoryInterface;
use App\Interfaces\DictionaryInterface;
use App\Interfaces\CustomerInterface;
use App\Interfaces\UserInterface;
use App\Interfaces\TagInterface;
use App\Interfaces\CommentInterface;
use App\Interfaces\UserProfileInterface;
use App\Interfaces\RoleInterface;
use App\Interfaces\PermissionInterface;

use App\Services\Customer;
use App\Services\User;
use App\Services\Article;
use App\Services\Category;
use App\Services\ArticleTag;
use App\Services\ArticleCategory;
use App\Services\Dictionary;
use App\Services\Tag;
use App\Services\Comment;
use App\Services\UserProfile;
use App\Services\Role;
use App\Services\Permission;

class RepoServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind ( UserInterface::class, User::class );
		$this->app->bind ( ArticleInterface::class, Article::class );
		$this->app->bind ( ArticleTagInterface::class, ArticleTag::class );
		$this->app->bind ( ArticleCategoryInterface::class, ArticleCategory::class );
		$this->app->bind ( CategoryInterface::class, Category::class );
		$this->app->bind ( CustomerInterface::class, Customer::class );
		$this->app->bind ( DictionaryInterface::class, Dictionary::class );
        $this->app->bind ( TagInterface::class, Tag::class );
        $this->app->bind ( CommentInterface::class, Comment::class );
        $this->app->bind ( UserProfileInterface::class, UserProfile::class );
        $this->app->bind ( RoleInterface::class, Role::class );
        $this->app->bind ( PermissionInterface::class, Permission::class );
    }
}
