<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Article;
use App\Services\Category;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $popular_article = Article::join('article_category as ac','ac.article_id','=','articles.id')
                            ->join('categories as cate','cate.id','=','ac.category_id')
                            ->where('articles.state',1)
                            ->where('cate.state',1)
                            ->select('cate.name as ct_name','cate.slug as ct_slug','articles.*')
                            ->groupBy('articles.id')
                            ->limit(5)
                            ->orderBy('articles.view_count','DESC')
                            ->get();

        $most_comment_article = Article::join('categories as cate','cate.id','=','articles.category_id')
                            ->join('comments as cm','cm.article_id','=','articles.id')
                            ->where('articles.state',1)
                            ->where('cate.state',1)
                            ->select('cate.name as ct_name','cate.slug as ct_slug','articles.*',\DB::raw('count(cm.article_id) as comment_count'))
                            ->groupBy('articles.id')
                            ->limit(5)
                            ->orderBy('comment_count','DESC')->get();

        //get all cate show in menu
        $all_category = Category::where('state',1)->where('showMenu',1)->orderBy('order', 'DESC')->get();

        $configs 	= \App\Models\Configure::all();
        $config_variable = [];
        if(!empty($configs)){
            foreach($configs as $configs){
                $config_variable[$configs->key]=$configs->value;
            }
        }
        
        \View::share(['configs'=>$config_variable,'popular_article'=> $popular_article,'most_comment_article'=>$most_comment_article,'all_category'=>$all_category]);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
