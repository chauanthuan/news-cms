<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['namespace' => 'Admin', 'prefix'=>'cms'], function () {
    //login
    Route::any('/auth/login',['uses'=>'AuthController@anyLogin'])->name('cms.auth.login');

    //logged In
    Route::group(['middleware' => ['auth.cms']], function () {
        
        //logout
        Route::any('/auth/logout', 'AuthController@getLogout')->name('cms.auth.logout');
        
        //dashboard
        Route::get('/',['uses'=>'DashboardController@getIndex' , 'as' => 'cms.dashboard']);
    
        //config
        Route::any('/configure','ConfigureController@anyUpdate')->name('cms.configure')->middleware(['role:supper-admin']);

        //account
        Route::any('/account', 'AccountController@getIndex')->name('cms.account')->middleware(['role:supper-admin']);
        Route::any('/account/create', 'AccountController@anyCreate')->name('cms.account.create')->middleware(['role:supper-admin,create-account']);
        Route::any('/account/update/{accountId}', 'AccountController@anyUpdate')->name('cms.account.update')->middleware(['role:supper-admin,update-account']);
        Route::any('/account/delete', 'AccountController@anyDelete')->name('cms.account.delete')->middleware(['role:supper-admin,delete-account']);

        //Role
        Route::any('/role', 'RoleController@getIndex')->name('cms.role')->middleware(['role:supper-admin']);
        Route::any('/role/create', 'RoleController@anyCreate')->name('cms.role.create')->middleware(['role:supper-admin,create-role']);
        Route::any('/role/update/{roleId}', 'RoleController@anyUpdate')->name('cms.role.update')->middleware(['role:supper-admin,update-role']);
        Route::any('/role/delete', 'RoleController@anyDelete')->name('cms.role.delete')->middleware(['role:supper-admin,delete-role']);

        //permission
        Route::any('/permission', 'PermissionController@getIndex')->name('cms.permission')->middleware(['role:supper-admin']);
        Route::any('/permission/create', 'PermissionController@anyCreate')->name('cms.permission.create')->middleware(['role:supper-admin,create-permission']);
        Route::any('/permission/update/{permissionId}', 'PermissionController@anyUpdate')->name('cms.permission.update')->middleware(['role:supper-admin,update-permission']);
        Route::any('/permission/delete', 'PermissionController@anyDelete')->name('cms.permission.delete')->middleware(['role:supper-admin,delete-permission']);
        
        //proflie account 
        Route::any('/account/profile', 'AccountController@anyProfile')->name('cms.account.profile')->middleware(['role:supper-admin,update-profile']);

        //category
        Route::any('/category', 'CategoryController@getIndex')->name('cms.category')->middleware(['role:supper-admin|manager']);
        Route::any('/category/create', 'CategoryController@anyCreate')->name('cms.category.create')->middleware(['role:supper-admin|manager,create-category']);
        Route::any('/category/update/{categoryId}', 'CategoryController@anyUpdate')->name('cms.category.update')->middleware(['role:supper-admin|manager,update-category']);;
        Route::any('/category/delete', 'CategoryController@anyDelete')->name('cms.category.delete')->middleware(['role:supper-admin|manager,delete-category']);;
        
        //article
        Route::any('/article', 'ArticleController@getIndex')->name('cms.article')->middleware(['role:supper-admin|manager|editor,view-all-article']);
        Route::any('/article/create', 'ArticleController@anyCreate')->name('cms.article.create')->middleware(['role:supper-admin|manager|editor,create-article']);
        Route::any('/article/update/{articleId}', 'ArticleController@anyUpdate')->name('cms.article.update')->middleware(['role:supper-admin|manager|editor,update-article']);
        Route::any('/article/update-main-img', 'ArticleController@updateMainImg')->name('cms.article.updatemainimg')->middleware(['role:supper-admin|manager|editor,update-main-img']);
        Route::any('/article/delete', 'ArticleController@anyDelete')->name('cms.article.delete')->middleware(['role:supper-admin|manager,delete-article']);

        //comment
        Route::any('/comment', 'CommentController@getIndex')->name('cms.comment')->middleware(['role:supper-admin|manager|editor']);
        Route::any('/comment/update/{commentId}', 'CommentController@anyUpdate')->name('cms.comment.update')->middleware(['role:supper-admin|manager|editor,update-comment']);
        Route::any('/comment/delete', 'CommentController@anyDelete')->name('cms.comment.delete')->middleware(['role:supper-admin|manager|editor,delete-comment']);

        //media
        Route::get('/upload',['uses'=>'MediaController@getIndex' , 'as' => 'cms.mediamodal'])->middleware(['role:supper-admin|manager|editor']);
        Route::get('/media',['uses'=>'MediaController@getAll' , 'as' => 'cms.media'])->middleware(['role:supper-admin|manager|editor']);
        Route::any('/media/create',['uses'=>'MediaController@anyCreate' , 'as' => 'cms.media.create'])->middleware(['role:supper-admin|manager|editor,create-comment']);
        Route::any('/media/delete',['uses'=>'MediaController@getDestroy' , 'as' => 'cms.media.delete'])->middleware(['role:supper-admin|manager,delete-media']);
    });
    
});
Route::group(['namespace' => 'Web'], function () {
    Route::get('/', 'IndexController@index')->name('web.index');
    Route::get('/lien-he', function(){
        return view('web.pages.contact');
    })->name('web.contact');
    //AUTH
    Route::any('/auth/login',	  ['uses'=>'AuthController@anyLogin'])->name('web.auth.login');
    // Route::any('/auth/register',	  ['uses'=>'AuthController@anyRegister'])->name('web.auth.register');
    // Route::any('/auth/register/confirm/{token}',	  ['uses'=>'AuthController@anyRegisterConfirm'])->name('web.auth.registerconfirm');
    Route::any('/auth/reset-pass',['uses'=>'AuthController@anyResetPass'])->name('web.auth.resetpass');
    Route::any('/auth/reset-pass/confirm/{token}',['uses'=>'AuthController@anyResetPassConfirm'])->name('web.auth.resetpassconfirm');

    Route::group(['middleware' => ['auth.web']], function () {    
        Route::any('/auth/logout',  ['uses'=>'AuthController@getLogout'])->name('web.auth.logout');
        Route::any('/user/change-pass',  ['uses'=>'AuthController@anyChangePass'])->name('web.auth.changepass');
        Route::any('/user/profile',['uses'=>'AuthController@anyProfile'])->name('web.auth.profile');
    });

    //search
    Route::get('/search',['uses'=>'IndexController@seachData'])->name('web.seach');

    //video
    Route::get('/videos',['uses'=>'IndexController@videos'])->name('web.video');

    //Gallery
    Route::get('/galleries',['uses'=>'IndexController@galleries'])->name('web.gallery');

    //detail article, category, tag
    Route::get('/tag/{tagSlug}', ['uses' => 'ArticleController@getArtileByTag', 'as' => 'web.tag']);
    Route::get('/{categorySlug}', ['uses' => 'CategoryController@index', 'as' => 'web.category']);
    Route::get('/{categorySlug}/{articleSlug}', ['uses' => 'ArticleController@getDetail', 'as' => 'web.article']);
    Route::post('/postcomment','CommentController@postComment')->name('postcomment');


    
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
